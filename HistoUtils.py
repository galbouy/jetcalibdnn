import numpy as np
import numexpr as ne


def average(x, w):
    wsum = w.sum()
    if wsum==0.:
        return 0.
    return (w*x).sum()/wsum

def averageAnderr(x,w,w2):
    wsum = w.sum()
    if wsum==0.:
        return 0, 1.
    over_neff =  w2.sum() / wsum
    m = (w*x).sum()/wsum
    err = np.sqrt( ( (x*x*w).sum()/wsum -m*m )*over_neff)

    return m ,err

def binIndexAndValidtyGeneric(bins, x, indices=None, validity=None):
    """Calculate the bin index of 'x' values corresponding to bins defined by 'bins'
    returns the array of these indices and the array of their validity (array of bool)
    If given, indices and validity arrays are filled and returned.
    
    (This function always create temporary indices )
    """
    if indices is not None:
        indices[:] = np.searchsorted(bins,x, 'right')
    else:
        indices = np.searchsorted(bins,x, 'right')
    if validity is None:
        validity= np.zeros_like(indices, dtype=np.bool)


    indices -=1
    ne.evaluate('(indices>-1) & (indices<{}) '.format(len(bins)-1),out=validity,casting='unsafe')
    #print(validity)    
    return indices, validity

def binIndexAndValidityReg(nbin, range,x, indices=None, validity=None):
    s = nbin / (range[1] - range[0])
    if indices is None:
        indices = np.zeros_like(x,dtype=int)        
    ne.evaluate( "floor((x-o)*s)", local_dict=dict(x=x,o=range[0], s=s), out=indices,casting='unsafe')
    ne.evaluate('(indices>-1) & (indices<{}) '.format(nbin),out=validity,casting='unsafe')
    return indices, validity

def binIndexReg(nbin, xmin, xmax,x, indices=None):
    """Return the indices of bins in which x fall, assuming there are  'nbin' bins between  xmin and xmax AND overflow and underflow bins.
    Underflows are indexed at 0, overflows indexed at nbin+1. 
    Valid bins are thus in [1, nbin]
    """
    s = nbin / (xmax - xmin)
    if indices is None:
        indices = np.zeros_like(x,dtype=int)        
    ne.evaluate( "(x-o)*s+1", local_dict=dict(x=x,o=xmin, s=s), out=indices,casting='unsafe')
    np.clip( indices, 0, nbin+1, out=indices)
    return indices


class AxisSpec:
    """Represents 1 dimension in a binned phase space. 
    This is essentially a list of bins on a 1D line which can be defined either by 
      * nbins , (xmin, xmax)  --> regular bins 
      * the list of nbins+1 edges of the bins. 
    In both case we have : edges[0]=xmin and edges[nbins]=xmax 
    """
    isGeV = True
    geVFactor = 1.

    save_att = ['edges', 'isRegular','name', 'title']

    def __init__(self, name, nbins=None, range=None, edges=None, title=None,  **args):
        self.name = name
        self.title = title or name

        
        if edges is not None:
            edges = np.array(edges)
            d0 = (edges[1]-edges[0])
            self.isRegular = np.all( np.abs(edges[1:]-edges[:-1] -d0) < d0*1e-3  )
            if nbins or range:
                print("AxisSpec ERROR specifiy either edge eithe nbins and range")
                raise
            self.edges = edges
            self.nbins = len(edges)-1
            self.range = edges[0], edges[-1]
        elif range is not None:
            self.isRegular = True
            if edges:
                print("AxisSpec ERROR specifiy either edge eithe nbins and range")
                raise
            self.nbins = nbins
            self.range = range
            self.edges = np.linspace(range[0], range[1],nbins+1)
            
        
        for k,v in args.items():
            setattr(self,k,v)

    #def findBins(self,x, indices=None, validity=None):
    def binIndexAndValidity(self,x, indices=None, validity=None):
        if self.isRegular:
            indices, validity=binIndexAndValidityReg(self.nbins, self.range, x, indices, validity)
        else:
            indices, validity =binIndexAndValidtyGeneric(self.edges, x, indices, validity)
        return indices,validity

    def binIndex(self, x, indices=None):
        if self.isRegular:
            return binIndexReg(self.nbins, self.range[0], self.range[1], x, indices)
        else:
            if indices:
                indices[:] = np.searchsorted(self.edges,x, 'right')
                return indices
            else:
                return np.searchsorted(self.edges,x, 'right')
            

    def binCenter(self, i):
        # we take i-1 because i is supposed to be a valid bin index, thus in [1, nbin]
        return 0.5*(self.edges[i-1]+self.edges[i])
            
    def binCenters(self):
        return (self.edges[:-1]+self.edges[1:])*0.5

    def describeBin(self, i):
        low , up = self.edges[i:i+2]
        if self.isGeV:
            low = int(low*self.geVFactor)
            up  = int(up*AxisSpec.geVFactor)
            binDesc='{} $\in [{:d}, {:d}]$' # self.geVDesc
        else:
            binDesc='{} $\in [{:.2f}, {:.2f}]$'
        return binDesc.format(self.title , low, up)

    def binWidth(self, i=0):
        return self.edges[i+1] -self.edges[i] 

    def dump(self):
        print( self.name, self.range, self.edges)


    def saveTo(self, out, prefix=None):
        prefix= prefix or self.name+"_"
        for att in self.save_att:
            out[prefix+att] = getattr(self, att)
    
    def loadFrom(self, inp, prefix=None):
        prefix= prefix if prefix is not None else self.name+"_"
        for att in self.save_att:
            setattr(self, att, inp.get(prefix+att,"") )
        e = self.edges
        self.range = (e.min(), e.max() )
        self.nbins = len(e)-1
        


## Histo manipulation
class HistoBase:
    #save_att = ['bins', 'hrange', 'hcontent', 'xbins']
    save_att = ['hcontent', 'hw2']
    def __init__(self,  hrange=None, x=None, y=None, w=None, name=None, title=None, axis=None,
                 hcontent=None, hw2=None,):
        if axis is not None:
            self.xaxis = axis
        elif hrange is not None:
            self.xaxis = AxisSpec('xaxis',nbins=hrange[0], range=hrange[1])
        else:
            self.xaxis = AxisSpec('xaxis')
        # self.bins=bins
        # self.hrange = hrange
        if x is not None:
            if hcontent is not None:
                raise Exception("HistoBase ERROR : do not specify x AND hcontent ")
            self.fromArrays(x,y=y,w=w)
        else:
            self.hcontent = hcontent
            if hw2 is None: hw2 = hcontent
            self.hw2 = hw2
        self.name = name or (self.__class__.__name__)
        self.title = title or self.name
        

    def asDict(self):
        d = dict( (self.name+'_'+att, getattr(self, att) ) for att in self.save_att)
        self.xaxis.saveTo(d, self.name+"_x") 
        return d
    
    def save(self, fname):
        d = self.asDict()
        np.savez(fname, **d)

    def load(self, fname):
        f = np.load(fname)
        for att in self.save_att:
            setattr(self, att, f[self.name+'_'+att] )
        self.xaxis.loadFrom( f, self.name+"_x")



class Histo1D(HistoBase):
        
    # def fromArraysO(self, x,  bins=None, hrange=None, y=None,w=None):
    #     if x is None: return
    #     self.bins = np.array(bins or self.bins)
    #     self.hrange = np.array(hrange or self.hrange)
    #     r  = np.histogram(x,bins=self.xaxis.nbins,range=(self.xaxis.min,self.xaxis.max),weights=w)
    #     self.hcontent=r[0]
    #     self.xbins = r[1]
    #     return r[0]

    def fromArrays(self, x,   y=None,w=None):
        if x is None: return
        x_i = self.xaxis.binIndex(x)
        self.hcontent = np.bincount(x_i, w , self.xaxis.nbins+2)
        if w is not None:
            self.hw2 = np.bincount(x_i, w*w , self.xaxis.nbins+2)
        else:
            self.hw2 = self.hcontent
        return self.hcontent
        
    def findBin(self,x):
        return self.axis.binIndex(x)
        
    def binContentAt(self, x,  outC=None , _x_i=None, scaleOutC=False):
        #x_i = self.binIndex(x,self.hrange,self.bins,_x_i)
        x_i = self.xaxis.binIndex(x)
        if outC is None:
            outC=self.hcontent[x_i]
        else:
            if scaleOutC: outC *=self.hcontent[x_i]
            else: outC[:] = self.hcontent[x_i]
        return outC

    def binWidth(self, i=0):
        return self.xaxis.binWidth(i)
    
    def mean(self):
        ed = self.xaxis.edges
        c = 0.5*(ed[:-1]+ed[1:])
        return average( c, self.hcontent[1:-1])

    def meanAnderr(self):
        w = self.hcontent[1:-1]
        w2 = self.hw[1:-1]
        ed = self.edges
        c = 0.5*(ed[:-1]+ed[1:])
        return averageAnderr(c, w, w2) 

    def nEffEntries(self):
        n = self.hw2[1:-1].sum()
        return 0 if n==0. else self.hcontent[1:-1].sum()**2/n

    def normalize(self):
        s=self.hcontent.sum()
        if s>0:
            self.hcontent/=s
            self.hw2/=s*s
    
    def draw(self, ax=None, same=False, ds='steps-post', err=False,  **opts):
        from matplotlib import pyplot as plt
        if ax is None: ax=plt.gca()
        if not same: ax.cla()
        #rr= ax.hist(axis._fXbins[:-1], axis._fXbins , weights=self.hcontent[1:-1], **opts)
        opts['ds']=ds
        opts.setdefault('label', self.title)
        #c = self.xaxis.binCenters()
        c = self.xaxis.edges[:-1]
        if err:
            rr= ax.errorbar(c, self.hcontent[1:-1], np.sqrt(self.hw2[1:-1]), **opts )
        else:
            rr = ax.plot(c, self.hcontent[1:-1],  **opts )
        #print("------> draw", repr(ax), same)
        plt.show()
        return rr
    
    # def draw(self,ax=None, **opts):
    #     from matplotlib import pyplot as plt
    #     if ax is None: ax=plt.gca()
    #     ax.hist(self.xbins[:-1], self.xbins, weights=self.hcontent, **opts)

    
class Histo2D(HistoBase):
    save_att = HistoBase.save_att+[ 'ybins']  

    def __init__(self,  hrange1=None, hrange2=None, x=None, y=None, w=None, name=None, title=None, xaxis=None, yaxis=None,
                 hcontent=None, hw2=None,):
        if xaxis is not None and yaxis is not None:
            self.xaxis = xaxis
            self.yaxis = yaxis
        elif hrange1 is not None and hrange2 is not None:
            self.xaxis = AxisSpec('xaxis',nbins=hrange1[0], range=hrange1[1])
            self.yaxis = AxisSpec('yaxis',nbins=hrange2[0], range=hrange2[1])
        else:
            self.xaxis = AxisSpec('xaxis')
            self.yaxis = AxisSpec('yaxis')
        # self.bins=bins
        # self.hrange = hrange
        if x is not None:
            if hcontent is not None:
                raise Exception("HistoBase ERROR : do not specify x AND hcontent ")
            self.fromArrays(x,y=y,w=w)
        else:
            self.hcontent = hcontent
            if hw2 is None: hw2 = hcontent
            self.hw2 = hw2
        self.name = name or (self.__class__.__name__)
        self.title = title or self.name

    def fromArrays(self, x, y, bins=None, hrange=None, w=None):
        if x is None or y is None: return        
        from matplotlib import pyplot as plt
        self.bins = np.array(bins or self.xaxis.nbins)
        self.hrange = np.array(hrange or self.xaxis.range)
        r  = plt.hist2d(x,y,bins=self.bins,range=(self.hrange, self.hrange), weights=w)
        self.hcontent=r[0]
        self.xbins = r[1]
        self.ybins = r[2]
        

    def findBin(self,x,y):
        xr = self.hrange[0]
        yr = self.hrange[1]
        x_i = self.binIndex(x,xr,self.bins[0])
        y_i = self.binIndex(y,yr,self.bins[1])
        return x_i, y_i
        
    def binContentAt(self, x, y, outC=None , _x_i=None, _y_i=None):
        xr = self.hrange[0]
        yr = self.hrange[1]
        x_i = self.binIndex(x,xr,self.bins[0],_x_i)
        y_i = self.binIndex(y,yr,self.bins[1],_y_i)
        if outC is None:
            outC=self.hcontent[x_i,y_i]
        else:
            outC[:] = self.hcontent[x_i,y_i]
        return outC
        
    def draw(self,ax=None, **opts):
        from matplotlib import pyplot as plt
        from array import array
        if ax is None: ax=plt.gca()
        ax.pcolormesh( array("d", np.linspace(self.xaxis.range[0],self.xaxis.range[1],self.xaxis.nbins+2)), array("d", np.linspace(self.yaxis.range[0],self.yaxis.range[1],self.yaxis.nbins+2)) , self.hcontent.T)
