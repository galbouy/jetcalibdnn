""" 
This sets up an analysis of histogram containers as produced by buildBHfromTrainer.py. 
 - Create a GraphicSession object
 - Load histo containers from the GraphicSession objects
 - loading analysis helper functions from responseAnalysis.py
"""
from JetCalibDNN.HistoAnalysis.BinnedArrays import *
from JetCalibDNN.HistoAnalysis import BinnedArraysGraphics as bag 

from JetCalibDNN.Trainer import Trainer

# Include a bunch of functions useful for analysis of response histograms :
exec(open('responseAnalysis.py').read())


plt.rc('xtick', labelsize=14)
plt.rc('ytick', labelsize=14)
plt.rc('font', size=16)
plt.rc('xaxis', labellocation='right')

plt.rc('legend', title_fontsize=22)
plt.rc('legend', fontsize=22)
        
    
# ********************************
# style functions
# They return a dictionnary of style (color, labels, line styles,etc...) according to the BinnedArrays they receive.
# They are called within bag.readBAFromFile() for each read BinnedArray
def atlasStyle(ba):
    if 'r_uncal' in ba.name:
        return dict(color='grey', label='no calib')
    if 'uncal' in ba.name:
        return dict(color='black', label='no calib')
    else:
        return dict(color='red', label='std calib')

def atlasStyleWithJES(ba):
    if 'r_uncal' in ba.name:
        return dict(color='grey', label='no calib')
    if 'uncal' in ba.name:
        return dict(color='black', label='no calib')
    elif 'jes' in ba.name:        
        return dict(color='blue', label='JES')
    else:
        return dict(color='red', label='JES+GSC')



# Create a GraphicSession object to hold all the histo/graph containers
gs = bag.GraphicSession() # 

inputDir = ''

# Load histos with uncalibrated and atlas-calibrated responses : 
#   Standard jets/ standard BH
gs.readBAFromFile(inputDir+'ATLASCalib_resp_pTMEtaBPS.h5', styleFunc=atlasStyleWithJES, tag='Atlas')

# mc16 QCD jets / DNN calib
gs.readBAFromFile(inputDir+'CSSKUFOSoftDropMDNAe3f0cf5396ar_e_r_mEtaGSBlockT3AtMDT6uoN700_batchSequenceMix_predQCD_directptmetarespDNN_mc16.h5', tag='cal_QCD_mc16')




# fix a few things which were not saved properly in earlier versions :
for b in gs.allBA: # allBA is the list of all BinnedArrays known to gs
    for a in b.bps.axis:
        if a.isGeV: a.title = a.title+'[GeV]'
    if isinstance(b, BinnedHistos):
        var = b.name.split('_')[0]
        if b.xaxis.name=='':
            b.xaxis.name = var
            b.xaxis.title = var

        

gs.bps = gs.allBA[0].bps
plt.ion()



            






