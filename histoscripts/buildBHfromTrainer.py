# USAGE : 

## 
# exec(open('HistoAnalysis/buildBHfromTrainer.py').read()) # IMPORTANT : this must be done BEFORE the trainer.train( ) call
## then : 
# trainer.runHistoFilling(conf.update(inputFriends=prefix+'CalibJZ_*.root', ), [eR_in_BINS_uncal,  eR_in_BINS_cal ] , outName='AntiKt4EMPFlow_atlas'+bpsTag+'_resp.h5' , beginFileTask=None)
#

#from HistoAnalysis.
from JetCalibDNN.HistoAnalysis.BinnedArrays import *
import os
import os.path


def seq(start, stop, step):
    return list(np.arange(start, stop+0.99*step, step))


#sampleType = ""
#sampleType = "fat"
#sampleType = "WZ"
#sampleType = "top"
sampleType = "HHbb"

if sampleType=="WZ":
    massBins = [70,100]
elif sampleType=="HHbb":
    massBins = [110,140]
elif sampleType=="top":
    massBins = [150, 200]
elif sampleType=="fat":
    massBins = [70, 100, 110, 140, 150, 200]
else: # Dijets
    massBins = [0, 50, 80, 100, 120, 150, 200, 250]

massBins_f = [0, 10, 20, 30, 40, 50, 80, 100, 120, 150, 200, 250]

print("   --->  Selected mass bins : ", massBins)


# Standard axis
axis_pt = AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True )
axis_m = AxisSpec("m", title="mass", edges= massBins, isGeV=True )
axis_eta = AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3., 0.2 ) , isGeV=False)
axis_eta_f = AxisSpec("etaf", title="$\eta$", edges=seq(-3.05 , 3.05, 0.1 ) , isGeV=False)

# Standard BPS
pTEtaBPS = BinnedPhaseSpace("pTEtaBPS", axis_pt, axis_eta)
pTEtaBPS.coords = ('pt_true', 'eta')

MEtaBPS = BinnedPhaseSpace("MEtaBPS", axis_m, axis_eta)
MEtaBPS.coords = ('m_true', 'eta')

pTEtafBPS = BinnedPhaseSpace("pTEtafBPS", axis_pt, axis_eta_f)
pTEtafBPS.coords = ('pt_true', 'eta')

# Complex BPS
# ********************************************
eEtaNPVBPS = BinnedPhaseSpace("eEtaNPVBPS",
                              AxisSpec("E", title="E",edges=[100, 200, 300, 500, 1000,  2000, 3000, 4000, 5000, 6000] ),
                              # eta bins
                              AxisSpec("abseta", title="|$\eta$|", edges=seq(0 , 3, 0.2 ) , isGeV=False),
                              AxisSpec("NPV", title="NPV", edges=[0,10,12,14,16,18,20,22,24,26,28,30,35,40,60] , isGeV=False),
                              )
eEtaNPVBPS.coords = ('e_true', 'abseta', 'NPV')

eEtaMuBPS = BinnedPhaseSpace("eEtaMuBPS",
                              AxisSpec("E", title="E",edges=[100, 200, 300, 500, 1000,  2000, 3000, 4000, 5000, 6000] ),
                              # eta bins
                              AxisSpec("abseta", title="|$\eta$|", edges=seq(0 , 3, 0.2 ) , isGeV=False),
                              AxisSpec("mu", title="$\mu$", edges=[0,10,20,25,30,32,34,36,38,40,45,50,60,70,100] , isGeV=False),
                              )
eEtaMuBPS.coords = ('e_true', 'abseta', 'mu')

eEtaPIDBPS = BinnedPhaseSpace("eEtaPIDBPS",
                              AxisSpec("E", title="E",edges=[100, 200, 300, 500, 1000,  2000, 3000, 4000, 5000, 6000] ),
                              # eta bins
                              AxisSpec("abseta", title="|$\eta$|", edges=seq(0 , 3, 0.2 ) , isGeV=False),
                              AxisSpec("PID", title="PID", edges=[0.5,1.5,2.5,3.5,4.5,5.5,6.5,20.5,21.5] , isGeV=False),
                              )
eEtaPIDBPS.coords = ('e_true', 'abseta', 'PID')

eEtaPIDqbgBPS = BinnedPhaseSpace("eEtaPIDqbgBPS",
                              AxisSpec("E", title="E",edges=[100, 200, 300, 500, 1000,  2000, 3000, 4000, 5000, 6000] ),
                              # eta bins
                              AxisSpec("abseta", title="|$\eta$|", edges=seq(0 , 3, 0.2 ) , isGeV=False),
                              AxisSpec("PID", title="PIDqbg", edges=[0.5,4.5,5.5,20.5,21.5] , isGeV=False),
                              )
eEtaPIDqbgBPS.coords = ('e_true', 'abseta', 'PID')

pTEtaPIDqbgBPS = BinnedPhaseSpace("pTEtaPIDqbgBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                              # eta bins
                              AxisSpec("abseta", title="|$\eta$|", edges=seq(0 , 3, 0.2 ) , isGeV=False),
                              AxisSpec("PID", title="PIDqbg", edges=[0.5,4.5,5.5,20.5,21.5] , isGeV=False),
                              )
pTEtaPIDqbgBPS.coords = ('pt_true', 'abseta', 'PID')

pTMEtaPIDqbgBPS = BinnedPhaseSpace("pTMEtaPIDqbgBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              # eta bins
                              AxisSpec("abseta", title="|$\eta$|", edges=seq(0 , 3, 0.2 ) , isGeV=False),
                              AxisSpec("PID", title="PIDqbg", edges=[-1.5,-0.5,0.5,4.5,5.5,20.5,21.5] , isGeV=False),
                              )
pTMEtaPIDqbgBPS.coords = ('e_true', 'm_true', 'abseta', 'PID')

# ********************************************
#maxpT = 4000. if sampleType=='WZ' else 3000.
pTMEtaBPS = BinnedPhaseSpace( "pTMEtaBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3., 0.2 ) , isGeV=False)
                             )
pTMEtaBPS.coords = ('pt_true', 'm_true', 'eta', )

pTMfEtaBPS = BinnedPhaseSpace( "pTMfEtaBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                              AxisSpec("mf", title="mass", edges= massBins_f, isGeV=True ),
                              AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3., 0.2 ) , isGeV=False)
                             )
pTMfEtaBPS.coords = ('pt_true', 'm_true', 'eta', )

pTMyBPS = BinnedPhaseSpace( "pTMyBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              AxisSpec("y", title="y", edges=seq(-3. , 3., 0.2 ) , isGeV=False)
                             )
pTMyBPS.coords = ('pt_true', 'm_true', 'y_true', )

pTMEtaLimitedBPS = BinnedPhaseSpace( "pTMEtaLimitedBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                              AxisSpec("m", title="mass", edges= [0,50,100,150,200], isGeV=True ),
                              AxisSpec("eta", title="$\eta$", edges=seq(-2. , 2., 0.1 ) , isGeV=False)
                             )
pTMEtaLimitedBPS.coords = ('pt_true', 'm_true', 'eta', )

pTthinMEtaBPS = BinnedPhaseSpace( "pTthinMEtaBPS",
                              AxisSpec("ptThin", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,5000,200), isGeV=True ),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3., 0.2 ) , isGeV=False)
                             )
pTthinMEtaBPS.coords = ('pt_true', 'm_true', 'eta', )

pTthickMEtaBPS = BinnedPhaseSpace( "pTthickMEtaBPS",
                              AxisSpec("ptThick", title="$p_{T}$ ", edges=seq(200,5200,500), isGeV=True ),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3., 0.2 ) , isGeV=False)
                             )
pTthickMEtaBPS.coords = ('pt_true', 'm_true', 'eta', )

EtaMpTBPS = BinnedPhaseSpace( "EtaMpTBPS",
                              AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3., 0.2 ) , isGeV=False),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,250,50)+seq(300,1000,100)+seq(1200,3000,200)+[3500,4000,5000], isGeV=True ),
                             )
EtaMpTBPS.coords = ('eta', 'm_true', 'pt_true', )



pTLMEtaBPS = BinnedPhaseSpace( "pTLMEtaBPS",
                              AxisSpec("pt", title="$p_{T}$ ", edges=seq(50,100,50)+seq(200,1000,100)+seq(1200,3000,300)+[3500,4000,5000], isGeV=True ),
                              AxisSpec("m", title="mass", edges= massBins, isGeV=True ),
                              AxisSpec("eta", title="$\eta$", edges=seq(-3. , 3., 0.2 ) , isGeV=False)
                             )
pTLMEtaBPS.coords = ('pt_true', 'm_true', 'eta', )



eMEtaBPS = BinnedPhaseSpace("eMEtaBPS",
                            AxisSpec("E",edges= [150,200, 250,350,450,600,750,900,1000,1200,1500,2000,2500, 3000, 3500, 4000, 6000]  ),
                            #AxisSpec("m",title="mass", edges=[0,30,40,50,60,70,80,100,200,400] ),
                            AxisSpec("m", title="mass", edges=massBins, isGeV=True ),
                            #AxisSpec("eta",title="$\eta$", edges=np.linspace(-3.,3.,31), isGeV=False ) , #seq(-3.,3.,0.4) )
                            AxisSpec("abseta", title="$\eta$", edges=[0,0.2,0.6, 1.0, 1.1, 1.2, 1.3,  1.4, 2., 3  ], isGeV=False)                            
                            #AxisSpec("eta",title="$\eta$", edges=symmetrize( seq(0,0.8,0.2) + seq(0.9,1.8,0.1) + seq(2,3,0.2) ), isGeV=False ) , #seq(-3.,3.,0.4) ) 
                            )
eMEtaBPS.coords = ('e_true', 'm_true','abseta')




noBPS = BinnedPhaseSpace("noBPS",
                         AxisSpec("pt", title="$p_{T}$ ", edges=[0,5000], isGeV=True ),
                         AxisSpec("eta", title="$\eta$", edges=[-3,3] , isGeV=False)
                        )
noBPS.coords = ('pt_true', 'eta')

if 1:
    # Standard response histos 
    
    #theBPS = pTMEtaBPS
    theBPS = pTMfEtaBPS
    #theBPS = pTMyBPS
    #theBPS = pTthinMEtaBPS
    #theBPS = pTthickMEtaBPS
    #theBPS = pTMEtaLimitedBPS
    #theBPS = EtaMpTBPS
    #theBPS = pTEtaBPS
    #theBPS = pTEtafBPS
    #theBPS = MEtaBPS
    #theBPS = noBPS
    #theBPS = pTLMEtaBPS
    #theBPS = eEtaNPVBPS
    #theBPS = eEtaMuBPS
    #theBPS = eMEtaBPS
    #theBPS = pTEtaBPS
    #theBPS = eEtaPIDBPS
    #theBPS = eEtaPIDqbgBPS
    #theBPS = pTMEtaPIDqbgBPS
    #theBPS = pTEtaPIDqbgBPS


    
    bpsTag = theBPS.axisTag()
    mR_in_BINS_dnn = BinnedHistos( "mR_in_"+bpsTag+"_dnn", theBPS, (300,(0.01,3)) )
    mR_in_BINS_dnn.coordNames = theBPS.coords
    mR_in_BINS_dnn.valueName = 'r_dnn_m'
    
    eR_in_BINS_dnn = BinnedHistos( "eR_in_"+bpsTag+"_dnn", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_dnn.coordNames = theBPS.coords
    eR_in_BINS_dnn.valueName = 'r_dnn_e'    
    
    mR_in_BINS_cal = BinnedHistos( "mR_in_"+bpsTag+"_cal", theBPS, (300,(0.01,3)) )
    mR_in_BINS_cal.coordNames = theBPS.coords
    mR_in_BINS_cal.valueName = 'r_cal_m'
    
    eR_in_BINS_cal = BinnedHistos( "eR_in_"+bpsTag+"_cal", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_cal.coordNames = theBPS.coords
    eR_in_BINS_cal.valueName = 'r_cal_e'

    mR_in_BINS_uncal = BinnedHistos( "mR_in_"+bpsTag+"_uncal", theBPS, (300,(0.01,3)) )
    mR_in_BINS_uncal.coordNames = theBPS.coords
    mR_in_BINS_uncal.valueName = 'r_raw_m'
    
    eR_in_BINS_uncal = BinnedHistos( "eR_in_"+bpsTag+"_uncal", theBPS, (240,(0.4,1.8)) )
    eR_in_BINS_uncal.coordNames = theBPS.coords
    eR_in_BINS_uncal.valueName = 'r_raw_e'
    
    eR_in_BINS_ratio = BinnedHistos( "eR_in_"+bpsTag+"_ratio", theBPS, (300,(0,2)) )
    eR_in_BINS_ratio.coordNames = theBPS.coords
    eR_in_BINS_ratio.valueName = "r_e_ratio"

    mR_in_BINS_ratio = BinnedHistos( "mR_in_"+bpsTag+"_ratio", theBPS, (300,(0,2)) )
    mR_in_BINS_ratio.coordNames = theBPS.coords
    mR_in_BINS_ratio.valueName = "r_m_ratio"

    eR_in_BINS_dnn_cal = BinnedHistos2D("eR_in_"+bpsTag+"_dnn_cal", theBPS, (100,(0.6,1.1)), (100,(0.75,0.95)) )
    eR_in_BINS_dnn_cal.coordNames = theBPS.coords
    eR_in_BINS_dnn_cal.valueName = ""
    eR_in_BINS_dnn_cal.valuesName = ["r_dnn_e", "r_cal_e"]
    
    mR_in_BINS_dnn_cal = BinnedHistos2D("mR_in_"+bpsTag+"_dnn_cal", theBPS, (100,(0.6,1.5)), (100,(0.75,1.5)) )
    mR_in_BINS_dnn_cal.coordNames = theBPS.coords
    mR_in_BINS_dnn_cal.valueName = ""
    mR_in_BINS_dnn_cal.valuesName = ["r_dnn_m", "r_cal_m"]

    pTR_in_BINS_dnn = BinnedHistos( "pTR_in_"+bpsTag+"_dnn", theBPS, (300,(0,2)) )
    pTR_in_BINS_dnn.coordNames = theBPS.coords
    pTR_in_BINS_dnn.valueName = "r_dnn_pt"

    pTR_in_BINS_cal = BinnedHistos( "pTR_in_"+bpsTag+"_cal", theBPS, (300,(0,2)) )
    pTR_in_BINS_cal.coordNames = theBPS.coords
    pTR_in_BINS_cal.valueName = "r_cal_pt"

    
    m_in_BINS_dnn = BinnedHistos( "m_in_"+bpsTag+"_dnn", theBPS, (300,(0.,250)) )
    m_in_BINS_dnn.coordNames = theBPS.coords
    m_in_BINS_dnn.valueName = 'm_dnn'

    e_in_BINS_dnn = BinnedHistos( "e_in_"+bpsTag+"_dnn", theBPS, (300,(0.,3000)) )
    e_in_BINS_dnn.coordNames = theBPS.coords
    e_in_BINS_dnn.valueName = 'e_dnn'

    m_in_BINS_cal = BinnedHistos( "m_in_"+bpsTag+"_cal", theBPS, (300,(0.,250)) )
    m_in_BINS_cal.coordNames = theBPS.coords
    m_in_BINS_cal.valueName = 'm_cal'

    e_in_BINS_cal = BinnedHistos( "e_in_"+bpsTag+"_cal", theBPS, (300,(0.,3000)) )
    e_in_BINS_cal.coordNames = theBPS.coords
    e_in_BINS_cal.valueName = 'e_cal'

    m_in_BINS_uncal = BinnedHistos( "m_in_"+bpsTag+"_uncal", theBPS, (300,(0.,250)) )
    m_in_BINS_uncal.coordNames = theBPS.coords
    m_in_BINS_uncal.valueName = 'm_reco'

    e_in_BINS_uncal = BinnedHistos( "e_in_"+bpsTag+"_uncal", theBPS, (300,(0.,3000)) )
    e_in_BINS_uncal.coordNames = theBPS.coords
    e_in_BINS_uncal.valueName = 'e_reco'



print('   --->  Selected BPS : ', theBPS.name)

def adaptVariables(config, bhList):
    needDNNPred = any( 'dnn' in cont.name for cont in bhList)
    needRespRatio = any( 'ratio' in cont.name for cont in bhList)
    needpT = any( 'pT' in cont.name for cont in bhList)
    needATLASCal = any( 'r_cal_pt' in cont.valueName for cont in bhList)

    # collect all variables from phase-spaces coordinates and histo variables :
    histoVars = set( sum( (bh.bps.coords for bh in  bhList), () ) )
    for bh in bhList:
        if "dnn_cal" in bh.name:
            histoVars.add(bh.valuesName[0])
            histoVars.add(bh.valuesName[1])
        else:
            histoVars.add(bh.valueName)

    if needRespRatio:
        needDNNPred = True
        for v in ['r_e_ratio', 'r_m_ratio']:
            histoVars.discard(v)
        for v in ['r_cal_e', 'r_cal_m']:
            histoVars.add(v)

    if needDNNPred:
        # remove 'r_dnn_X' vars : they will be predicted and need not to be prepared from the inputs
        for v in ['r_dnn_e', 'r_dnn_m']:
            histoVars.discard(v)
    
    if needpT:
        # remove 'r_dnn_pt' vars : they will be predicted and need not to be prepared from the inputs
        histoVars.discard('r_dnn_pt')
    if needATLASCal:    
        histoVars.discard('r_cal_pt')
        histoVars.add('r_cal_e')
        histoVars.add('r_cal_m')

    histoVars.discard("e_dnn")
    histoVars.discard("m_dnn")
    histoVars.add('pt_true')
    conf.additionalVars += list(histoVars)
    print(conf.additionalVars)
    return config
    


adaptConfigFunc = adaptVariables

def runHistoFilling(self, config, bhList, reload=0,  beginFileTask=None, outName=None, specialTag='', nFile=-1, inputMode='reco', useWeights=False, pt_sel=False):

    # adapt config to histo building :
    config = adaptConfigFunc( config, bhList )

    # force config to be in  prediction mode
    config.mode= inputMode+':predict'

    # setup the Trainer :
    self.train(config, reload = reload, fitSequence=None)

    if outName is None:
        tag = prefix if reload==1 else prefix+str(reload)
        outName = trainer.outName(specialTag or config.specialTag).replace('.h5', bpsTag+'respDNN.h5')
    
    print("*********Running histo filling ********")
    print(" will ouput to ",outName)
    print()
    # call the actual histo filling procedure :
    if useWeights!=None:
        self.fillBinnedHistos( bhList, useWeights=useWeights, beginFileTask=beginFileTask, outName=outName, nFile=nFile, pt_sel=pt_sel)
    else:
        self.fillBinnedHistos( bhList, useWeights=bool(config.sampleWeights), beginFileTask=beginFileTask, outName=outName, nFile=nFile, pt_sel=pt_sel)

Trainer.runHistoFilling = runHistoFilling                           
    

def fillBinnedHistos(self, bhList, useWeights=True, beginFileTask=None, outName=None, nFile=-1, pt_sel=False ):
    self.gen.reset()
    gen = self.gen

    mode = self.config.mode.split(':')[1]
    if mode not in ('predict'):
        print('ERROR !! use predict mode, not ', self.config.mode)
        return
    if not isinstance(bhList, list): bhList = [bhList]

    if useWeights :
        for c in gen.allChains():
            c.weights_var.normalize = False


    
    # group by bps to factorize indices calculations
    bhDict = dict()
    for bh in bhList:
        bhDict.setdefault( (bh.bps,)+bh.coordNames, []).append(bh)
        
        
    maxDimBPS = max( bhList , key=lambda bps:bh.bps.nDim() ).bps

    # instantiate one array of N-dim indices suitable to the bigges BPS & input data
    binIndices = maxDimBPS.buildNCoordinates(gen.chain.max_entries)
    nFile = len(gen.input_files) if nFile==-1 else nFile
    for i in range(nFile):
        hIndices = np.zeros( (gen.chain.max_entries,) ,dtype=int)
        gen.loadFile(i, noTransform=True)
        gen.transform_current_sample() # make sure the ratios are calculated
        gen.untransform_current_sample() # make sure we revert to nominal variables

        
        allVars = self.allVariables()
        if callable(beginFileTask): beginFileTask(self)
        
        N = gen.current_file.Nentries
        if useWeights:
            w0 = gen.chain.weights_var.array[:N]
            #w0 = gen.chain.weights_var.transformed_array()[:N]

        if pt_sel:
            valid = np.where(allVars["pt_true"].array[:N]>200)
            hIndices = hIndices[valid]

        #loop over the BinnedHistos which share the same bps & coordinates 
        for _, bhL in bhDict.items():            
            bps = bhL[0].bps 
            print("******************** Fill for BPS ",bps.name)
            # compute the common index coordinates for these BinnedHistos :
            if pt_sel:
                coords = [ allVars[v].array[:N][valid] for v in bhL[0].coordNames]
            else:
                coords = [ allVars[v].array[:N] for v in bhL[0].coordNames]
            tupleI, validCoordI = bps.findValidBins(coords, binIndices[:bps.nDim(),:len(coords[0])] )

            # Then fill these BH with their values at the calculated coordinates
            w = w0[validCoordI] if useWeights else 1.
            #print("weights : ", w[:7])
            w2=w*w
            for bh in bhL:
                print("******************** ---> Fill for BinnedHistos ",bh.name)
                if "dnn_cal" in bh.name:
                    values1 = allVars[bh.valuesName[0]].array
                    values2 = allVars[bh.valuesName[1]].array
                    print('---> ',values1, values2)
                    bh.fillAtIndices( values1[:N], values2[:N], validCoordI, tupleI, w, w2,)# hIndices=hIndices[:N])
                else:
                    values = allVars[bh.valueName].array[:N]
                    hIndices = hIndices[:N]
                    print('---> ',values)
                    
                    if pt_sel:
                        print('---> pT true sel ', allVars["pt_true"].array[valid])
                        values = values[valid]
                    
                    bh.fillAtIndices( values, validCoordI, tupleI, w, w2, hIndices=hIndices)

    if outName:
        print("Saving Binned Histo into ", outName)
        if outName[-2:]=='h5':
            self.saveBinnedHistosAsH5(bhList, outName)
        else:
            self.saveBinnedHistosAsROOT(bhList, outName)



        
def predOnly(trainer, pred_vname_e= 'r_dnn_e', pred_vname_m='r_dnn_m'):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.

    print( 'UUUUUUUU ', r[:10,0] )
    # assigne the prediction to a dummy Variable onto the current list of vars :
    trainer.gen.chain.allVars[pred_vname_e] = _tmp(r[:,0])
    
    hasM = len(trainer.config.originalTargets) > 1
    
    if hasM:
        trainer.gen.chain.allVars[pred_vname_m] = _tmp(r[:,trainer.config.nPredictedParam])

        
def predCalib(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
    r_cal_e = r[:,0]
    hasM = len(trainer.config.originalTargets) > 1

    if hasM:
        r_cal_m = r[:,trainer.config.nPredictedParam]

        print("r_m", trainer.gen.chain.allVars.r_m.array[:7])
        print("r_cal_m", r_cal_m[:7])

    if trainer.config.optimTag != 'directSF':
        # perform r_cal = r / r_cal in-place :
        r_cal_e = np.reciprocal( r_cal_e, out=r_cal_e)
        if hasM:
            r_cal_m = np.reciprocal( r_cal_m, out=r_cal_m)  

    r_cal_e *= trainer.gen.chain.allVars.r_e.array[:N]
    trainer.gen.chain.allVars.r_dnn_e = _tmp(r_cal_e)
    if hasM:
        r_cal_m *= trainer.gen.chain.allVars.r_m.array[:N]
        print("r_cal_m", r_cal_m[:7])    
        trainer.gen.chain.allVars.r_dnn_m = _tmp(r_cal_m)

def createRespRatio(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
    r_dnn_e = r[:,0]
    print("r_dnn_e", r_dnn_e[:7])
    hasM = len(trainer.config.originalTargets) > 1

    if hasM:
        r_dnn_m = r[:,trainer.config.nPredictedParam]

        #print("r_m", trainer.gen.chain.allVars.r_m.array[:7])
        print("r_dnn_m", r_dnn_m[:7])

    r_cal_e = trainer.gen.chain.allVars.r_cal_e.array[:N]
    r_cal_e = np.reciprocal( r_cal_e, out=r_cal_e)
    r_cal_e *= trainer.gen.chain.allVars.r_e.array[:N]
    print("r_std_e", r_cal_e[:7])
    
    if hasM:
        r_cal_m = trainer.gen.chain.allVars.r_cal_m.array[:N]
        r_cal_m = np.reciprocal( r_cal_m, out=r_cal_m)
        r_cal_m *= trainer.gen.chain.allVars.r_m.array[:N]
        print('r_std_m', r_cal_m[:7])

    r_cal_e = np.reciprocal( r_cal_e , out=r_cal_e)
    r_dnn_e *= r_cal_e
    print("ratio_e", r_dnn_e[:7])
    trainer.gen.chain.allVars.r_e_ratio = _tmp(r_dnn_e)
    if hasM:
        r_cal_m = np.reciprocal( r_cal_m , out=r_cal_m)
        r_dnn_m *= r_cal_m
        print("ratio_m", r_dnn_m[:7])   
        trainer.gen.chain.allVars.r_m_ratio = _tmp(r_dnn_m)


def getCalib(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
    r_dnn_e = r[:,0]
    print("r_dnn_e", r_dnn_e[:7])
    hasM = len(trainer.config.originalTargets) > 1

    if hasM:
        r_dnn_m = r[:,trainer.config.nPredictedParam]

        #print("r_m", trainer.gen.chain.allVars.r_m.array[:7])
        print("r_dnn_m", r_dnn_m[:7])

    r_cal_e = trainer.gen.chain.allVars.r_cal_e.array[:N]
    r_cal_e = np.reciprocal( r_cal_e, out=r_cal_e)
    r_cal_e *= trainer.gen.chain.allVars.r_e.array[:N]
    print("r_std_e", r_cal_e[:7])
    
    if hasM:
        r_cal_m = trainer.gen.chain.allVars.r_cal_m.array[:N]
        r_cal_m = np.reciprocal( r_cal_m, out=r_cal_m)
        r_cal_m *= trainer.gen.chain.allVars.r_m.array[:N]
        print('r_std_m', r_cal_m[:7])

    trainer.gen.chain.allVars.r_dnn_e = _tmp(r_dnn_e)
    trainer.gen.chain.allVars.r_cal_e = _tmp(r_cal_e)
    if hasM:
        trainer.gen.chain.allVars.r_dnn_m = _tmp(r_dnn_m)
        trainer.gen.chain.allVars.r_cal_m = _tmp(r_cal_m)


def predCalibPT(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
    r_cal_e = r[:,0]
    hasM = len(trainer.config.originalTargets) > 1

    if hasM:
        r_cal_m = r[:,trainer.config.nPredictedParam]

        print("r_m", trainer.gen.chain.allVars.r_m.array[:7])
        print("r_cal_m", r_cal_m[:7])
    
    r_cal_e = np.reciprocal( r_cal_e, out=r_cal_e)
    if hasM:
        r_cal_m = np.reciprocal( r_cal_m, out=r_cal_m)  

    eta = trainer.gen.chain.allVars.eta.array[:N]
    print('eta', eta[:7])
    e = trainer.gen.chain.allVars.e_reco.array[:N]
    print('e', e[:7])
    m = trainer.gen.chain.allVars.m_reco.array[:N]
    print('m', m[:7])
    
    pT_cal = np.sin(2*np.arctan(np.exp(-eta)))*np.sqrt((e*r_cal_e)**2 + (m*r_cal_m)**2)
    print('pT_cal', pT_cal[:7])
    
    pT_true = trainer.gen.chain.allVars.pt_true.array[:N]
    print('pT_true', pT_true[:7])
    pT_true_inv = np.reciprocal( pT_true)
    trainer.gen.chain.allVars.r_dnn_pt = _tmp(pT_cal*pT_true_inv)

def calibPT(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    
    eta = trainer.gen.chain.allVars.eta.array[:N]
    print('eta', eta[:7])
    e_cal = trainer.gen.chain.allVars.e_cal.array[:N]
    print('e', e_cal[:7])
    m_cal = trainer.gen.chain.allVars.m_cal.array[:N]
    print('m', m_cal[:7])
    
    pT_cal = np.sin(2*np.arctan(np.exp(-eta)))*np.sqrt(e_cal**2 + m_cal**2)
    print('pT_cal', pT_cal[:7])
    
    pT_true = trainer.gen.chain.allVars.pt_true.array[:N]
    print('pT_true', pT_true[:7])
    pT_true_inv = np.reciprocal( pT_true)
    trainer.gen.chain.allVars.r_cal_pt = _tmp(pT_cal*pT_true_inv)




def saveBinnedHistosAsROOT(self, bhList, fname):
    f = uproot.recreate(fname)
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveAsROOT(f)
    f.close()

def saveBinnedHistosAsH5(self, bhList, fname):
    if os.path.exists( fname) : os.remove(fname)        
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveInH5(fname)

    if self.net is not None:
        self.saveTrainSummary(fname)
    
Trainer.fillBinnedHistos = fillBinnedHistos
Trainer.saveBinnedHistosAsH5 = saveBinnedHistosAsH5
Trainer.saveBinnedHistosAsROOT = saveBinnedHistosAsROOT



print("trainer.runHistoFilling(conf.update(inputFriends=prefix+'CalibJZ_*.root', ), [eR_in_BINS_uncal,  eR_in_BINS_cal ] , outName='AntiKt4EMPFlow_atlas'+bpsTag+'_resp.h5' , beginFileTask=predCalib)" ) 

