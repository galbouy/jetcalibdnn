""" 
Histogram fitting and plotting functions.

Many of the plotting functions here are meant to be used with BinnedHistos containers build from
trainer.fillBinnedHistos(). 
They are added as methods to the GraphicSession class and operate on list


"""
#import numexpr as ne
import numpy as np
import os, glob

import h5py
import matplotlib
import matplotlib.pyplot as plt


from JetCalibDNN.HistoAnalysis import BinnedArraysGraphics as bag 
from JetCalibDNN.HistoUtils import Histo1D



# ################################################################
# ################################################################
# Fit & mode calculations
from scipy.optimize import curve_fit
import ROOT

class RespWrapper:
    """ Class to compute response mode or mean from multiple binned histograms containing E/M response
        Initialised with a list of BH and a fit function, then apply a response calculation 
        Plot features are available (plotRespHisto, globalPlotFct)
        Save features are available (saveFigure, saveManyPlots, createPDF)
    """

    def __init__(self, bh_list, func=None):
        # Set main variables
        self.bh_list=bh_list
        self.fitFunc = func
        self.params = {}                     # to store fit params
        self.respName = bh_list[0].name[0]   # get response name ("e" or "m" or "pT")
        if self.respName=='e':
            self.fullRespName = 'Energy' 
        elif self.respName=='m':
            self.fullRespName = 'Mass' 
        else:
            self.fullRespName = 'pT' 

        self.loadBH()

        self.bps = bh_list[0].bps
        self.axis = self.bps.axis

        # Set saving variables
        self.saveFig = False
        self.outDir = 'savedFigures/'
        self.outExt = '.png'
        self.savedPlots = []                 # to store filenames of the saved plots 

        self.init_fig()

        # Set fit options : initial guess and bounds of the parameters
        if callable(self.fitFunc):
            if self.fitFunc==gauss:
                self.opts = dict( p0=[1, 1, 0.2], bounds=([0.0,0.0,0.0], [np.inf, 2., 5.]))
            if self.fitFunc==gauss2side:
                self.opts = dict( p0=[1, 1, 0.2,0.2], bounds=([0.0,0.5,0.0,0.0], [np.inf, 1.5, 5.,5.]))


    def init_fig(self, ): 
        self.color = ['b', 'r', 'k', 'g', 'c', 'm', 'y']
        self.ROOTcolor = [4, 8, 2, 6, 9, 28, 46]
        self.marker = ['v', '^', '<', '>', '|', 'x']
        self.ROOTmarker = [21, 22, 20, 23, 33, 34, 47, 43]
        self.ROOTline = [1, 3, 2, 1, 1, 1, 1, 1]
        #self.fig = plt.figure(figsize=(15,5))
        for axis in self.axis:
            if axis.name=='eta': 
                axis.fancy_name = '$\eta$'
                axis.unit = ''
            elif axis.name=='abseta': 
                axis.fancy_name = '|$\eta$|'
                axis.unit = ''
            elif 'pt' in axis.name: 
                axis.fancy_name = '$p_{T}$'
                axis.unit = 'GeV'
            elif axis.name=='m': 
                axis.fancy_name = axis.name
                axis.unit = 'GeV'
            else:
                axis.fancy_name = axis.name
                axis.unit = ''


    def resetFig(self, ):     # Init figure if no window already open or clear figure
        try:
            self.fig.clf()
        except:
            self.fig = plt.figure(figsize=(15,5))
        self.ax = plt.gca()

    def saveFigure(self, name):
        self.fig.savefig(self.outDir + name + self.outExt)
        self.savedPlots.append(name)

    def saveBH(self, ):
        print("Saving modified binned histograms ...")
        for bh in self.bh_list:
            bh.saveInH5()

    def loadBH(self, ):
        print("Loading binned histograms ...")
        for bh in self.bh_list:
            bh.loadContent()


    def getAllRespInBH(self, drawFit=False, drawFitROOT=False, coord=None, smooth=True, reFit=False, doNorm=False, decrease=False,thin=False, N=None):
        # Compute response/IQR/error for each histo in multiple binned histogram (bh_list) 
        # Each will be added as 'friend' arrays to each bh

        if callable(self.fitFunc): self.respType = 'mode'
        elif self.fitFunc==None: self.respType = 'mean'
        else: 
            print("Unknown fit function")
            return

        self.nBadFit = 0
        for bh in self.bh_list:
            try:
                resp = bh.resp
                if reFit==True: raise RedoFit
                if coord!=None: raise RedoFit
                print("Binned histos ", bh.name + bh.tag, " already processed ... will not do resp calculation")
                continue
            except:
                print("response calculation for histos in ", bh.name + bh.tag, " with response fit = ", self.respType)
                print("Binned phase space -> ", bh.bps.coords," shape : ", bh.bps.shape())
            
            resp = bh.addFriend( 'resp' )
            IQR = bh.addFriend( 'IQR' )
            resolution = bh.addFriend( 'resolution' )
            error = bh.addFriend( 'error' )

            if doNorm: bh.normalize()

            coords = bh.bps.indices[()] if coord==None else [coord]
            nbins = bh.bps.nbins
            i=1
            for c in coords:
                #####
                if i%10==0: print("Processing ... ", "{:.2f}%".format(i/nbins *100), end='\r', flush=True)
                i+=1
                #####
                c = tuple(c)
                h = bh.histoAt( c )
                h_array = h.hcontent[1:-1]
                h_array[np.isnan(h_array)] = 0
                centers = h.xaxis.binCenters()

                if np.sum(h_array)==0:
                    resp[c], IQR[c], error[c] = 0, 1, 1
                    #resp[c], IQR[c], error[c] = None, None, None
                    continue

                # Get inter quartile range
                qI = quantiles(h_array, [0.16,0.84] )
                q1,q2 = h.xaxis.binCenter(qI+1) # +1 because in histo bin 0 is underflow
                IQR[c] = q2-q1

                # If no fit function or not enough stat -> take directly mean of histo as resp
                if self.fitFunc == None or np.sum(h_array)<100:  
                    resp[c] = h.mean()
                    error[c] = np.sqrt(np.average((centers - resp[c])**2, weights=h_array))

                    if drawFit:
                        self.resetFig()
                        h.draw()
                        plt.plot([resp[c],resp[c]], [0,max(h_array)], c='r', lw=1, label='mean')
                        plt.legend()
                        plt.pause(1)
                    continue

                # If there is a fit function -> apply curve fit
                if callable(self.fitFunc) :
                    # Smooth hcontent or not
                    h_fit = h_array
                    if smooth==True: h_fit = smoothing(h)
                    hw2 = h.hw2[1:-1]
                    hw2 = [h if h!=0 else 1 for h in hw2]
                    
                    # Select fitting zone
                    mode_index = np.argmax(h_fit)          # index of max value of h_array == mode
                    mode = centers[mode_index]               # get the mode
                    
                    std = np.sqrt(np.average((centers - mode)**2, weights=h_fit))   # get standard deviation
                    std_nbins = int(std/h.xaxis.binWidth())                                # get the equivalent of std in number of bins
                    if N==None:
                        N_list = [0.75,1.,1.25,1.5,2.,2.5,3.,3.5,4.,4.5,5.]
                        if decrease : N_list = [0.75,0.65,0.5,0.35,0.25,0.125,0.10]
                        if thin : N_list = [0.1,0.125,0.25,0.35,0.5,0.65,0.75]
                    else:
                        N_list=[N]

                    # Loop on the range size for fitting until the fit is ok
                    for n in N_list:
                        i_min = mode_index - int(n*std_nbins)
                        i_max = mode_index + int(n*std_nbins) +1
                        if i_min<0: i_min = 0
                        if i_max>=len(centers): i_max = len(centers)

                        try:
                            params, cov = curve_fit(self.fitFunc, centers[i_min:i_max], h_fit[i_min:i_max], sigma=hw2[i_min:i_max], **self.opts)
                            
                            if drawFit:
                                self.resetFig()
                                h.draw()
                                if smooth==True: plt.plot(centers, h_fit, c='k', label='Smoothed histo')
                                plt.plot(centers[i_min:i_max], self.fitFunc(centers[i_min:i_max], *params), c='r', lw=1, label='gaussian fit')
                                plt.plot([params[1],params[1]], [0,max(h_fit)], c='r', lw=1, label='mode')
                                plt.title("Response fit at c = " + str(c))
                                plt.legend()
                                plt.pause(1)
                            if drawFitROOT:
                                self.drawRespFitROOT(coord, h, centers, h_fit, centers[i_min:i_max], self.fitFunc(centers[i_min:i_max], *params), mode, doSave=True)

                            if self.checkFit(params, std, np.mean(h_fit[i_min:i_max]), h_fit[mode_index])==False: raise BadFit
                            self.params[bh.name+bh.tag,c] = params
                            resp[c] = params[1]
                            error[c] = np.sqrt(np.diag(cov)[1])
                            
                            
                            
                            break
                    
                        except:
                            #print("fit Failed at c=",c, " with N = ",n, " ... will try with N+0.5")
                            if n==N_list[-1]:
                                print("fit Bad/Failed at c=",c," ... will take mean")
                                self.nBadFit+=1
                                resp[c] = np.average(centers[i_min:i_max], weights=h_fit[i_min:i_max])
                                error[c] = np.sqrt(np.average((centers - resp[c])**2, weights=h_array))
                
                resolution[c] = IQR[c]/resp[c]

        print("\n Done !!!")                
        return
            

    def checkFit(self, params, std, mean_bins, h_max):
        n = len(params)
        if params[0] < mean_bins:
            #if params[0] < 0.6*h_max:
            return False
        if n>3:
            #if params[2]*5<params[3] or params[3]*5<params[2]:
            #    return False
            if params[2]>2*std or params[3]>2*std:
                return False
            return True
        else:
            if params[2]>2*std:
                return False
            return True



    def drawRespHisto(self, coord, drawFit=False, saveFig = False, plotName = None, doNorm=False):
        self.resetFig()
        c_i = 0
        hist_list = []
        for bh in self.bh_list:
            h = bh.histoAt( coord )
            h_array = h.hcontent[1:-1]
            centers = h.xaxis.binCenters()
            label = bh.name.split('_')[-1] + ' ' + bh.tag 
            if bh.name.split('_')[-1]=='dnn' : label = 'DNN calibration'
            elif bh.name.split('_')[-1]=='cal' : label = 'Standard calibration'
            elif bh.name.split('_')[-1]=='uncal' : label = 'Uncalibrated'

            if len(self.bh_list)==1 : label = ""
            hist_list.append(dict(hist = h, label = label, color = self.ROOTcolor[c_i], style = self.ROOTline[c_i]))
            plt.step( centers, h_array, where='mid', c=self.color[c_i], lw=1, label=label)

            if drawFit:
                try:
                    plt.plot(centers, self.fitFunc(centers, *self.params[bh.name+bh.tag,coord]), c=self.color[c_i], lw=1)
                    plt.plot([self.params[bh.name+bh.tag,coord][1],self.params[bh.name+bh.tag,coord][1]], [0,max(h_array)], c=self.color[c_i], lw=1)
                except:
                    plt.plot([centers[np.argmax(h_array)],centers[np.argmax(h_array)]], [0,max(h_array)], c=self.color[c_i], lw=1)
                
            c_i+=1
        title = self.fullRespName + ' Response hist | '
        interval = ''
        for i in range(len(self.axis)):
            interval += self.axis[i].fancy_name + '$\in$[' + str(np.round(self.axis[i].edges[coord[i]],1)) + ',' + str(np.round(self.axis[i].edges[coord[i]+1],1)) + ']'
            interval += ' GeV' if self.axis[i].isGeV else ''
            if i!=len(self.axis)-1: interval += ';'
        title += interval
        plt.title(title)
        plt.xlabel('R')
        plt.ylabel('N event')
        self.ax.xaxis.set_label_coords(1.0, -0.05)
        plt.legend()

        plotName = 'resphist_' + str(coord) if plotName==None else plotName
        
        if self.saveFig==True:
            self.saveFigure('resphist_' + str(coord))


    def __getPlotOpts__(self, plot='resp', bins = [None], yrange = None):
        n_axis = len(self.axis)
        n_bins = len(bins)
        if n_bins != n_axis : 
            print("Too much or not enough bins coords")
            return [None]*10

        n_None = 0
        index_None = []
        index_coord = []
        slices = []
        suffix = ''
        for i in range(n_bins):
            if bins[i]==None : 
                n_None+=1
                index_None.append(i)
                if self.axis[i].name=='pt':
                    slices.append(slice(3,None))
                elif self.axis[i].name=='eta':
                    slices.append(slice(4,self.axis[i].nbins-4))
                elif self.axis[i].name=='abseta':
                    slices.append(slice(0,self.axis[i].nbins-4))
                else:
                    slices.append(slice(0,None))
            elif bins[i] is True:
                suffix = self.axis[i].name
                slices.append(slice(0,1))
            else:
                index_coord.append(i)
                slices.append(slice(bins[i],bins[i]+1))

        interval = '| '
        in_bin = ''
        for i in index_coord:
            interval += self.axis[i].fancy_name + '$\in$[' + str(np.round(self.axis[i].edges[bins[i]],1)) + ',' + str(np.round(self.axis[i].edges[bins[i]+1],1)) + ']'
            interval += ' GeV' if self.axis[i].isGeV else ''
            if i!=len(self.axis)-1: interval += ';'
            in_bin += self.axis[i].name + str(np.round(self.axis[i].binCenter(bins[i]+1),1))

        if n_None>2 or n_None<1:
            print("Not enough specified coords")
            return [None]*10
        if n_None<1:
            print("Too much specified coords")
            return [None]*10

        if plot=='resp' or plot=='IQR' or plot=='resolution':
            if plot=='resp':
                f = 'R'
                ylabel = 'R'
            if plot=='IQR':
                f = 'IQR'
                ylabel = 'IQR'
            if plot=='resolution':
                f = r'$\sigma$'
                ylabel = r'$\sigma$'


        elif plot=='meanresp' or plot=='meanresolution' or plot=='resp_grad':
            if plot=='meanresp':
                f = r'$\overline{R}_{' + suffix + '}$'
                ylabel = r'$\overline{R}_{' + suffix + '}$'
            if plot=='meanresolution':
                f = r'$\overline{\sigma}_{' + suffix + '}$'
                ylabel = r'$\overline{\sigma}_{' + suffix + '}$'
            if plot=='resp_grad':
                f = r'$\nabla_{' + suffix + '}$ R'
                ylabel = r'$\nabla_{' + suffix + '}$ R'
        
        elif plot=='ratio':
            f = 'Resp Ratio'
            ylabel = 'Resp ratio'

        else:
            print("Cannot plot : ", plot)
            return [None]*10

        f_axis = self.axis[index_None[0]]
        xlabel = f_axis.fancy_name
        title = self.fullRespName + ' ' + f + '(' + xlabel + ') ' + interval
        xlabel += ' GeV' if f_axis.isGeV else ''
        if n_None==2 :
            sec_axis = self.axis[index_None[1]]
            list_bins = np.linspace(0, sec_axis.nbins-1, sec_axis.nbins)
            if sec_axis.name=='pt':
                list_bins = list_bins[3:]
        else:
            sec_axis = self.axis[-1]
            list_bins = [bins[-1]]

        if plot=='resp' or plot=='meanresp':
            plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [1, 1], lw=1, c='k')
            plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [1.01, 1.01], lw=0.5, c='b')
            plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [0.99, 0.99], lw=0.5, c='b')
            if f_axis.name=='pt': plt.plot([200, 200], [0, 2], lw=1, c='k')
            #if self.respName=='e': plt.ylim(bottom = 0.85, top = 1.05)
            if self.respName=='e' or self.respName=='p': plt.ylim(bottom = 0.8, top = 1.05)
            if self.respName=='p': plt.ylim(bottom = 0.8, top = 1.2)
            if self.respName=='m': 
                #plt.ylim(bottom = 0.8, top = 1.2)
                plt.ylim(bottom = 0.7, top = 1.5)
                plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [1.05, 1.05], lw=0.5, c='b')
                plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [0.95, 0.95], lw=0.5, c='b')
        if plot=='IQR' or plot=='meanIQR' or plot=='resolution' or plot=='meanresolution':
            if self.respName=='e' or self.respName=='p': plt.ylim(bottom=0., top=0.5)
            if self.respName=='m': plt.ylim(bottom=0., top=1.5)
        if plot=='resp_grad':
            plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [0, 0], lw=1, c='k')
            plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [0.001, 0.001], lw=0.5, c='b')
            plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [-0.001, -0.001], lw=0.5, c='b')
            plt.ylim(bottom=-0.01, top=0.01)
        if plot=='ratio':
            plt.plot([f_axis.binCenters()[0], f_axis.binCenters()[-1]], [1, 1], lw=1, c='k')
            plt.ylim(bottom = 0.8, top = 1.05)
            if f_axis.name=='pt': plt.plot([200, 200], [0, 2], lw=1, c='k')

        if yrange!=None: plt.ylim(bottom = yrange[0], top = yrange[1])

        return f_axis, xlabel, ylabel, title, sec_axis, list_bins, in_bin, slices, index_None, suffix



    def globalPlotFct(self, plot='resp', bins = [None], yrange = None, saveFig = False, plotName = None, sec_bins=None):
        # Plot function :
        # Input : plot = what you want to plot, can be resp, meanResp, resp_grad, IQR, meanIQR, resolution, meanresolution
        #         bins = a list of coords to know what to plot
        #                if basic plot (resp,IQR,...) list must contain one None and specific coordinates for other axis -> will plot resp/IQR/resolution as a function of the axis corresponding to the None position in bins
        #                if plot is mean/grad list must contain one None, one True and specific coords for other axis -> will plot mean/grad along the axis corresponding to the True position and as a function of the None axis   
        # Ex : to plot mean resp along eta axis as a function of pt axis with m_bin = 0
        #      bins = [None,0,True]
               
        self.resetFig()

        c_i = 0
        for bh in self.bh_list:
            self.axis = bh.bps.axis
            self.init_fig()

            f_axis, xlabel, ylabel, title, sec_axis, list_bins, in_bin, slices, index_None, suffix = self.__getPlotOpts__(plot, bins, yrange)
            if f_axis==None : return
            
            err = bh.error
            if plot=='resp_grad' : err = getattr(bh, plot + '_err' + suffix)
            f = getattr(bh, plot+suffix)
            
            if sec_bins!=None: list_bins=sec_bins

            for sec_bin in list_bins[:len(self.color)]:
                if len(index_None)==2: slices[index_None[1]] = slice(int(sec_bin),int(sec_bin)+1) 
                f_to_plot = f[tuple(slices)]
                f_to_plot = f_to_plot.reshape(f_to_plot.size)
                #if None in f_to_plot : continue
                err_to_plot = err[tuple(slices)]
                err_to_plot = err_to_plot.reshape(err_to_plot.size)
                if 'mean' in plot : err_to_plot = None
                if plot=='IQR' or plot=='resolution': err_to_plot = None
                if plot=='ratio': err_to_plot = None

                label = bh.name.split('_')[-1] + ' ' + bh.tag
                #"""
                if bh.name.split('_')[-1]=='dnn' : label = 'DNN calibration'
                elif bh.name.split('_')[-1]=='cal' : label = 'Standard calibration'
                elif bh.name.split('_')[-1]=='uncal' : label = 'Uncalibrated'
                elif bh.name.split('_')[-1]=='ratio' : label = 'Ratio'
                if len(self.bh_list)==1 : label = ""
                #"""
                label += ' ' + sec_axis.fancy_name + '$\in$[' + str(np.round(sec_axis.edges[int(sec_bin)],1)) + ', ' + str(np.round(sec_axis.edges[int(sec_bin)+1],1)) + '] ' + sec_axis.unit if len(index_None)>1 else ''
                plt.errorbar(f_axis.binCenters()[slices[index_None[0]]], f_to_plot, err_to_plot, c=self.color[c_i], lw=1.5, marker='.', label=label)
                plt.legend(prop={'size': 15})
                plt.xlabel(xlabel)
                plt.ylabel(ylabel)
                plt.title(title)
                
                c_i += 1
        
        self.ax.xaxis.set_label_coords(0.95, -0.05)
        self.plotName = self.respName + plot + 'VS' + xlabel + '__' + in_bin 
        self.plotName = self.plotName.replace("\eta",'eta').replace('$','').replace(' ', '').replace('{','').replace('}','').replace('|','')
        print(self.plotName, end='\r', flush=True)

        if self.saveFig:
            self.saveFigure(self.plotName)
        
        return self.plotName if plotName==None else plotName


    def saveManyPlots(self, plot='resp', ranges=[None]):
        self.saveFig=True
        if plot=='meanResp' or plot=='meanIQR':
            print("will see later")
        else:
            n_bins = len(ranges)
            bins = []
            for i in range(n_bins):
                if ranges[i]!=None and type(ranges[i])!=int: 
                    index_range = i
                    bins.append(0)
                else:
                    bins.append(ranges[i])

            range_list = np.linspace(ranges[index_range][0],ranges[index_range][1], ranges[index_range][1]-ranges[index_range][0]+1)

            for i in range_list:
                bins[index_range] = int(i)
                self.globalPlotFct(plot=plot, bins=bins)


        self.saveFig=False


    def createPDF(self, outputFile=None):
        from fpdf import FPDF
        pdf = FPDF()

        files = self.savedPlots
        print('Files to pdf : ', files)

        if outputFile==None:
            outputFile = 'resp_' 
            for bh in self.bh_list:
                outputFile += bh.name + bh.tag
            outputFile += '.pdf'
        else:
            outputFile += '.pdf'
        print('Output File : ', outputFile)

        for file in files:
            if '.pdf' in file: continue 
            pdf.add_page(orientation='L')
            pdf.image(self.outDir + file + self.outExt,x=0,y=50, w=300,h=100)
        pdf.output(self.outDir + outputFile, "F")

    def removeSavedPlots(self, ):
        for file in self.savedPlots:
            os.remove(self.outDir + file + self.outExt)
        self.savedPlots = []




def gauss(x, A=1, mu=1, sigma=0.2):
    return A*np.exp(-0.5*(x-mu)**2/sigma**2)

def gauss2side(x, A=1, mu=1, sigma1=0.2, sigma2=0.2):
    return A*np.exp(  -(x-mu)**2/ (2*np.where(x<mu,sigma1,sigma2)**2) )


def smoothing(h, ):
    from scipy.signal import savgol_filter
    h_array = h.hcontent[1:-1]
    hw2 = h.hw2[1:-1]

    bestrelErr = np.nanmin((np.sqrt(hw2) / h_array))
    if np.isnan(bestrelErr): bestrelErr = 0

    n = h.xaxis.nbins
    w = max( int( max(bestrelErr,0.05)*n), 5 )
    h_filter = np.maximum(0, savgol_filter(h_array, w+1 if w%2==0 else w ,3, mode='constant') )

    return h_filter



class RespDep(RespWrapper):

    def linearFit(self, bh, bins, drawFit=False, ):
        # Apply a linear fit to the response along an axis (e.g pT,eta,NPV)
        # Need to specify a bh and bins as a list of coords with one None placed on the corresponding axis
        # Ex : if BPS=pTmeta and want to apply a linear fit of the response along the axis m 
        # and at pT_bin = 5 and eta_bin = 15
        # then bins=[5,None,15]

        resp = bh.resp
        error = bh.error

        n_bins = len(bins)
        slices = []
        interval = ''
        for i in range(n_bins):
            if bins[i]==None :
                index_None = i
                slices.append(slice(0,None))
            else:
                slices.append(slice(bins[i],bins[i]+1))

        f_to_fit = resp[tuple(slices)]
        f_to_fit = f_to_fit.reshape(f_to_fit.size)
        err = error[tuple(slices)]
        err = err.reshape(err.size)

        x_axis = self.axis[index_None].binCenters()
        params, cov = curve_fit(linear, x_axis, f_to_fit, sigma=err, p0=[0.5, 1])

        if drawFit:
            self.resetFig()
            plt.plot(x_axis,f_to_fit, lw=2, label='Resp')
            plt.plot(x_axis,linear(x_axis,*params), lw=1, color='r', label='Linear fit, grad = ' + str(params[0]))
            plt.plot([x_axis[0],x_axis[-1]], [f_to_fit[0],f_to_fit[0]], lw=0.5, c='k')
            plt.legend()
            plt.xlabel(self.axis[index_None].name)
            title = self.fullRespName + ' ' + 'R' + '(' + self.axis[index_None].name + ')' 
            plt.title(title)
            plt.ylim(bottom = 0.85, top = 1.05)
            plt.pause(1)

        return params[0], np.sqrt(np.diag(cov)[0])


    def getGradResp(self, axis, drawFit=False):
        # Apply linear fit to resp along the specified axis in each bins of the other axis
        # Store the gradient as a friend of the bh
        axis-=1
        for bh in self.bh_list:
            grad = bh.addFriend( "resp_grad" + self.axis[axis].name )
            err = bh.addFriend( "resp_grad_err" + self.axis[axis].name )
            coords = bh.bps.indices[()]

            for c in coords:
                if c[axis]!=0: continue
                bins = list(c)
                bins[axis] = None
                c = tuple(c)
                grad[c], err[c] = self.linearFit(bh, bins=bins, drawFit=drawFit)

