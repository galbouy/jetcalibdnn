import sys



exec(open('initRespAnalysis.py').read())

bh_tags = sys.argv[5:]

reFit=False
if sys.argv[-1] == 'reFit': 
	reFit = True
	bh_tags = sys.argv[5:-1]

bps = sys.argv[1]
jet_type = sys.argv[2]
if jet_type=='None': jet_type=''
plot = sys.argv[3]
resp_type = sys.argv[4]
if resp_type=='m+e' or resp_type=='e+m':
	genericRespCalc(gs, bps = bps, plot = plot, resp_type='e', bh_tags=bh_tags, jet_type = jet_type, reFit=reFit )
	genericRespCalc(gs, bps = bps, plot = plot, resp_type='m', bh_tags=bh_tags, jet_type = jet_type, reFit=reFit )

else:
	genericRespCalc(gs, bps = bps, plot = plot, resp_type=resp_type, bh_tags=bh_tags, jet_type = jet_type, reFit=reFit )