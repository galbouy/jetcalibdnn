"""Input/Output operations.

"""
import numpy as np
import numexpr as ne
from tensorflow.keras.utils import Sequence
import tensorflow as tf
import uproot

from . import NNUtils as utils
from .ConfigUtils import ConfigDict
from .Variables import Variable

def split_by_nbytes(a,nbytes):
    if a.nbytes > nbytes :
        utils.debug( a.nbytes , "  > " , nbytes )
        n = len(a)//2
        l = split_by_nbytes(a[:n], nbytes) + split_by_nbytes(a[n:], nbytes)
        return l
    return [a]


def writeTTree(fname, treename, **branch_dict):

    from uproot.writing import recreate
    f = recreate(fname)
    f[treename] = branch_dict
    f.close()
    
    # branchDef = dict( (k,'float32') for k in branch_dict )
    # # uproot is unable to save arrays bigger than 2**24 : split them 
    # branch_arrays = dict( (bname, split_by_nbytes(a, 2**24) ) for (bname, a ) in branch_dict.items() )

    # arrayL0 = list(branch_arrays.values())[0]
    
    # with uproot.recreate(fname) as newf:
    #     newf[treename] = uproot.newtree(branchDef)
    #     for i in range(len(arrayL0)):
    #         newf[treename].extend(
    #             dict( (bname, larray[i])  for  (bname, larray ) in branch_arrays.items() )
    #         )


class TreeChain:
    """Holds a list of Variable instances and connects them to a list of input TTrees stored in 
    one or many TFiles. 
    Also supports a "tree friend" mechanism similar to TTree::AddFriend().
    """
    def __init__(self, treename, input_files, input_friends=[] ):
        self.treename=treename
        self.friends = []
        self.weights_var =None # meant to point to the Variable representing the sample weights
        self.allVars = None    # All the Variable instances belonging to this TreeChain
        self.filtering_vars = [] # variables used only to filter inputs

        self.xy_vars = []  # list of input and target variables
        self.aux_vars = [] # list of variables not used in training but needed for computing x or y

        self.ordered_vars = [] # list of all variables ordered by dependenccy. 

        self.current_file = None
        self.last_loaded_file = None

        self.post_filters = []
        self.pre_filters = []
        self.common_array = None # the numpy array containing all variables being read from file

        
        self.interpretationsD = {} # will instruct uproot in which array to load data
        
        if input_files != []:
            self.file_helpers = [ DataFileHelper(i, n) for i,n in enumerate(input_files) ]
            for i, f in enumerate(self.file_helpers[:-1]):
                f.next_file = self.file_helpers[i+1]
            self.file_helpers[-1].next_file = self.file_helpers[0]
            self.file_helpers[-1].last = True
            self.file_helpers[0].batch_index_offset = 0

            
        if input_friends!=[]:
            if len(input_friends) != len( input_files) :
                print("ERROR number of friends different from number of input files")
                raise Exception()
            
            self.add_friend(input_friends)

        self.input_file_names = input_files
        self.input_friend_names = input_friends
        
        
        
    def add_friend(self, input_friends):        
        self.friends.append(TreeChain(self.treename, input_friends, []) )
        # relate self.file_helpers instances to those inside the newly created TreeChain :
        for myf, friendf in zip(self.file_helpers, self.friends[-1].file_helpers):
            myf.friends.append(friendf)
            


    def shuffle_files(self, indices=None):
        if indices is None:
            indices = list(range(len(self.input_file_names)))
            np.random.shuffle(indices)
        for i, n in enumerate(indices):
            self.file_helpers[i].fname = self.input_file_names[n]
        for friend_chain in self.friends:
            friend_chain.shuffle_files(indices)
        return indices
    
    def unshuffle_files(self, ):
        for i,n in enumerate(self.input_file_names):
            self.file_helpers[i].fname = n
        for friend_chain in self.friends:        
            friend_chain.unshuffle_files()
            
    def branch_names(self, t=None):
        if t is None:
            f0 = uproot.open(self.file_helpers[0].fname)
            t = f0[self.treename]
        return [b for b in t.keys()] # keys() returns bytes objects, not strings
        
    def entries_max_sum(self):
        entriesNum = [uproot.open(f.fname)[self.treename].num_entries for f in self.file_helpers]
        self.max_entries = max( entriesNum )
        self.total_entries = sum(entriesNum)
        return self.max_entries, self.total_entries


    def setup(self, xy_vars, aux_vars,parent):
        """Define the variables to be associated to this TreeChain.
        xy_vars : the  input & targets list of Variable instances
        aux_vars : the list of other Variable instances needed for calculations
        parent : the parent Keras.Sequence
        
        This function creates the main numpy array to contain all the numbers needed. 
        The rows of this array will be associated to each Variable through Variable.setup() calls 
        
        """
        self.parent = parent # the parent keras.Sequence
        self.entries_max_sum()
        self.allVars = ConfigDict( **dict( (v.name, v) for v in xy_vars + aux_vars) )
        for vn, alias in Variable.aliases:
            if vn in self.allVars:
                self.allVars[alias] = self.allVars[vn]
        
        
        self.xy_vars = xy_vars
        self.aux_vars = aux_vars

        # Build the main array :
        a = np.zeros( (len(xy_vars) , self.max_entries, ), dtype=np.float32)
        
        for v in aux_vars:
            v.setup( self,  np.zeros_like(a[0]) )
        for i,v in enumerate(xy_vars):
            v.setup(self,  a[i] )
            
        self.common_array = a
        allvariables = xy_vars+aux_vars

        self.weights_var = None
        for v in allvariables:
            if v.is_sampleW:
                self.weights_var = v

        self.order_variables()
        

        self.link_arrays_to_TTrees()
        for c in self.friends:
            c.common_array = a
        

    def order_variables(self):
        """Compute the dependency order of each variable and fill them sorted according to this order into self.order_variables.
        """
        # sort variables, so when we call their update() method consistently by following this order
        allvariables = set(self.allVars.values()) # set : avoid double coounting aliases
        for v in allvariables:
            v.update_order = None
        for v in allvariables:
            v.calculate_update_order()
        self.ordered_vars = list(allvariables)
        from operator import attrgetter
        self.ordered_vars.sort( key=attrgetter('update_order') )

    def set_weights(self, wvar):
        if wvar==1:
            # remove weights
            if self.weights_var:
                self.allVars.pop(self.weights_var.name)
                if self.weights_var.from_src:
                    self.interpretationsD.pop(self.weights_var.src_name)
                self.weights_var = None
        else:
            # use these weights:
            #wvar.update_order= -10000
            if self.weights_var:
                wvar.setup(self, self.weights_var.array)
            else:
                a = np.zeros( ( self.max_entries, ), dtype=np.float32)
                wvar.setup(self, a)
                if wvar.from_src:
                    self.setup_interpretations([ wvar] )
            self.weights_var = wvar
            self.allVars[wvar.name] = wvar
        self.order_variables()
        
                
    def link_arrays_to_TTrees(self):
        ## *****************
        ## split variables on each friends if this tchain  
        chains = [self] + self.friends
        available_names = [c.branch_names() for c in chains ]
        variablesByChains = [ [] for c in chains ]
        for v in self.ordered_vars:
            if not v.from_src: continue
            found = False
            for i, vList in enumerate(available_names):                
                if v.src_name in vList:
                    found= True
                    variablesByChains[i].append(v)
                    break
            if not found:
                print ("ERROR  :  variable ",v.name, " with src_name = ", v.src_name , " not found in input trees")
                raise KeyError
        
        # then tell each friends to associate TTree branches with arrays (using uproot "interpretation" mechanism)
        for c, vL in zip(chains, variablesByChains):
            c.setup_interpretations(vL)
        ## *****************
        
        
    def setup_interpretations(self, variables):
        if variables == [] : return
        f0 = uproot.open( self.file_helpers[0].fname )
        t0 = f0[self.treename]
        interpretationsD = self.interpretationsD
        for v in variables:
            if v.from_src:
                print( "TreeChain : var from input TTree:   ",v.name, " from branch ", v.src_name , self.file_helpers[0].fname)            
                interpretationsD[v.src_name] = t0[v.src_name].interpretation.inplace(v.array)

    def remove_interpretations(self,variables):
        for v in variables:
            self.interpretationsD.pop(v.src_name)

    def clone(self,):
        
        cl = TreeChain(self.treename,[],[])
        cl.file_helpers = self.file_helpers
        for fchain in self.friends:
            cl.friends.append(fchain.clone() )
        cl.input_file_names = self.input_file_names
        return cl

    def view(self):
        pass

    def load_file(self, file_helper):
        """Unconditionnaly load raw data from root file described by file_helper into the numpy arrays we hold (through our Variables).
         The file_helper object state is modified.
        """
        #i = file_helper.file_index
        self.current_file = uproot.open(file_helper.fname)
        self.current_tree = self.current_file[self.treename]
        self.current_tree.arrays(self.interpretationsD, library='np')

        utils.debug(self,  " ccccccccccccccc loaded ", id(self.common_array), self.common_array[:,:1] , file_helper.file_index, 'vs', self.last_loaded_file)
        for i,c in enumerate(self.friends):
            c.load_file(file_helper.friends[i])
        self.last_loaded_file = file_helper
        file_helper.Nentries  = self.current_tree.num_entries
        file_helper.is_scaled = False
        file_helper.is_transformed = False
        print(" TreeChain loaded file ",file_helper.file_index, file_helper.fname, "in ",self )
        # apply unconditionnal irreversible transformation on variables which needs it.
        self.irrev_transform_variables()
        
        return self.current_tree.num_entries
        
        

    def set_filters(self, preFilterDict, postFilterDict):
        """Set data filters on top of the filters defined by the input variables themselves.
        
        The final pre_filters are set to the input variables pre_filtes AND the AND-combination of the variable filters defined by preFilterDict
        (same thing for post_filters).        
        """
        pre_filters = []
        post_filters = []
        for v in self.ordered_vars:
            pre_filters += v.build_pre_maskers()
            post_filters += v.build_post_maskers()

        # reset filtering_vars
        self.remove_interpretations(self.filtering_vars)
        self.filtering_vars = []
        def fill_filter_list(filters, filterDict):
            for v, filterFunc in filterDict.items():
                if v in self.allVars:
                    filters +=  self.allVars[v].build_maskers(filterFunc)
                else:
                    print("Filters : looking for additionnal variable ", v)
                    var = Variable.referenceVars[v].clone()
                    var.setup( self,  np.zeros_like(self.common_array[0]) )
                    var.calculate_update_order()
                    filters += var.build_maskers(filterFunc)
                    self.filtering_vars.append( var  )
            return filters
        self.pre_filters = fill_filter_list(pre_filters, preFilterDict)
        self.post_filters = fill_filter_list(post_filters,postFilterDict)
        self.setup_interpretations(self.filtering_vars)




    def apply_pre_filters(self, masked_entries):
        for f in self.pre_filters:
            f(masked_entries)
    def apply_post_filters(self, masked_entries):
        for f in self.post_filters:
            f(masked_entries)


    def irrev_transform_variables(self):
        for v in self.ordered_vars:
            v.irrev_update_array()
            
    def transform_variables(self,reverse):
        if self.filtering_vars != []:
            allvariables  = self.filtering_vars + self.ordered_vars
            from operator import attrgetter                    
            allvariables.sort( key=attrgetter('update_order') )
        else:
            allvariables = self.ordered_vars
        for v in allvariables:
            v.update_array(reverse=reverse)

class DataFileHelper:
    """This class represents the state of data loaded for 1 input file.
    It calculates and records how  the input data array is splitted into batches.
    It also points to the next DataFileHelper instance to be loaded.
    """
    def __init__(self, i, fname):

        self.valid_bindex = False        
        self.is_transformed = False
        self.is_scaled = False

        self.fname= fname
        
        self.file_index = i

        self.Nentries = -1

        self.batch_size = 0

        # these represent the filtered indices from which to pick the batches.
        # these 2 variables are set by the ROOTDataGenerator.transform_filter_sample method
        self.Nindices = 0
        self.indices = None # 

        self.batch_index_offset = None

        self.next_file = None

        self.last = False

        self.friends = []
        
    def calculate_batch_boundaries(self,total_Nbatch, batch_index_to_file_index):
        if self.last:
            # last file ! Fix the number of batch :
            expected_Nbatch = total_Nbatch - self.batch_index_offset
            print("  Last File !!  nbatch ", expected_Nbatch)
        else:
            expected_Nbatch = (self.Nentries // self.batch_size  ) 

        self.expected_Nbatch = expected_Nbatch

        # self.Nindices and self.indices are set by the transform_filter_sample() method
        actual_batch_size , reminder  = np.divmod(self.Nindices, expected_Nbatch) 
        
        #self.
        next_reload_index = min(self.batch_index_offset + expected_Nbatch, total_Nbatch)  #+ (1 if reminder>0 else 0)
        if not self.last: self.next_file.batch_index_offset = next_reload_index
        
        #self.file_index_to_batch_offset[self.file_index+1] = next_reload_index        
        batch_index_to_file_index[self.batch_index_offset:next_reload_index] = self.file_index

        
        self.batch_boundaries = np.ones(expected_Nbatch+1,dtype=int)
        self.batch_boundaries[0]=0
        self.batch_boundaries[:reminder+1] *= (actual_batch_size+1)
        self.batch_boundaries[reminder+1:] *= (actual_batch_size)
        self.batch_boundaries.cumsum(out=self.batch_boundaries)
        self.valid_bindex = True
        print(  "total valid=", self.Nindices , " = ", actual_batch_size," x ",self.expected_Nbatch,"+ reminder", " -->  next at ", next_reload_index, '/' , total_Nbatch )

        
    def boundaries(self, index):
        i = index - self.batch_index_offset
        start = self.batch_boundaries[i] # i*self.thisload_batch_size
        stop = min(self.batch_boundaries[i+1], self.Nentries)
        #print(" ---> batch ",index, " entry : ",start, stop)
        return start, stop

    def indices_at(self, batch_index):
        start, stop = self.boundaries(batch_index)
        return self.indices[start:stop]


        
class ROOTDataGenerator(Sequence):
    """Generates data for Keras
    
    Sequence based data generator. Suitable for building data generator for training and prediction.

    The class must implements the __len__ methods returning the number of batch  it will generate for a given batch size.
    However, because we filter the input data this not knowable until *all* the data has been filtered (thus everything has been loaded at leace once). 
    This can take a lot of time before the training starts. 

    So this class instead estimate a total number of batch (self.total_Nbatch) and for each input file loaded an expected number of batches (self.thisload_expected_Nbatch).
    Then it adapts the batch sizes so we have in total exactly self.total_Nbatch and thus meet the expectations of keras.Model.fit()

    The loading of input file is done by a TreeChain object attached to this ROOTDataGenerator.
    """
    def __init__(self, treename, input_files, x_vars, y_vars, aux_vars=[], shuffle=False, input_friends=[], maxEvents=-1):
        """Initialization """
        self.treename = treename
        self.input_files = input_files
        self.x_vars = x_vars
        self.y_vars = y_vars
        self.aux_vars = aux_vars
        self.all_vars = y_vars+x_vars+aux_vars
        self.n_y = len(y_vars)
        self.n_x = len(x_vars)
        self.shuffle = shuffle
        self.debug= False

        self.iterator = None
        self.thisload_file_index = -1
        self.total_Nfiles = len(input_files)

        self.batch_index_to_file_index = []
        self.file_index_to_batch_offset = []
        
        self.chain = TreeChain(treename, input_files, input_friends)
        
        self.current_file = self.chain.file_helpers[0]
        
        self.varDict = ConfigDict()
        for v in self.all_vars: self.varDict[v.name]=v

        self.thisload_is_transformed = False
        self.thisload_is_scaled = False

        self.inscaler = utils.NoScaler()
        self.outscaler = utils.NoScaler()


        self.current_weight = None
        self.batch_size = 1000

        self.maxEvents = maxEvents

        self.num_seensofar = 0
        
    def setup_variables(self ):
        
        self.chain.setup(self.x_vars+self.y_vars, self.aux_vars, self)        

        self.total_entries = self.chain.total_entries
        maxEntries = self.chain.max_entries

        self.src_indices = np.arange(maxEntries)
        self.masked_entries = np.ndarray( (maxEntries,), dtype=bool)
        
        self.buffer_array = self.chain.common_array.T #a.T 

        self.next_reload_index = 0

        self.current_file = self.chain.file_helpers[0]

        self.chain.set_filters({}, {})

        self.set_batch_size(self.batch_size) # default value


    def allChains(self):
        return [self.chain]
        
    def set_weights(self, wvar):        
        if isinstance(wvar, str):
            wvar = Variable.referenceVars[wvar]
        if wvar == self.current_weight:
            return
        self.current_weight = wvar
        if wvar ==1:
            self._set_weights(1)
            self.current_weight = None        
        else:
            if self.current_file: self.format_current_sample(reverse=True)
            self._set_weights(wvar.clone())
            if self.current_file: self.format_current_sample()        

    def _set_weights(self, wvar):
        self.chain.set_weights(wvar)


    def set_filters(self, preFilters, postFilters):
        self.chain.set_filters(preFilters, postFilters)
        self.current_file.valid_bindex = False # this will trigger the recalculation of new variables in filtering_vars
        
        
    def set_batch_size(self, batch_size):
        total_entries = self.total_entries if self.maxEvents<0 else min(self.maxEvents, self.total_entries)

        if batch_size>total_entries:
            batch_size=total_entries
        self.total_Nbatch = int(np.floor(total_entries / batch_size))
        self.batch_size = batch_size 
        print( "generator reset batch size ", batch_size,  " / total nbatch= ", self.total_Nbatch)

        self.batch_index_to_file_index = -np.ones( self.total_Nbatch, dtype=np.int32)
        self.batch_index_to_file_index[0] = 0

        for f in self.chain.file_helpers:
            f.valid_bindex = False
            f.batch_size = batch_size
        
            
    def set_scalers(self, inscaler, outscaler):                
        if self.current_file: self.unscale_current_sample()
        self.inscaler = inscaler
        self.outscaler = outscaler


    def shuffle_files(self, ):
        indices = self.chain.shuffle_files()
        self.set_batch_size(self.batch_size)
        self.reset()
        return indices

    def unshuffle_files(self, ):
        self.chain.unshuffle_files()
        self.set_batch_size(self.batch_size)
        self.reset()
        
        
    def sample(self, batchi = 0):
        X,Y = self[batchi][:2] # just to trigger the loading of first load
        return self.current_sample()
    
    def current_sample(self, formatted=True, directY=False):
        self.format_current_sample(reverse = not formatted)
        X , Y = self.x_slice_filtered(), self.y_slice_filtered(direct=directY)
        return X, Y

    def current_unfiltered_sample(self, formatted=True, directY=False):
        self.format_current_sample(reverse = not formatted)
        X , Y = self.x_slice_unfiltered(), self.y_slice_unfiltered(direct=directY)
        N = self.current_file.Nentries    
        return X[:N], Y[:N]
        


    def scale_current_sample(self):
        #print ( "uuuuuuuuuu scale_current_sample")
        if  self.current_file.is_scaled:
            return
        self.inscaler.scale_inplace(self.x_slice_unfiltered() )
        self.outscaler.scale_inplace(self.y_slice_unfiltered(direct=True) )
        self.current_file.is_scaled = True

    def unscale_current_sample(self, ):
        if not self.current_file.is_scaled:
            return
        self.inscaler.unscale_inplace(self.x_slice_unfiltered() )
        self.outscaler.unscale_inplace( self.y_slice_unfiltered(direct=True)  )
        self.current_file.is_scaled = False

    def untransform_current_sample(self):
        self.transform_current_sample(reverse=True)
    def transform_current_sample(self, reverse=False):
        if self.current_file.is_transformed and not reverse: return
        if not self.current_file.is_transformed and reverse: return

        self.chain.transform_variables(reverse)
        self.current_file.is_transformed = not reverse


    def format_current_sample(self, reverse= False):
        if reverse:
            self.unscale_current_sample()
            self.untransform_current_sample()
        else: # normal transformation             
            self.transform_current_sample()
            self.scale_current_sample()            
        
        
        
    def format_inputs(self, inputs):
        outputs = inputs.copy()
        for i, v in enumerate(self.x_vars):
            outputs[:,i] = v.transformed_array(array=inputs[:,i])
        self.inscaler.scale_inplace(outputs)
        return outputs


    def transform_filter_sample(self, ):
        file_helper = self.current_file
        self.masked_entries[:file_helper.Nentries] = True
        self.masked_entries[file_helper.Nentries:] = False

        # ensure we start from unformated data :
        self.format_current_sample(reverse=True)
        # filters typically come from user-specified conditions like abs(eta)>3 or e<500 etc...
        self.chain.apply_pre_filters(self.masked_entries)
        self.transform_current_sample()
        self.chain.apply_post_filters(self.masked_entries)        
        self.scale_current_sample()
        
        indices = self.src_indices[self.masked_entries]
        file_helper.Nindices = len(indices)
        if self.shuffle:
            np.random.shuffle(indices)
        file_helper.indices = indices
        
        
        
    def loadForBatch(self, index):
        file_index = self.batch_index_to_file_index[index]
        current_file = self.current_file
        if file_index==-1:
            for f in self.chain.file_helpers:
                if not f.valid_bindex:
                    self.loadFile(f.file_index)
                file_index = self.batch_index_to_file_index[index]
                if file_index !=-1: break
                
        elif current_file is None or file_index!= current_file.file_index or not current_file.valid_bindex:
            self.loadFile(file_index)

    def loadFile(self, file_index, noTransform=False, calcBatchBound=True):
        # load data
        utils.debug("start loading file = ", file_index )
        current_file = self.chain.file_helpers[file_index]
        if self.current_file == current_file :
            if current_file.valid_bindex:
                return
        self.current_file = current_file        

        # Load data, if needed, and update current_file members Nentries, is_scaled and is_transformed        
        nn = self.loadFileData()        
        print(" --> done loading for File = ", file_index, nn ," entries"  )
        
        # -----------------------------
        # noTransform is used in operations unrelated to training or predicing, ex : building histograms
        if noTransform: return 
        
        self.transform_filter_sample()

        if calcBatchBound:
            current_file.calculate_batch_boundaries(self.total_Nbatch, self.batch_index_to_file_index)

        #self.num_seensofar += current_file.Nindices
        print(" data ready file ",file_index , '  --> ',self.num_seensofar)

    def loadFileData(self, ):
        file_helper = self.current_file
        if self.chain.last_loaded_file == file_helper:
            return file_helper.Nentries
        return self.chain.load_file(file_helper)

    def reset(self):
        self.chain.last_loaded_file = None
        self.current_file = None

    
    def __len__(self):
        """Denotes the number of batches per epoch
        :return: number of batches per epoch
        """
        return self.total_Nbatch

    def __getitem__(self, index):
        """Generate one batch of data
        :param index: index of the batch
        :return: X and y when fitting. X only when predicting
        """
        self.loadForBatch(index)

        batch_indices = self.current_file.indices_at( index ) 
        X = self.x_slice_filtered( batch_indices )
        Y = self.y_slice_filtered( batch_indices )

        self.num_seensofar += X.shape[0]
        
        if self.current_weight is not None:
            W = self.w_slice_filtered( batch_indices )
            #print ('w=',W[:5])
            return (X, Y, W )
        
        return X, Y



class SingleTargetGenerator:
    """ Implements methods to return the inputs and targets from the common array when the model expects
    a single target"""
    
    singleTarget =True
    def x_slice_filtered(self, indices=None):
        indices = self.current_file.indices if indices is None else indices
        return self.chain.common_array[:self.n_x,indices].T 
    def x_slice_unfiltered(self):
        return self.chain.common_array[ :self.n_x, :].T 

    def y_slice_filtered(self, indices=None, **a):
        indices = self.current_file.indices if indices is None else indices
        #return self.buffer_array[indices, self.n_x: self.n_x+1]
        return self.chain.common_array[self.n_x: self.n_x+1,indices].T 

    def y_slice_unfiltered(self, **args):
        #return self.buffer_array[:, self.n_x:self.n_x+1]
        return self.chain.common_array[self.n_x:self.n_x+1,:].T 

    def w_slice_filtered(self,indices=None):
        indices = self.current_file.indices if indices is None else indices
        # return self.buffer_array[:, self.n_x+self.n_y:self.n_x+self.n_y+1] 
        return self.chain.weights_var.array[indices]

    def w_slice_unfiltered(self):
        return self.chain.weights_var.array

class DoubleTargetGenerator(SingleTargetGenerator):
    """ Implements methods to return the inputs and targets from the common array when the model expects
    2 separate targets"""
    singleTarget = False
    addVar1 = None # some loss functions want additionnal variables passed along with the Y values. this gives the index of variables
    addVar2 = None # some loss functions want additionnal variables passed along with the Y values. this gives the index of variables
    def y_slice_filtered(self, indices=None, direct=False):
        indices = self.current_file.indices if indices is None else indices        
        #y=self.buffer_array[indices, self.n_x: self.n_x+2]
        #y=self.chain.common_array[self.n_x: self.n_x+2,indices].T
        y1,y2 = self.chain.common_array[self.n_x: self.n_x+2,indices]
        if self.addVar1 is not None:
            y1 = np.stack( [y1, self.chain.common_array[self.addVar1,indices] ] ) 
        #return (y[:,0] , y[:,1])
        if direct:
            self.chain.common_array[self.n_x: self.n_x+2,indices].T
        return y1.T,y2.T

    def y_slice_unfiltered(self, direct=False):
        #y= self.buffer_array[:, self.n_x:self.n_x+2]
        y= self.chain.common_array[self.n_x:self.n_x+2,:].T
        if direct: return y
        return (y[:,0] , y[:,1])
    def w_slice_filtered(self,indices=None):
        indices = self.current_file.indices if indices is None else indices
        # return self.buffer_array[:, self.n_x+self.n_y:self.n_x+self.n_y+1]
        w = self.chain.weights_var.array[indices]
        return (w,w)

    def w_slice_unfiltered(self):
        w=self.chain.weights_var.array
        return (w,w)

    
from concurrent.futures import ThreadPoolExecutor    
class ROOTDataGeneratorMT(ROOTDataGenerator):
    """Specializes the data loading methods so one file is loaded in a parralel thread """
    class PSeudoFutures:
        def result(self):
            return 

    def __init__(self, treename, input_files, x_vars, y_vars, aux_vars=[], shuffle=False, input_friends=[], maxEvents=-1):
        if len(input_files)<2:
            print( "ROOTDataGeneratorMT error : can't use MT generator with only 1 input file")
            raise Exception("ROOTDataGeneratorMT only 1 file")
        super(ROOTDataGeneratorMT, self).__init__(treename, input_files, x_vars, y_vars, aux_vars, shuffle, input_friends, maxEvents)

        self.chain_workers = [self.chain, self.chain.clone() ]
        self.nextchain = self.chain_workers[1]
        self.pool = ThreadPoolExecutor(max_workers=2)
        self.reset()
        
    def reset(self):
        self.chain.last_loaded_file = None
        self.current_file = None
        self.thisload_chain_index = 1
        self.nextchain = self.chain_workers[1]
        self.chain = self.chain_workers[0]
        
        self.future = ROOTDataGeneratorMT.PSeudoFutures()
        
    def setup_variables(self ):
        xy_vars_cl =[ v.clone() for v in self.x_vars+self.y_vars]
        aux_vars_cl= [v.clone() for v in  self.aux_vars ]
        super(ROOTDataGeneratorMT,self).setup_variables()
        self.chain_workers[1].setup(xy_vars_cl, aux_vars_cl, self)
        self.chain_workers[1].set_filters({}, {})
        
        
    def loadFileData(self, ):
        file_helper = self.current_file
        if self.chain.last_loaded_file == file_helper:
            return file_helper.Nentries
        # make sure the last reading is done 
        nn = self.future.result()
        # now the data is in the other chain... unless we're NOT trying to read sequentially :
        if self.nextchain.last_loaded_file != file_helper:
            # force loading now :
            nn = self.nextchain.load_file( file_helper )
        # now the data is in the other chain, so swap :
        self.thisload_chain_index = 1 - self.thisload_chain_index
        self.chain, self.nextchain = self.nextchain, self.chain
        self.buffer_array = self.chain.common_array.T

        # then start reading the next file in a thread from the pool
        self.future = self.pool.submit( self.nextchain.load_file, file_helper.next_file ) 
        
        #self.future = self.submitNextLoad(file_helper)
        #print ("----------> ", self.future)
        #print( " -----> ", self.x_slice_unfiltered()[:3])
        return nn

    # def submitNextLoad(self, file_helper):
    #     self.thisload_chain_index = 1 - self.thisload_chain_index
    #     self.chain = self.chain_workers [ self.thisload_chain_index ]
    #     self.buffer_array = self.chain.common_array.T
    #     nextchain = self.chain_workers [ 1 - self.thisload_chain_index ]
    #     future = self.pool.submit( nextchain.load_file, file_helper.next_file ) 
    #     return future

    def _set_weights(self, wvar):
        self.chain_workers[0].set_weights(wvar)
        if wvar !=1 : wvar = wvar.clone()
        self.chain_workers[1].set_weights(wvar)

    def set_filters(self, preFilters, postFilters):
        self.chain_workers[0].set_filters(preFilters, postFilters)
        self.chain_workers[1].set_filters(preFilters, postFilters)
        if self.current_file: self.current_file.valid_bindex = False # this will trigger the recalculation of new variables in filtering_vars
    def allChains(self):
        return self.chain_workers




## The Concrete class we use to implement a Keras Sequence are coumpound
## of ROOTDataGenerator and Single/DoubleTargetGenerator :

class Generator1Target(ROOTDataGenerator,SingleTargetGenerator ):
    pass
class Generator2Target(ROOTDataGenerator,DoubleTargetGenerator ):
    pass
    
class Generator1TargetMT(ROOTDataGeneratorMT,SingleTargetGenerator ):
    pass
class Generator2TargetMT(ROOTDataGeneratorMT,DoubleTargetGenerator ):
    pass


#
class ROOTDataGeneratorValid(Sequence):
    """A Sequence dedicated to run a validation sample from a dedicated input file.
    This class re-use the arrays setup by an existiting ROOTDataGenerator and just implements what's necessary 
    to load & run on an other single input file.
    """
    def __init__(self, generator, valid_file, valid_friend):
        self.generator = generator
        self.file_helper = DataFileHelper(-10, valid_file,)
        self.friend_helper = DataFileHelper(-10, valid_friend,)

        self.file_helper.friends.append( self.friend_helper )
        self.file_helper.batch_index_offset=0
        self.file_helper.last = True
        self.batch_size = 0
        self.total_entries= uproot.open(valid_file)[generator.chain.treename].num_entries
        self.needLoad = True
        self.total_Nbatch = None
        
    def set_batch_size(self, batch_size):
        self.batch_size = batch_size
        self.total_Nbatch = max(1,int(np.floor(self.total_entries / batch_size)))

        
    def __len__(self):
        return self.total_Nbatch

    def reset(self):
        self.needLoad = True


    def load(self):
        if self.generator.chain.last_loaded_file != self.file_helper:
            gen = self.generator
            gen.chain.load_file(self.file_helper)
            #print(" xxxxxxxxx", self.file_helper.is_transformed , self.file_helper.is_scaled)
            fh_tmp = gen.current_file
            gen.current_file = self.file_helper
            gen.transform_filter_sample()
            gen.current_file = fh_tmp
            self.file_helper.calculate_batch_boundaries(self.total_Nbatch, np.zeros(1))
            self.chain = gen.chain # redirect so DoubleTargetGenerator functions work
            self.needLoad = False

    def current_unfiltered_sample(self, formatted=True, directY=False):
        self.load()
        gen = self.generator
        fh_tmp = gen.current_file
        gen.current_file = self.file_helper
        r = gen.current_unfiltered_sample(formatted,directY)
        gen.current_file = fh_tmp
        return r
    
    
    def __getitem__(self, index):
        gen = self.generator
        self.load() # loads only if needed
        
        batch_indices = self.file_helper.indices_at( index )         
        X = gen.x_slice_filtered( batch_indices )
        Y = gen.y_slice_filtered( batch_indices )

        
        if gen.current_weight is not None:
            W = gen.w_slice_filtered( batch_indices )
            return (X, Y, W )
        
        return X, Y





    


##  *************************************************
##  *************************************************
##  *************************************************

## experimental code below 
    
class BatchPacker:
    PACKSIZE=150000
    def __init__(self, buffer_array, n_x, n_y):
        self.buffer_array = buffer_array
        self.n_x = n_x
        self.n_y = n_y

    def reset_load(self, thisload_indices, batch_size):
        self.thisload_indices = thisload_indices
        self.pack_offset = 0
        self.batch_size = batch_size
        self.next_pack = 0
        self.set_next_pack()
        
    def set_next_pack(self):
        self.pack_offset = self.next_pack
        pack_indices = self.thisload_indices[self.pack_offset:self.pack_offset+self.PACKSIZE]
        self.next_pack = self.pack_offset+self.PACKSIZE  - (self.PACKSIZE % self.batch_size) 
        self.X_t = tf.convert_to_tensor( self.buffer_array[pack_indices, self.n_y:self.n_x+self.n_y])
        self.Y_t = tf.convert_to_tensor( self.buffer_array[pack_indices, :self.n_y])
        
    def sample(self, start, stop):
        if start >= self.next_pack:
            self.set_next_pack()
        start -= self.pack_offset
        stop -= self.pack_offset
        X = tf.slice( self.X_t , (start,0), (stop,-1)  )
        Y = tf.slice( self.Y_t , (start,0), (stop,-1)  )
        return X, Y
        


class ROOTDataGeneratorDS(ROOTDataGenerator):
    def __init__(self, treename, input_files, x_vars, y_vars, aux_vars=[], shuffle=False, input_friends=[]):
        super(ROOTDataGeneratorDS,self).__init__(treename, input_files, x_vars, y_vars, aux_vars, shuffle, input_friends)

    def getDataset(self):
        X = self.buffer_array[self.thisload_indices, self.n_y:self.n_x+self.n_y]        
        Y = self.buffer_array[self.thisload_indices, :self.n_y]

        ds = tf.data.Dataset.from_tensor_slices((X,Y) )
        return ds
        


    def getXYTensors(self):
        self.buffer_array[:self.thisload_Nindices] = self.buffer_array[self.thisload_indices]
        self.thisload_Nentries = self.thisload_Nindices
        X = self.buffer_array[:self.thisload_Nindices, self.n_y:self.n_x+self.n_y] 
        Y = self.buffer_array[:self.thisload_Nindices, :self.n_y] 


        if tf.config.experimental.list_physical_devices('GPU') != []:
            with tf.device('/GPU:0'):
                self.X = tf.convert_to_tensor( X[:100*1000] )
                self.Y = tf.convert_to_tensor( Y[:100*1000] )
        else:
            self.X = tf.convert_to_tensor( X[:100*1000] )
            self.Y = tf.convert_to_tensor( Y[:100*1000] )
            
        return self.X, self.Y
        

    
class PairScaler:
    def __init__(self, s1, s2 ):
        self.s1 = s1
        self.s2 = s2
    @tf.function
    def scale(self, X, Y) :
        return (self.s1.scale(X), self.s2.scale(Y)) 
    @tf.function
    def unscale(self, X, Y) :
        return (self.s1.unscale(X), self.s2.unscale(Y)) 
    
