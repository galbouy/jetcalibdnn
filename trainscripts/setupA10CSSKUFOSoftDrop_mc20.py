# this file is meant to be included from the main jetCalibDNN.py script 

conf.update(
    nnName = "CSSKUFOSoftDrop" ,
    inputFiles = 'CSSKUFOSoftDrop_*.root',
    treeName = "IsoJetTree" ,

    features = ['eta', 'e_var', 'm_var', 'Width', 'EMFrac', 'EM3Frac', 'Tile0Frac', 'EffNConsts', 'groomMratio', 'neutralFrac', 'sumPtTrkFrac', 'sumMassTrkFrac', 'Split12', 'Split23', 'C2', 'D2', 'Tau32', 'Tau21', 'Qw', 'mu', 'NPV', ]

)


refV=Variable.referenceVars    
    
# adapt the variable src_name to the source name in the input file :
for v in [ 'Width', 'Split12', 'Split23', 'C2', 'D2', 'Qw' ]:
    refV[ v ].src_name = 'jet_'+v

refV.EMFrac.src_name = 'jet_EMFracCaloBased'
refV.EM3Frac.src_name = 'jet_EM3FracCaloBased'
refV.Tile0Frac.src_name = 'jet_Tile0FracCaloBased'
refV.EffNConsts.src_name = 'jet_EffNClustsCaloBased'
refV.groomMratio.src_name = 'jet_groomMRatio'
refV.neutralFrac.src_name = 'jet_NeutralEFrac'
refV.sumPtTrkFrac.src_name = 'jet_ChargePTFrac'
refV.sumMassTrkFrac.src_name = 'jet_ChargeMFrac'
refV.Tau32.src_name = 'jet_Tau32_wta'
refV.Tau21.src_name = 'jet_Tau21_wta'

refV.e_true.src_name = 'jet_true_E'
refV.pt_true.src_name = 'jet_true_pt'
refV.m_true.src_name = 'jet_true_mass'
refV.eta_true.src_name = 'jet_true_eta'
refV.PID.src_name = 'jet_PartonTruthLabelID'

refV.e_reco.src_name = 'jet_E'
refV.pt_reco.src_name = 'jet_pt'
refV.m_reco.src_name = 'jet_mass'
refV.eta.src_name = 'jet_eta'

refV.mu.src_name = 'actualMu'
refV.NPV.src_name = 'NPV'
refV.eventWeightXS.src_name = 'weight'

conf.additionalVars += ["eventWeightXS"]

# Below we adapt the reference variables to our sample
#  - adjust the scale factors and offsets of variable normalizations

# Adjust offset and scale factors. Check the normalized histo with :
# exec(open('plotAndDebugDNN.py').read())
# trainer.histogramInputs(formatted=True)

refV.eta.setScaleParameters(2.23e-01,1.00e-05)
refV.e_reco.setScaleParameters(sf=0.390, o=-2.430 )
refV.m_reco.setScaleParameters(1.95e-01,-.53e+00)
refV.Width.setScaleParameters(2.8e+00,-1e+00)
refV.EMFrac.setScaleParameters(1.55e+00,-0.95e+00)
refV.EM3Frac.setScaleParameters(3.0e+00,-0.9e-00)
refV.Tile0Frac.setScaleParameters(1.9e+00,-0.8e-00)
refV.EffNConsts.setScaleParameters(3.5e-01,-1.3e+00)
refV.groomMratio.setScaleParameters(1.98e-00,-0.99e+00)
refV.neutralFrac.setScaleParameters(1.98e-00,-0.99e+00)
refV.sumPtTrkFrac.setScaleParameters(1.8e-00,-1.0e-00)
refV.sumMassTrkFrac.setScaleParameters(1.99e+00,-9.92e-01)
refV.Split12.setScaleParameters(9.2e-07,-1.0e+00)
refV.Split23.setScaleParameters(3.e-06,-1.0e+00)
refV.C2.setScaleParameters(3.2e-00,-1.00e+00)
refV.D2.setScaleParameters(1.e-02,-1.00e+00)
refV.Tau32.setScaleParameters(2.05e-00,-1.0e-00)
refV.Tau21.setScaleParameters(2.1e-00,-1.0e-00)
refV.Qw.setScaleParameters(1.75e-03,-1.0e+00)
refV.mu.setScaleParameters(2.0e-02,-1.0e+00)
refV.NPV.setScaleParameters(2.3e-02,-1.0e+00)
refV.r_raw_e.setScaleParameters(7.5e-01,-1.03e-00)
refV.r_raw_m.setScaleParameters(2.0e-03,-.0e-00)
