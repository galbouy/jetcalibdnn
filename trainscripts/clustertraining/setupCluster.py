# this file is meant to be included from the main jetCalibDNN.py script 
import JetCalibDNN.ModelDefinitions as mod
from JetCalibDNN.Variables import Variable,VarGenExpr2

clustVar = [
    Variable("clusterE" ,    title="$E_{EM}$",  h_range=(0,2000), array_transfo=[ "log(a)"  ], array_revtransfo=["exp(a)"]),
    Variable("clusterEta" ,  title='$\eta$', h_range=(-1,1) , pre_filter= lambda eta : abs(eta)<0.8),

    Variable("clusterECalib" ,    title="$E_{LCW}$",  h_range=(0,2000), ),
    
    Variable("cluster_CENTER_LAMBDA", title="$<\lambda>$",  h_range=(0,2000) ),
    Variable("cluster_SECOND_LAMBDA", title="$<\lambda^2>$", h_range=(0,250000) ),
    Variable("cluster_LATERAL", title="$<m^2_{lat}>$",  h_range=(0,1.01) ),
    Variable("cluster_ENG_FRAC_EM", title="$f_{EM}$",  h_range=(0,1.01) ),
    Variable("cluster_FIRST_ENG_DENS", title="$ < rho > $",   h_range=(0,0.0014), array_transfo=[ "where(a<=0.,c_01*c_0e5,a)","log(a)"  ], array_revtransfo=["exp(a)"]),
    Variable("cluster_SIGNIFICANCE", title="$\chi^{EM}_{clus}$",  h_range=(0,30) ),
    Variable("cluster_PTD", title="$p_TD$",  h_range=(0,1.01) ),
    Variable("cluster_LONGITUDINAL", title="$<m^2_{long}>$",  h_range=(0,1.01) ),

    Variable("cluster_ENG_CALIB_TOT", title="$E^{dep}_{clus}$", h_range=(0,2000) ),
    
    Variable("cluster_HAD_WEIGHT", title="$HAD_w$", h_range=(0,2000) ),
    Variable("charge","truthPDG", title="$E^{dep}_{clus}$", h_range=(0,1),  irrev_array_transfo=[ "a==111"] ,  ),

    VarGenExpr2("clusterEHad", "v2*v1", v1='clusterE', v2='cluster_HAD_WEIGHT', title='$E_{LCW}$' ,h_range=(0.2000), ),
    
    ]


Variable.aliasConfig.reco.eta = 'clusterEta'
class HeadIdCoreT0ST(mod.ModelDefiner, mod.Identity,  mod.DenseCoreT0, mod.Layer1Tail):
    N = 200



conf.update(
    nnName = "clusterCalib" ,
    inputFiles = 'ClusterT*.root',
    treeName = "ClusterTree" ,
    
    features = ["clusterEta","clusterE",
                "cluster_CENTER_LAMBDA", "cluster_LONGITUDINAL", "cluster_LATERAL", "cluster_ENG_FRAC_EM", "cluster_FIRST_ENG_DENS", "cluster_SIGNIFICANCE","cluster_PTD",],

    # 'jet_ChargedFraction_tracks', 'jet_ChargedFraction_combined', 'jet_ChargedFraction_SumPtTrkPt500',  'jet_ChargedFraction_charged', 'jet_ChargedFraction',
    # ------------
    targets=['r_e', ],
    # ------------

)




refV=Variable.referenceVars    

def addJes():
    conf.features.append('jesR')
    conf.inputFriends = 'AntiKt4EMPFlowJetsJES_*.root'
    


Variable.aliasConfig.true.e_var = "cluster_ENG_CALIB_TOT"
def checkEandMVariants():
    for vfull in ['clusterE', ]:
        if vfull in conf.features:
            refV['r_raw_e'].numerator = vfull
            refV['r_raw_e'].denom = "cluster_ENG_CALIB_TOT"
            refV['r_raw_e'].reset_depenencies()
            
            refV['r_cal_e'].numerator = vfull+'Had'
            refV['r_cal_e'].denom = "cluster_ENG_CALIB_TOT"
            refV['r_cal_e'].reverse_update=True
            refV['r_cal_e'].reset_depenencies()
            Variable.aliasConfig.reco['e_var'] = vfull
checkEandMVariants()

    

# Below we adapt the reference variables to our sample
#  - adjust the scale factors and offsets of variable normalizations

# Adjust offset and scale factors. Check the normalized histo with :
# exec(open('plotAndDebugDNN.py').read())
# trainer.histogramInputs(formatted=True)


refV.clusterEta.setScaleParameters(sf=1.000, o=0.000 ) 
refV.clusterE.setScaleParameters(sf=0.18, o=-0.4 )
refV.cluster_CENTER_LAMBDA.setScaleParameters(sf=0.0008, o=-0.950 ) 
refV.cluster_LONGITUDINAL.setScaleParameters(sf=1.900, o=-0.900 ) 
refV.cluster_LATERAL.setScaleParameters(sf=1.800, o=-0.900 ) 
refV.cluster_FIRST_ENG_DENS.setScaleParameters(1.3e-1, o=1.8 )  
refV.cluster_SIGNIFICANCE.setScaleParameters(sf=0.0023, o=-0.9 ) 
refV.cluster_ENG_FRAC_EM.setScaleParameters(sf=1.800, o=-0.900 ) 
refV.cluster_PTD.setScaleParameters(sf=2, o=-1.1 ) 


def defaultSequenceCl(trainer):
    args = trainer.config.refitParams
    
    #trainer.config.callbacks+=[ utils.earlyStopLoss , keras.callbacks.TerminateOnNaN()]
    #if not trainer.gen.singleTarget: utils.earlyStopLoss.lossT = -5.5
    print("aaaaa0")
    if trainer.config.lossType=="MDNA":
        lossFunc = lambda t: utils.amdnLoss
    elif trainer.config.lossType=="MDN":
        lossFunc = lambda t: utils.truncmdnLoss(t)

        #loss = utils.mdnLoss if trainer.config.nPredictedParam else utils.lgkLoss(1e-3, 1e-3)
    trainer.refit( batch_size = 1000, epochs=1 , steps_per_epoch=30000, optimizer='rada', loss=lossFunc(None), **args)
    if trainer.net.stop_training:
        print("ERROR")
        return
    print("aaaaa1")
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-3), epochs=5, loss=lossFunc(None) , **args)

    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =25000, optimizer=radaOpt(1e-4), epochs=5, loss=lossFunc(None), **args )
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =35000,  optimizer=radaOpt(1e-4), nepoch=3 , loss=lossFunc(None)  , **args  )
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-4), nepoch=3 , loss=lossFunc(3)  , metrics=[], **args  )
    trainer.save_model()

