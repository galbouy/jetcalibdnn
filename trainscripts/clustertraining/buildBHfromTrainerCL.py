# USAGE : 

## 
# exec(open('HistoAnalysis/buildBHfromTrainer.py').read()) # IMPORTANT : this must be done BEFORE the trainer.train( ) call
##
# trainer.train(conf.update(inputFiles='CSSKUFOSoftDrop_*.root' , nInputFiles = -1 , outputDir='cca/', modelBuilder =ModelDefinitions.HeadEtaBlockCoreT0Tail1(N = 500, coverage= 'full' ), sampleWeights = None, mode= 'reco:predict', inputFriends='CSSKUFOSoftDropAtlas*root' ), reload = 1, fitSequence = defaultSequence)
#trainer.fillBinnedHistos( [eR_in_BINS_cal,  eR_in_BINS_uncal, mR_in_BINS_uncal, mR_in_BINS_cal ] ,nFile=-1, outName='CSSKUFOSoftDrop_atlas'+bpsTag+'_respBH0.h5' , useWeights=False)
#  ---> gemerate binmed histos with ATLAS calin (using inputFriends , then _cal)



#from HistoAnalysis.
from JetCalibDNN.HistoAnalysis.BinnedArrays import *
import os
import os.path


def symmetrize(l):
    """ given [0, a, b, c], returns [-c, -b, -a, 0, a, b, c]"""
    newL =[-i for i in l[1:] ] #assumee l starts with 0.
    newL.reverse()
    return newL+l

def seq(start, stop, step):
    return list(np.arange(start, stop+0.99*step, step))

    

atlasCalib = True
#atlasCalib = False

eEtaBPS = BinnedPhaseSpace("eEtaBPS",
                           AxisSpec("E", title="E",edges=[0, 1, 2, 5, 10, 20, 30,  50, 100, 300, 600,  1200,  2500] ),
                           # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                           AxisSpec("eta", title="$\eta$", edges=seq(-0.8 , 0.8, 0.4 ) , isGeV=False),
                           )
eEtaBPS.coords = ('cluster_ENG_CALIB_TOT', 'clusterEta')

eEtaChBPS = BinnedPhaseSpace("eEtaChBPS",
                           AxisSpec("E", title="E",edges=[0, 1, 2, 5, 10, 20, 30,  50, 100, 300, 600,  1200,  2500] ),
                           # eta bins used for JES. 'seq' is a helper function defined in PyHelpers.py
                           AxisSpec("eta", title="$\eta$", edges=seq(-0.8 , 0.8, 0.4 ) , isGeV=False),
                           AxisSpec('charge', title="charge", edges=[-0.1,0.5,1.1], isGeV=False)
                           )
eEtaChBPS.coords = ('cluster_ENG_CALIB_TOT', 'clusterEta', 'charge')


# ********************************************

theBPS = eEtaChBPS


bpsTag = theBPS.axisTag()

refV = Variable.referenceVars
for c,ax in zip(theBPS.coords, theBPS.axis):
    ax.title = refV[c].title


eR_in_BINS_dnn = BinnedHistos( "eR_in_"+bpsTag+"_dnn", theBPS, (240,(0.4,2.4)) )
eR_in_BINS_dnn.coordNames = theBPS.coords
eR_in_BINS_dnn.valueName = 'r_dnn_e'


eR_in_BINS_uncal = BinnedHistos( "eR_in_"+bpsTag+"_uncal", theBPS, (240,(0.4,2.4)) )
eR_in_BINS_uncal.coordNames = theBPS.coords
eR_in_BINS_uncal.valueName = 'r_raw_e'

eR_in_BINS_cal = BinnedHistos( "eR_in_"+bpsTag+"_cal", theBPS, (240,(0.4,2.4)) )
eR_in_BINS_cal.coordNames = theBPS.coords
eR_in_BINS_cal.valueName = 'r_cal_e'


# in some smallR ntuple we have the intermediate "EtaJES" calibrated E



if atlasCalib :
    allBinnedHistos = [ eR_in_BINS_cal, eR_in_BINS_uncal, ]
else:
    allBinnedHistos = [  eR_in_BINS_dnn, ]



## *********************
# Depending on which BPS and BinnedHistos we filled, some additionnal variables need to
# be read from file. 
if atlasCalib :
    loadedVars = set( theBPS.coords + tuple(bh.valueName for bh in allBinnedHistos) )
    loadedVars.add("cluster_HAD_WEIGHT")

    # build a variable filtering func. This will be called by Trainer to limit the number of variables (only to speed-up the read operations).
    conf.filterLoadedVar = lambda vL : [v for v in vL if v.name in loadedVars] 
    # make sure these variables are properly loaded.
    conf.additionalVars += ["cluster_HAD_WEIGHT",'r_cal_e', ]
else:    
    #conf.additionalVars += ['r_dnn_e', ]
    pass
if 'charge' in [a.name for a in theBPS.axis]:
    conf.additionalVars += ['charge', ]
## *********************



    
    
def fillBinnedHistos(self, bhList, useWeights=True, beginFileTask=None, outName=None, nFile=-1, doSave=True, ):
    self.gen.reset()
    gen = self.gen

    if not self.config.mode.endswith('predict'):
        print('ERROR !! use predict mode, not ', self.config.mode)
        return
    if not isinstance(bhList, list): bhList = [bhList]

    if useWeights :
        for c in gen.allChains():
            c.weights_var.normalize = False


    
    # group by bps to factorize indices calculations
    bhDict = dict()
    for bh in bhList:
        bhDict.setdefault( (bh.bps,)+bh.coordNames, []).append(bh)
        
        
    maxDimBPS = max( bhList , key=lambda bps:bh.bps.nDim() ).bps

    # instantiate one array of N-dim indices suitable to the bigges BPS & input data
    binIndices = maxDimBPS.buildNCoordinates(gen.chain.max_entries)
    hIndices = np.zeros( (gen.chain.max_entries,) ,dtype=int)
    nFile = len(gen.input_files) if nFile==-1 else nFile
    for i in range(nFile):
        gen.loadFile(i, noTransform=True)
        allVars = self.allVariables()

        gen.transform_current_sample() # make sure the ratios are calculated
        gen.untransform_current_sample() # make sure we revert to nominal variables
        #print("XXXX2 = ",allVars.charge.array.sum())
        
        if callable(beginFileTask): beginFileTask(self)
        N = gen.current_file.Nentries
        if useWeights:
            w0 = gen.chain.weights_var.array[:N]
            
            
        #loop over the BinnedHistos which share the same bps & coordinates 
        for _, bhL in bhDict.items():            
            bps = bhL[0].bps 
            print("******************** Fill for BPS ",bps.name)
            #print(allVars.m_true.array[:7])
            # compute the common index coordinates for these BinnedHistos :
            coords = [ allVars[v].array[:N] for v in bhL[0].coordNames]
            tupleI, validCoordI = bps.findValidBins(coords, binIndices[:bps.nDim(),:N] )
            print(" coords=", coords[2][:10], tupleI[2][:10] , '  ',coords[2].sum() )
            
            # Then fill these BH with their values at the calculated coordinates
            w = w0[validCoordI] if useWeights else 1.
            w2=w*w
            for bh in bhL:
                print("******************** ---> Fill for BinnedHistos ",bh.name)
                values = allVars[bh.valueName].array
                print('---> ',values)
                bh.fillAtIndices( values[:N], validCoordI, tupleI, w, w2, hIndices=hIndices[:N])
    if outName:
        print("Saving Binned Histo into ", outName)
        self.saveBinnedHistosAsH5(bhList, outName)

        
def predCalib(trainer):
    N = trainer.gen.current_file.Nentries
    class _tmp:
        def __init__(self,a):
            self.array=a
    r = trainer.fullPredictions()
    trainer.gen.format_current_sample(reverse=True) # to get back e and m at normal scales.
    r_cal_e = r[:,0]

    print("r_e", trainer.gen.chain.allVars.r_e.array[:7])
    print("r_cal_e", r_cal_e[:7])

    if trainer.config.optimTag != 'directSF':
        # perform r_cal = r / r_cal in-place :
        r_cal_e = np.reciprocal( r_cal_e, out=r_cal_e) 
    r_cal_e *= trainer.gen.chain.allVars.r_e.array[:N]
    print("r_cal_e", r_cal_e[:7])
    
    trainer.gen.chain.allVars.r_dnn_e = _tmp(r_cal_e)

def saveBinnedHistosAsROOT(self, bhList, fname):
    f = uproot.recreate(fname)
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveAsROOT(f)
    f.close()

def saveBinnedHistosAsH5(self, bhList, fname):
    if os.path.exists( fname) : os.remove(fname)        
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveInH5(fname)

    if self.net is not None:
        self.saveTrainSummary(fname)
    
if 'trainer' in dir():
    Trainer.fillBinnedHistos = fillBinnedHistos
    Trainer.saveBinnedHistosAsH5 = saveBinnedHistosAsH5
    Trainer.saveBinnedHistosAsROOT = saveBinnedHistosAsROOT


prefix = ''
print("""trainer.prepareData(conf.update(inputFiles=prefix+'CSSKUFOSoftDrop_*root' , outputDir='' , nInputFiles = -1 , modelBuilder =ModelDefinitions.HeadEtaBlockCore1LDeepT(N = 600, coverage= 'full', batchNorm=True ), sampleWeights = "Eweights", mode= 'reco:predict' ,inputFriends=prefix+'CSSKUFOSoftDropAtlasCalib*root', doLookahead=True), initScalers=False)
""")
