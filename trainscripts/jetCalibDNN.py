## ######################################################333
## Top level scripts.

## defines a Trainer object, a configuration and training sequences.
##
## typically used interactively from python3 -i jetCalibDNN.py 
## 
##  PLEASE SEE README FOR USAGE EXAMPLES
##
import sys
import numpy as np
import h5py
import argparse

# Import Keras (either standalone, either as embedded in tensorflow )
from JetCalibDNN.ImportKeras import *
import JetCalibDNN.NNUtils as utils

from JetCalibDNN.ConfigUtils import defaultConfig
from JetCalibDNN  import ModelDefinitions
from JetCalibDNN.GeneratorFromRootFile import ROOTDataGeneratorMT, Generator1TargetMT, Generator2TargetMT
from JetCalibDNN.Variables import Variable, dataPartitions
from JetCalibDNN import NNUtils


from JetCalibDNN.Trainer import Trainer

## ***********************************************************************
## Argument parser
## ***********************************************************************
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--variableFile", type=str, help="The configuration file defining the DNN input vars", required=True, default='setupA10CSSKUFOsoftDrop.py'
    )

    parser.add_argument("--training", action="store_true",)
    parser.add_argument("--prediction", action="store_true",)

    return parser.parse_args()

## ***********************************************************************
## Configuration
## ***********************************************************************

conf=defaultConfig.clone(

    # **********************
    # the options below are necessary : we set them in a dedicated file to allow easy switching from
    # jet one collection to another. See variableFile below
    #nnName = "" ,     SET in variableFile 
    #inputFiles = '',  SET in variableFile
    #treeName = "" ,   SET in variableFile
    #features = [],     SET in variableFile

    # **********************

    # ------------    
    inputDir='data/',
    outputDir='',
    # ------------    

    # define the input reading classes for 1 or 2 targets : 
    inputClassList = [ Generator1TargetMT, Generator2TargetMT ],
    # ------------    
    
    # ------------
    targets=['r_e', 'r_m'],
    # ------------
    useWeights = False,
    sampleWeights = None,
    # ------------    

    # ------------    
    lossType = 'MDNA', # Trainer will use this to set conf.nPredictedParam 
    # ------------    
    
    # ------------    
    # modelTag = '', set automatically by the object passed as modelBuilder
    optimTag = '',
    # specialTag='_batchSequenceMix',
    specialTag='_batchSequenceMix_apr23',
    # ------------    
    
    # ------------    
    callbacks = [
        keras.callbacks.ModelCheckpoint('tmp.hdf5', monitor='loss', mode='min',save_best_only=True,verbose=1),
    ],
    metrics = ['loss', 'accuracy'],
    # ------------    

    # ----------------
    doLookahead = True,
    # -----------------
    ngpu = 1,
    # --------------------
    
    # --------------------
    modelBuilder = ModelDefinitions.HeadEtaBlockCoreT3AttMDeepT6(N = 700, last_activation='tanhp'),
    # --------------------

    # --------------------
    mode = 'reco:train',
    # --------------------
)

# Get arguments
args = get_args()

print(" Including ", args.variableFile)
    
exec(open(args.variableFile).read(), )

#****************************************
trainer = Trainer()

prefix = ''


## ******************************************************************
## Final training sequence definition :
## ******************************************************************

def radaOpt(lr):
    return tfa.optimizers.RectifiedAdam(lr=lr)

def diffgrad(lr):
    from JetCalibDNN.DiffGradOptimizer import DiffGrad
    return DiffGrad(lr)

    
def defaultSequence(trainer):

    # Set interval for loss monitoring
    trainer.loglossnseen.ninterval=10e5

    print("Start training")

    # Get default parameters to pass to the trainer.refit() function 
    args = trainer.config.refitParams

    # Set initial loss function
    lossFunc = lambda t: utils.truncamdnLoss(t)    

    # Default sequence
    trainer.refit( batch_size =15000, optimizer=radaOpt(1e-3), nepoch=2, loss=[lossFunc(None), lossFunc(None)] , **args)
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =25000, optimizer=radaOpt(1e-3), nepoch=2, loss=[lossFunc(None ), lossFunc(None)], **args )
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =35000, optimizer=radaOpt(1e-3), nepoch=2 , loss=[lossFunc(4), lossFunc(4) ], **args  )
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-3), nepoch=2 , loss=[lossFunc(3.5), lossFunc(3.5) ], **args  )
    trainer.save_model()

    print("End of default sequence, succesfull")

    # Main sequence
    print("Start main sequence")

    print("Start common training sequence")
    # Set loss function for common training sequence
    lossFunc = lambda t: utils.truncamdnLoss(t)
    
    trainer.refit(batch_size=95000, optimizer=diffgrad(1e-3), nepoch=6, loss=[lossFunc(3.5),lossFunc(3.5)], partition=None, loss_weights=[1.,1.],)
    utils.reset_regularizer(trainer.net, 1e-5 )
    trainer.refit(batch_size=95000, optimizer=diffgrad(1e-3), nepoch=6, loss=[lossFunc(3.5),lossFunc(3.5)], partition=None,loss_weights=[1.,1.],)
    trainer.refit(batch_size=125000, optimizer=diffgrad(1e-3), nepoch=6, loss=[lossFunc(3.2),lossFunc(3.2)], partition=None,loss_weights=[1.,1.],)
    utils.reset_regularizer(trainer.net, 0. )
    trainer.refit(batch_size=125000, optimizer=diffgrad(1e-3), nepoch=6, loss=[lossFunc(3.2),lossFunc(3.2)], partition=None,loss_weights=[1.,1.],)
    trainer.refit(batch_size=155000, optimizer=diffgrad(5e-4), nepoch=10, loss=[lossFunc(3),lossFunc(3)], partition=None,loss_weights=[1.,1.],)
    trainer.refit(batch_size=95000, optimizer=diffgrad(1e-5), nepoch=15, loss=[lossFunc(3),lossFunc(2.0)], partition=None,loss_weights=[1.,1.],)
    trainer.save_model()
    print("Common training sequence succesfull")

    print("Start mass exclusive training sequence")

    # Freeze ancestor layers of energy prediction
    trainer.freezeAncestorLayers('outputE')
    # Set loss for mass exclusive training sequence
    lossFunc = lambda t: utils.truncmdnLoss(t)

    trainer.refit(batch_size=95000, optimizer=diffgrad(1e-5), nepoch=50, loss=[lossFunc(3.),lossFunc(1.0)], partition=None,loss_weights=[1.,1.],)
    trainer.save_model()
    print("Mass exclusive training sequence succesful")

    print("End of main sequence, succesful")

    return



def main():

    if args.training:
        # Launch training
        trainer.train(conf, reload = False, fitSequence = defaultSequence)
    
    if args.prediction:
        # Reload model
        trainer.train(conf.update(mode = 'reco:predict'), reload = True, fitSequence = None)

    return

if __name__ == "__main__":
    main()