import ROOT
import sys
ROOT.EnableImplicitMT()

inputFiles = sys.argv[1:]
#print("Input files : ")
#print(inputFiles)

chain = ROOT.TChain("IsoJetTree")
for i, file in enumerate(inputFiles):
    
    chain.Add(file)

rdf = ROOT.RDataFrame(chain)

hist = rdf.Histo1D( ("jet_pT_weighted", "jet pT weighted;pT(GeV);Nevents", 400,0,6000) , "jet_pt", "weight").GetPtr()
#hist.Draw()

hist_true_weight = rdf.Histo1D( ("jet_true_pT_weighted", "jet true pT weighted;pT(GeV);Nevents", 200,0,700) , "jet_true_pt", "weight").GetPtr()
#hist_true_weight.Draw()

hist_true = rdf.Histo1D( ("jet_true_pT", "jet true pT;pT(GeV);Nevents", 200,0,700) , "jet_true_pt").GetPtr()
hist_true.Draw()

hist_w = rdf.Histo1D( ("weight", "event weight;w;Nevents", 200,0,10000) , "weight").GetPtr()
#hist_w.Draw()

hist_m = rdf.Histo1D( ("mass", "energy;w;Nevents", 200,0,200) , "jet_true_mass").GetPtr()
#hist_m.Draw()

input("Press enter to continue ...")