import ROOT
import sys
ROOT.EnableImplicitMT()

inputFiles = sys.argv[1:]
#print("Input files : ")
#print(inputFiles)

vetoFiles = [ ]

chain = ROOT.TChain("IsoJetTree")
for i, file in enumerate(inputFiles):
    if file.split('/')[-1] in vetoFiles :continue

    if i%100==0: print(i, " files added to chain")
    
    tfile = ROOT.TFile(file)
    keys = tfile.GetListOfKeys()
    if keys.GetEntries()==0:
        print(file)
        vetoFiles.append(file.split('/')[-1])
    elif keys[0].GetName()=="IsoJetTree":
        chain.Add(file)
    
    

    

print("Number of events : ", chain.GetEntries())

rdf = ROOT.RDataFrame(chain)

sumW = rdf.Sum("weight").GetValue()
print("Sum of weights = ", sumW)
