## **********************************************
# Flattening and randomized merging of several ttree containing jet data
#  input TTrees are containing vector<float> per event
#  output tree contains only scalars (float, int) with each entry representing 1 jet
#  output tree is also a randomized merge of the input samples
#  output tree also contains a "eventWeight" corresponding to (MC weight)x(xsection weight)
#
# usage :
#  f=createFlattener("flattreeName",neventPerOutFile=12000000)
#  f.run()

# If sum of weights inside histograms in TFiles : 
# for i in user.delsart.3647*; do  cd $i; hadd -T CountHisto.root user.delsart* ; cd .. ;done
#

import glob
import ROOT
import os
import random

ROOT.EnableImplicitMT()

# Path to python scripts
path = "/pbs/home/g/galbouy/DNN_myfork/JetCalibDNN/inputs_utilities/"

# Name of the input TTree
inputTreeName =  "IsoJetTree"

# Name of the event weight branch
eventWeightName = "weight"

# List of variables to discard
vetoVariables = []


# ***************************
# Helper to describe the variables we want to read & write
#  they contain the C++ code which we're going to inject in the main cpp class and compile on-the-fly
class Var:
    def __init__(self,name, typ):
        self.name = name
        self.typ = typ
        self.declInVar = self.t_declInVar.format(typ,name)
        self.initVar = self.t_initVar.format(name,name)
        self.declOutVar = self.t_declOutVar.format(typ, name)
        self.createBranch = self.t_createBranch.format(name, name, name, 'F' if 'oat' in typ else 'I')

    def copy(self):
        print("COPY ",self.name )
        return self.t_copyVar.format(self.name,self.name)
        
        
class ScalarVar(Var):
    t_declInVar = 'TTreeReaderValue<{}> {}'
    t_initVar = '{}(reader, "{}")'
    t_declOutVar = '{} out_{}'
    t_createBranch = 'outTree->Branch("{}",&out_{} , "{}/{}")'
    t_copyVar = 'out_{} = *currentReader->{}'
    

class WeightVar(ScalarVar):
    t_copyVar = 'out_{} = (*currentReader->{})*currentReader->sampleWFactor'
        
class VectVar(Var):        
    t_declInVar = 'TTreeReaderArray<{}> {}'
    t_initVar = '{}(reader, "{}")'
    t_declOutVar = '{} out_{}'
    t_createBranch = ' outTree->Branch("{}",&out_{} , "{}/{}")'
    t_copyVar = 'out_{} = currentReader->{}[i]'

    def __init__(self, name, typ):        
        typ = typ.replace('vector<','')[:-1]
        Var.__init__(self, name, typ)

class FixEinGeV(VectVar):
    ## Very specific case to fix stupid issue in a private ntuple
    t_copyVar = 'out_{} = 0.001*currentReader->{}[i]'



# ***************************
#  Builds the c++ source code needed to flatten the given input TChain c
def buildCode(c):
    varList = []
    refName=None
    seenNames = set()
    for l in c.GetListOfLeaves():
        typ = l.GetTypeName()
        name = l.GetName()
        if name in seenNames:
            print("!!! appearing twice : ",name)
            continue
        seenNames.add(name)
        if name in vetoVariables:
            continue
        if name in []: #["Qw","m","m_true"]:
            varList.append( FixEinGeV(name,typ))
        elif 'vector' in typ:
            refName = name
            varList.append( VectVar(name,typ))
        elif name==eventWeightName:
            varList.append( WeightVar(name,typ) )
        else:
            varList.append( ScalarVar(name,typ))        



    from string import Template
    templateCode = Template(open(path+'treeFlattener.cpp').read())
    return varList, templateCode.substitute(
        declInVar = ';\n'.join( [ v.declInVar for v in varList ] +[''] ),
        declOutVar = ';\n'.join( [ v.declOutVar for v in varList ]+[''] ),
        createBranch = ';\n'.join( [ v.createBranch for v in varList ]+[''] ),
        scalarCopy = ';\n'.join( [ v.copy() for v in varList if isinstance(v,ScalarVar) ] +['']),
        vectorCopy = ';\n'.join( [ v.copy() for v in varList if isinstance(v,VectVar) ] +[''] ),
        ref = refName,
        varInit = ',\n'.join( [ v.initVar for v in varList ] ),
        vecInput = "#define VECTOR_INPUT" if refName != None else "",
    )



# ***************************
#  Setup the Flattener C++ object. This is done by 1st generating the C++ code according to the input TTree variable content,
#  then compiling this code, then setting up JetReader object for each of the JZ slices.

def createFlattener(chains, normalizations, outName='out', maxEvents=None, neventPerOutFile=-1):
    varList, code = buildCode(chains[0])
    print(code)
    ROOT.gInterpreter.LoadText(code)

    f = ROOT.Flattener()
    f.varList = varList
    f.outFileName = outName
    if maxEvents : f.maxEvents = maxEvents
    f.neventPerOutFile = neventPerOutFile
    py_jetreaders =[]
    for c, norm in zip(chains, normalizations):
        r = ROOT.JetReader()
        if norm!=None:
            r.sampleWFactor = norm
        r.init(c)
        py_jetreaders.append(r)
        f.jetreaders.push_back(r) 
    f.init()
    f.totNumEntries = sum( [c.GetEntries() for c in chains])
    f.py_jetreaders = py_jetreaders

    def run(self):
        # remove previous file (ROOT will not owverwrite in this case)
        from os import remove
        for outF in glob.glob(outName+"*.root"):
            remove(outF)
        self.cpp_run()
    ROOT.Flattener.run = run
    
    return f


# List of slices to randomize and merge
samples = ["JZ2", "JZ3","JZ4", "JZ5", "JZ6", "JZ7", "JZ8" , "JZ9", "JZ10", "JZ11", "JZ12" ]
 
exec(open(path + "largeRjetSlices.py").read())

# ***************************
# Create one flatenner per slice to merge different MC campaigns:
# chainsForSlice is defined in largeRjetSlices.py 
# create for each slice N TChain (for each MC campaigns) containing the files corresponding to the slice and MC campaign 
for jzslice in samples:
    chains, normalizations = chainsForSlice(jzslice)
    f = createFlattener(chains, normalizations, "AntiKt10UFOCSSKJets_flat_JZslices/flattree_" + jzslice, neventPerOutFile = 10000000)
    f.run()


# create final flattener with all slices:
# create one TChain per slice containing the merge MC campaigns files
# run on all slices to randomize and merge them
chains = []
inputPattern = "/sps/atlas/g/galbouy/DNN_training_samples/AntiKt10UFOCSSKJets_flat_JZslices/flattree_{JZ}*.root"
for jzslice in samples:
    c = ROOT.TChain(inputTreeName)
    l = sorted(glob.glob(inputPattern.format(JZ=jzslice)))
    print(l)
    for f in l:
        c.AddFile(f)
   
    chains.append(c)

f = createFlattener(chains, [None,] * len(chains), "AntiKt10UFOCSSKJets_flatTree/CSSKUFOSoftDrop", neventPerOutFile = 10000000)
f.run()
