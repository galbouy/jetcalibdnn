# Total integrated luminosity of input MC campaigns
lumiDic = {
    300000 : 44307.4, # mc20d
    310000 : 58450.1, # mc20e
    }


# Cross sections of the different dijet slices
# one list per slice of the same size as the number of MC campaigns
xsections = {
"JZ2" : (2433000000.0,	 0.009863128),
"JZ3" : (26450000.0  ,   0.01165838),
"JZ4" : (254610.0    ,   0.01336553),
"JZ5" : (4553.2	     ,   0.01452648),
"JZ6" : (257.54	     ,   0.009471878),
"JZ7" : (16.215	     ,   0.01109724),
"JZ8" : (0.62506     ,   0.01015436),
"JZ9" : (0.019639    ,   0.01205606),
"JZ10" :(0.0011962   ,   0.005893302) ,
"JZ11" :(4.2263e-05  ,   0.002673) ,
"JZ12" :(1.0367e-06  ,	 0.0004288923) ,
}

# Sum of MC weights for each dijet slice for each MC campaign
# can be obtained by running sumOfWeights.py script
sumOfWeights= {
    "JZ2":  {310000: 10046.05532597764, 300000: 2911.8362836014235, } ,  
    "JZ3":  {310000: 1460.3085152217768, 300000: 1099.47297691266, } ,  
    "JZ4":  {310000: 46.3293717898383, 300000: 33.24121859162793, } ,  
    "JZ5":  {310000: 1.343839120840102, 300000: 1.0575998177010952, } ,  
    "JZ6":  {310000: 0.03886691377887914, 300000: 0.027776817905829962, } ,  
    "JZ7":  {310000: 0.005335462268421749, 300000: 0.0038041808075784025, } ,  
    "JZ8":  {310000: 0.0015411112517857064, 300000: 0.0011050103048783188, } ,  
    "JZ9":  {310000: 0.00027436154639629267, 300000: 0.0002145218823109132, } ,  
    "JZ10": {310000: 3.2022030524655326e-05, 300000: 2.6446806308648603e-05, } ,  
    "JZ11": {310000: 1.6917165505930115e-05, 300000: 1.3984324212993997e-05, } ,  
    "JZ12": {310000: 9.832055152784666e-06, 300000: 8.12296954761987e-06, } ,  
}

# Function to compute the global weight associated to a dijet slice
# used as normalisation factor
def globalWeight(jzslice, runNum):    
    sumW = sumOfWeights[jzslice][runNum]
    xsec, eff = xsections[jzslice]
    lumi = lumiDic[runNum]

    return xsec*eff/sumW*lumi

# List of files to not consider
vetoFiles = [ ]


def chainsForSlice(jzslice):
    """returns 2 lists :
       [ TChains for runNum in (mc20 d,e) ]
       [ normalization for runNum in (mc20 d,e) ]
    This allows to run a Flattener just to mix all MC campaigns events within a given JZ slice.
    """
    # Dict to convert MC campaign to the right reconstruction tag
    runNum_to_r = { 300000 : 'r13144',
		            310000 : 'r13145'}
		    
    chains = []
    normalizations = []

    # File base string 
    fileBase = '/sps/atlas/g/galbouy/DNN_training_samples_mc20/AntiKt10UFOCSSKJets_mc20_Ntuple/group.perf-jets.galbouy.mc20_13TeV.3647*.'+ jzslice +'WithSW.dumpJets_ntuples*.e7142_s3681_{r}_r13146_p5548_largeRJets.outputs.root/*.root'
    for runNum in [300000, 310000]:
        c = ROOT.TChain(inputTreeName)
        print(fileBase.format(r = runNum_to_r[runNum]))
        l = sorted(glob.glob(fileBase.format(r = runNum_to_r[runNum])))
        print(l)
        for f in l:
            if f.split('/')[-1] in vetoFiles : continue
            c.AddFile(f)

        chains.append(c)
        normalizations.append( globalWeight(jzslice, runNum) )

    return chains, normalizations
