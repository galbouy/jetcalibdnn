# from https://github.com/bckenstler/CLR
from tensorflow.keras.callbacks import *
import numpy as np


class CyclicNoOp(Callback):
    def set(self, **pars):
        return self
    
class CyclicLR(Callback):
    """This callback implements a cyclical learning rate policy (CLR).
    The method cycles the learning rate between two boundaries with
    some constant frequency, as detailed in this paper (https://arxiv.org/abs/1506.01186).
    The amplitude of the cycle can be scaled on a per-iteration or 
    per-cycle basis.
    This class has three built-in policies, as put forth in the paper.
    "triangular":
        A basic triangular cycle w/ no amplitude scaling.
    "triangular2":
        A basic triangular cycle that scales initial amplitude by half each cycle.
    "exp_range":
        A cycle that scales initial amplitude by gamma**(cycle iterations) at each 
        cycle iteration.
    For more detail, please see paper.
    
    # Example
        ```python
            clr = CyclicLR(base_lr=0.001, max_lr=0.006,
                                step_size=2000., mode='triangular')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```
    
    Class also supports custom scaling functions:
        ```python
            clr_fn = lambda x: 0.5*(1+np.sin(x*np.pi/2.))
            clr = CyclicLR(base_lr=0.001, max_lr=0.006,
                                step_size=2000., scale_fn=clr_fn,
                                scale_mode='cycle')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```    
    # Arguments
        base_lr: initial learning rate which is the
            lower boundary in the cycle.
        max_lr: upper boundary in the cycle. Functionally,
            it defines the cycle amplitude (max_lr - base_lr).
            The lr at any cycle is the sum of base_lr
            and some scaling of the amplitude; therefore 
            max_lr may not actually be reached depending on
            scaling function.
        step_size: number of training iterations per
            half cycle. Authors suggest setting step_size
            2-8 x training iterations in epoch.
        mode: one of {triangular, triangular2, exp_range}.
            Default 'triangular'.
            Values correspond to policies detailed above.
            If scale_fn is not None, this argument is ignored.
        gamma: constant in 'exp_range' scaling function:
            gamma**(cycle iterations)
        scale_fn: Custom scaling policy defined by a single
            argument lambda function, where 
            0 <= scale_fn(x) <= 1 for all x >= 0.
            mode paramater is ignored 
        scale_mode: {'cycle', 'iterations'}.
            Defines whether scale_fn is evaluated on 
            cycle number or cycle iterations (training
            iterations since start of cycle). Default is 'cycle'.
    """

    def __init__(self, base_lr=0.001, max_lr=0.006, step_size=2000., mode='triangular',
                 gamma=1., scale_fn=None, scale_mode='cycle'):
        super(CyclicLR, self).__init__()

        self.base_lr = base_lr
        self.max_lr = max_lr
        self.step_size = step_size
        self.mode = mode
        self.gamma = gamma
        if scale_fn == None:
            if self.mode == 'triangular':
                self.scale_fn = lambda x: 1.
                self.scale_mode = 'cycle'
            elif self.mode == 'triangular2':
                self.scale_fn = lambda x: 1/(2.**(x-1))
                self.scale_mode = 'cycle'
            elif self.mode == 'exp_range':
                self.scale_fn = lambda x: gamma**(x)
                self.scale_mode = 'iterations'
        else:
            self.scale_fn = scale_fn
            self.scale_mode = scale_mode
        self.clr_iterations = 0.
        self.trn_iterations = 0.
        self.history = {}
        self.epoch_n=0
        self._reset()

    def _reset(self, new_base_lr=None, new_max_lr=None,
               new_step_size=None):
        """Resets cycle iterations.
        Optional boundary/step size adjustment.
        """
        if new_base_lr != None:
            self.base_lr = new_base_lr
        if new_max_lr != None:
            self.max_lr = new_max_lr
        if new_step_size != None:
            self.step_size = new_step_size
        self.clr_iterations = 0.
        
    def clr(self):
        cycle = np.floor(1+self.clr_iterations/(2*self.step_size))
        x = np.abs(self.clr_iterations/self.step_size - 2*cycle + 1)
        if self.scale_mode == 'cycle':
            return self.base_lr + (self.max_lr-self.base_lr)*np.maximum(0, (1-x))*self.scale_fn(cycle)
        else:
            return self.base_lr + (self.max_lr-self.base_lr)*np.maximum(0, (1-x))*self.scale_fn(self.clr_iterations)
        
    def on_train_begin(self, logs={}):
        logs = logs or {}

        if self.clr_iterations == 0:
            K.set_value(self.model.optimizer.lr, self.base_lr)
        else:
            K.set_value(self.model.optimizer.lr, self.clr())        
            
    def on_batch_end(self, epoch, logs=None):
        
        logs = logs or {}
        self.trn_iterations += 1
        self.clr_iterations += 1

        self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
        self.history.setdefault('iterations', []).append(self.trn_iterations)

        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)
        lr = self.clr()
        #print("CLR :setting LR", lr)
        K.set_value(self.model.optimizer.lr, lr)


    def on_epoch_end(self, epoch, logs=None):
        self.epoch_n +=1
        self.history.setdefault('epoch_lr', []).append(K.get_value(self.model.optimizer.lr))
        self.history.setdefault('epoch_it', []).append(self.trn_iterations)
        self.history.setdefault('epoch_n', []).append(self.epoch_n)

class CyclicLR1C(CyclicLR):
    
    def __init__(self, base_lr=0.001, max_lr=0.006, ninput=1, batch_size=1):
        super(CyclicLR1C, self).__init__()
        self.base_lr = base_lr
        self.max_lr = max_lr
        self.iterations_per_epoch = 1
        self.ninput = ninput
        self.batch_size = batch_size
        self.reset()

    def reset(self):
        self.iterations_per_epoch = self.ninput/self.batch_size
        self.iterations_in_cycle = int(self.iterations_per_epoch*0.95)
        self.slope = 2./self.iterations_in_cycle 
        self.print_end_cycle = True
        

    def set(self, **pars):
        for k,v in pars.items():
            setattr(self,k,v)
        self.reset()
        return self
        
    def on_train_begin(self, logs={}):
        logs = logs or {}

        self.momentum0 = K.get_value(self.model.optimizer.momentum)
        if self.clr_iterations == 0:
            K.set_value(self.model.optimizer.lr, self.base_lr)
        else:
            K.set_value(self.model.optimizer.lr, self.clr())        


    def clr(self):
        if self.clr_iterations>=self.iterations_in_cycle:
            if self.print_end_cycle: print(" End 1C at iteration ",self.clr_iterations, " --> ", self.base_lr)
            self.print_end_cycle = False
            return (self.base_lr*(1-0.97*(self.clr_iterations-self.iterations_in_cycle)/(self.iterations_per_epoch-self.iterations_in_cycle)), self.momentum0)
        else:
            i = min(self.clr_iterations, self.iterations_in_cycle-self.clr_iterations)
            return  (i*self.slope* self.max_lr, (1-i*self.slope*0.05)*self.momentum0 )
            
    def on_batch_end(self, epoch, logs=None):
        
        logs = logs or {}
        self.trn_iterations += 1
        self.clr_iterations += 1

        self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
        self.history.setdefault('iterations', []).append(self.trn_iterations)

        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)
        lr,momentum = self.clr()
        #print("CLR :setting LR", lr)
        K.set_value(self.model.optimizer.lr, lr)
        K.set_value(self.model.optimizer.momentum, momentum)


    def on_epoch_end(self, epoch, logs=None):
        self.epoch_n +=1
        K.set_value(self.model.optimizer.momentum, self.momentum0)
        self.max_lr *=0.8
        self.print_end_cycle = True
        self.clr_iterations = 0
        self.history.setdefault('epoch_lr', []).append(K.get_value(self.model.optimizer.lr))
        self.history.setdefault('epoch_it', []).append(self.trn_iterations)
        self.history.setdefault('epoch_n', []).append(self.epoch_n)

    

    def set_max_lr(self, m):
        self.max_lr = m
        self.slope = 2./self.iterations_in_cycle * self.max_lr
        return self

    def __deepcopy__(self,d):
        return self # NO deep copy

class CyclicAdam(Callback):

    def __deepcopy__(self,d):
        return self # NO deep copy
    def __init__(self, ninput=1, batch_size=1, ncycle_per_epoch=10):
        super(CyclicAdam, self).__init__()
        self.ninput = ninput
        self.batch_size = batch_size
        self.ncycle_per_epoch = ncycle_per_epoch
        self.reset()
        self.history = {}
        
    def set(self, **pars):
        for k,v in pars.items():
            setattr(self,k,v)
        self.reset()
        return self

    def reset(self):
        self.iterations_per_epoch = int(self.ninput/self.batch_size)
        self.iterations_per_cycle = int(self.iterations_per_epoch/self.ncycle_per_epoch)

        

    def on_train_begin(self, logs={}):
        logs = logs or {}
        self.iterations = 0


            
    def on_batch_end(self, epoch, logs=None):
        
        logs = logs or {}
        self.iterations += 1
        #print(self, self.iterations)

        #self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
        self.history.setdefault('iterations', []).append(self.iterations)
        
        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

        if self.iterations % self.iterations_per_cycle ==0:
            K.set_value(self.model.optimizer.iterations, int(0.8*(self.iterations-self.iterations_per_cycle)) )
            #print(" reset Adam iterations !")


    def on_epoch_end(self, epoch, logs=None):
        #K.set_value(self.model.optimizer.momentum, self.momentum0)
        #self.print_end_cycle = True
        #self.clr_iterations = 0
        #self.history.setdefault('epoch_lr', []).append(K.get_value(self.model.optimizer.lr))
        #self.history.setdefault('epoch_it', []).append(self.trn_iterations)
        self.history.setdefault('epoch_n', []).append(epoch)
        
