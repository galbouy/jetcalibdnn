
import h5py
import numpy as np
# Import Keras (either standalone, either as embedded in tensorflow )
from .ImportKeras import *
from .ConfigUtils import ConfigDict
#****************************************
def debug_silent(*l):
    pass

debug = debug_silent
def switch_debugging(on=True):
    global debug
    debug = print if on else debug_silent


## ***********************************************************************
## Loss functions
## ***********************************************************************

@tf.function
def mdnLoss(y_true, y_pred):
    sig = tf.maximum(y_pred[:,1],0.005)

    loss= tf.math.log(sig)+ 0.5*( (y_pred[:,0]-y_true[:,0])/sig )**2
    #tf.print("\n xxx ", y_true[:4], "   ", loss[:4])
    return loss


def truncmdnLoss(nsigma=None,):
    if nsigma is None:
        return mdnLoss

    # compute truncated gaussian normalization as function of nsigma and sigma
    erfF = np.vectorize(np.math.erf)
    erf = erfF(nsigma/np.sqrt(2))
    sigmaFactor = 1./ ( (1-np.sqrt(2./np.pi)*nsigma*np.exp(-0.5*nsigma**2)/erf)) # > 1 up to 3.4 at nsigma=1

    @tf.function
    def _truncmdnLoss(y_true, y_pred):

        # by convention, assume predictions in the form (mu, sig)            
        sig = K.maximum(y_pred[:,1],0.005) # force sigma>0 to avoid generating nan values
        diff = (y_pred[:,0]-y_true[:,0])        
        nsig = sig*nsigma

        n0 = tf.cast(tf.shape(sig)[0], tf.float32)
        normDist = (diff/sig)**2

        # the normal MDN loss (except for the sigmaFactor)
        loss = (tf.math.log(sig)+ 0.5*normDist*sigmaFactor)

        # Find where to truncate at n sigma (--> m will be 0 where truncated and 1 elsewhere)
        m = tf.logical_and(diff < nsig , diff > -nsig)

        n = tf.cast(m,tf.float32)* n0 / K.maximum( 1., tf.math.count_nonzero(m,dtype=tf.float32))
            
        # Apply truncation & normalization to the loss values 
        loss *= tf.stop_gradient(n)

        return loss
    return _truncmdnLoss     


@tf.function
def amdnLoss(y_true, y_pred):
    """ MDN loss assuming an assymetric double-sided gaussian as the pdf. Thus expects y_pred.shape == (:,3) """


    ## IMPORTANT !!!!
    ## Bug in keras/TF ??
    ## if using directly (y_true - mu ) --> memory allocation issues (systems try to allocate (batch_size,batch_size)
    ## MUST USE: y_true[:,0] !!
    ## see test function below to reproduce
    mu, sig1, sig2 = y_pred[:,0], tf.maximum(y_pred[:,1],0.005), tf.maximum(y_pred[:,2],0.005) #tf.unstack(y_pred,axis=1)
    totalSig = 0.5*( sig1+sig2 )

    sig = tf.where( y_true[:,0] > mu  , sig1 , sig2)
    #sig = tf.maximum(y_pred[:,1:],0.005)
    loss= tf.math.log(totalSig)+ 0.5*( (mu-y_true[:,0])/sig )**2
    return loss


def truncamdnLoss(nsigma=3):
    if nsigma is None:
        return amdnLoss
    @tf.function    
    def _amdnLoss(y_true, y_pred):
        """ MDN loss assuming an assymetric double-sided gaussian as the pdf. Thus expects y_pred.shape == (:,3) """


        ## IMPORTANT !!!!
        ## Bug in keras/TF ??
        ## if using directly (y_true - mu ) --> memory allocation issues (systems try to allocate (batch_size,batch_size)
        ## MUST USE: y_true[:,0] !!
        ## see test function below to reproduce
        mu, sig1, sig2 = y_pred[:,0], tf.maximum(y_pred[:,1],0.005), tf.maximum(y_pred[:,2],0.005) #tf.unstack(y_pred,axis=1)
        #mu, sig1, sig2 = tf.unstack( tf.maximum( y_pred, 0.005) ,axis=1)
        totalSig = 0.5*( sig1+sig2 )

        sig = tf.where( y_true[:,0] > mu  , sig1 , sig2)
        #sig = tf.maximum(y_pred[:,1:],0.005)
        normDist = (mu-y_true[:,0])/sig
        loss= tf.math.log(totalSig)+ 0.5*(  normDist )**2

        #nsig = sig*nsigma
        m = tf.cast( tf.logical_and(normDist < nsigma , normDist > -nsigma), tf.float32)

        loss*=tf.stop_gradient(m)
        return loss
    return _amdnLoss


## ***********************************************************************
## activation functions
## ***********************************************************************

def mish(x):
    return x*K.tanh(K.softplus(x))

def tanhp(x):
    return 1+K.tanh(x)

def tanh0(x):
    y0,y1 = tf.unstack(x,axis=1)
    return tf.stack([tf.tanh(y0),tf.sigmoid(y1) ] ,axis=1)


activationDict = ConfigDict(
    mish = mish,
    tanhp = tanhp,
    tanh0 = tanh0,
)


## ***********************************************************************
## CallBacks
## ***********************************************************************


#Directory to store the log and call tensorboard for profiling
def tensorBoardProfile():
    from datetime import datetime
    log_dir="logs/logs_" + datetime.now().strftime("%Y%m%d-%H%M%S")
    TensorBoard = keras.callbacks.TensorBoard
    tensorboard_callback = TensorBoard(log_dir=log_dir,  histogram_freq=1, profile_batch=3)
    return tensorboard_callback


class SaveGoodLoss(keras.callbacks.Callback):
    def __init__(self, lossT=-3.5, ):
        super(SaveGoodLoss, self).__init__()
        self.lossT = lossT

        
    def on_batch_end(self, batch, logs):
        if self.model.gen.num_seensofar > 2e7:
            loss= logs['loss']
            if loss < self.lossT:
                self.model.trainer.save_model('goodL'+'{:.3f}'.format(abs(loss)))
                self.lossT -= 0.2

class ShuffleInputFiles(keras.callbacks.Callback):
    def __init__(self,  ):
        super(ShuffleInputFiles, self).__init__()

        
    def on_epoch_end(self, batch, logs):
        iL= self.model.trainer.gen.shuffle_files()
        print('ShuffleInputFiles --> file0 = ', self.model.trainer.gen.chain.file_helpers[0].fname, iL)       
        

class ValidCallBack(keras.callbacks.Callback):
    def __init__(self,  trainer, ):
        super(ValidCallBack, self).__init__()
        self.valLossTot=[]
        self.valLossE=[]
        self.valLossM=[]
        
    def on_epoch_end(self, epoch, logs=None):
        iL= self.model.trainer.validation(batch_size=200000)
        print( "validation : " , iL)
        from numbers import Number
        if isinstance(iL, Number):
            self.valLossE.append(iL)
        elif len(iL)==0:
            self.valLossE.append(iL[0])
        else:
            self.valLossTot.append(iL[0])
            self.valLossE.append(iL[1])
            self.valLossM.append(iL[2])

    def toDict(self):
        d = dict(
            valLossTot = self.valLossTot,
            valLossE = self.valLossE,
            valLossM = self.valLossM,
            )
        return d
    
    def saveDict(self,):
        import pickle
        d = dict(
            valLossTot = self.valLossTot,
            valLossE = self.valLossE,
            valLossM = self.valLossM,
        )
        with open("valLoss_data.pkl", "wb") as pf:
            pickle.dump(d, pf)

    def fromDict(self, d):
        self.valLossTot = d.get('valLossTot', [] )
        self.valLossE = d.get('valLossE', [] )
        self.valLossM = d.get('valLossM', [] )
    
    def draw(self, trainer, plotLosses=True, plotLogLoss=True, norm=False, ax=None, **args):
        from matplotlib import pyplot as plt
        ax= ax or plt.gca()
        ax.cla()
        x = trainer.loglossnseen.lossD['njetatepoch']
        pList = dict()
        axOpt = dict(
            ylabel=args.pop('ylabel', 'loss'),
            xlabel=args.pop('xlabel', 'Num jet processed'),
            )
        if plotLosses: 
            pList["valLossTot"] = self.valLossTot
            pList["valLossE"] = self.valLossE
            pList["valLossM"] = self.valLossM
        if plotLogLoss:
            trainer.loglossnseen.draw()
        nx = len(x)
        for i,loss in enumerate(pList):
            a = pList[loss]
            startx = 0 if len(a)==nx else len(a)
            #if norm : a -= a[-20:].mean()
            #if not self.scaled: a+=5 # to allow log plots
            ax.plot(x[-startx:], a, label=loss, **args)
        ax.set_ylim([-5,0])
        self.vlines=[]
        ax.legend()
        ax.set(**axOpt)
        plt.show()
        self.scaled=True


            
class StopAtNan(keras.callbacks.Callback):
    def __init__(self, trainer ):
        super(StopAtNan, self).__init__()
        self.trainer = trainer
    def on_batch_end(self, batch, logs):
        if np.isnan(logs['loss']):
            self.model.stop_training = True
            print("NAN at batch ", batch)
            if self.trainer.config.sampleWeights:
                x,y,w = self.trainer.gen[batch]
            else:
                x,y = self.trainer.gen[batch]
            if np.any(np.isnan(logs['loss'])):
                print(' nan in loss')
                print(logs['loss'])
            if np.any(np.isnan(x)):
                print(' nan in x')
            if np.any(np.isnan(y)):
                print(' nan in y')
            raise Exception
        
    
class LogLossNumSeen(keras.callbacks.Callback):
    scaled = False
    def __init__(self, ninterval=10e6, ):
        self.njetprocessed= 0
        self.ninterval = ninterval
        # if lossnames==():
        #     lossnames = [ 'loss' , 'outputE_loss' ,'outputM_loss' ]
        self.lossnames = dict(loss='loss') #lossnames
        #self.metrics = [ 'outputE__lgkLoss','outputM__lgkLoss' ]
        #self.lossD = dict( (ln,[]) for ln in lossnames+ ['njetprocessed']+self.metrics)
        self.lossD = None
        
    def on_epoch_end(self, ep, logs):
        self.lossD.setdefault('njetatepoch',[]).append(self.model.gen.num_seensofar)
        
    def on_batch_end(self, batch, logs):
        njetprocessed = self.model.gen.num_seensofar
        if self.njetprocessed < njetprocessed:
            self.njetprocessed = njetprocessed
            for k, lname in self.lossnames.items():
                self.lossD[k].append( logs[lname]) 
            for l in self.metrics:
                self.lossD.setdefault(l,[]).append( logs.get(l,9) ) 
            self.lossD['njetprocessed'].append(njetprocessed)
            self.njetprocessed += self.ninterval
        


    def setupTrainer(self,trainer):
        outLayersName = [l.name for l in trainer.net.layers if l.outbound_nodes==[] ]
        #if 'outputE_bis' in outLayersName:
        #    self.lossnames['E_loss'] = 'outputE_bis_loss'
        #if 'outputM_bis' in outLayersName:
        #    self.lossnames['M_loss'] = 'outputM_bis_loss'

        for lname in outLayersName:
            if 'outputE' in lname:
                self.lossnames['E_loss'] = lname+'_loss'
            if 'outputM' in lname:
                self.lossnames['M_loss'] = lname+'_loss'
        
        if len(outLayersName)==1:
            self.lossnames['E_loss'] = "loss"
            
        #     self.lossnames['E_loss'] = [ l.name+"_loss" for l in outLayers if 'outputE' in l.name][0]
        # except:
        #     self.lossnames['E_loss'] = [ "scale_output" for l in outLayers if 'scale_output' in l.name][0]
        # if 'r_m' in trainer.config.targets:
        #     self.lossnames['M_loss'] = [ l.name+"_loss" for l in outLayers if 'outputM' in l.name][0]
        # else:
        #     self.lossnames['E_loss'] = "loss"
        # self.metrics = [l.name+'lgkloss' for l in outLayers]
        self.metrics = []
        if self.lossD is None:
            self.lossD = dict( (ln,[]) for ln in [*self.lossnames]+ ['njetprocessed']+self.metrics)        
        else:
            # check if pre-existing output loss exists (ex: when copying over different models)
            # if so alias the existing loss entry
            print('AAAAAAAAAAAAAAAAAAAAAAA ')
            tmpD = dict()
            for k,v in self.lossD.items():
                if k in self.metrics: continue
                if 'outputE' in k:
                    tmpD[ 'E_loss' ] = v
                elif 'outputM' in k:
                    tmpD[ 'M_loss' ] = v
            self.lossD.update(**tmpD)

    def toDict(self):
        d = dict(self.lossD)
        d['ninterval'] = self.ninterval
        return d
    
    def saveDict(self,):
        import pickle
        d = dict(
            lossTot = self.lossD["loss"],
            lossE = self.lossD["E_loss"],
            lossM = self.lossD["M_loss"],
            njetprocessed = self.lossD["njetprocessed"],
            njetatepoch = self.lossD['njetatepoch'],
        )
        with open("logLoss_data.pkl", "wb") as pf:
            pickle.dump(d, pf)

    def fromDict(self, d):
        self.ninterval = d.pop('ninterval')
        self.lossD = d
        # temporary fix compatibility with old saved copies:
        for k,v in list(d.items()):
            if 'activation_' in k:
                d[k.replace('activation_', 'output')] = v
        l = d['njetprocessed']
        self.njetprocessed = l[-1] if len(l)>0 else 0
        
    def equalizeLengths(self):
        N = len(self.lossD['njetprocessed'])
        for k,l in self.lossD.items():
            if len(l)<N:
                self.lossD[k] = [l[0]]*(N-len(l))+l
        
                
    def draw(self, showPlot=False, plotLosses=True, plotMetrics=False, toPlots=[], norm=False, ax=None, plotEpochsAt=-5, **args):
        from matplotlib import pyplot as plt
        ax= ax or plt.gca()
        ax.cla()
        x = self.lossD['njetprocessed']
        pList = list(toPlots)
        axOpt = dict(
            ylabel=args.pop('ylabel', 'loss'),
            xlabel=args.pop('xlabel', 'Num jet processed'),
            )
        if plotLosses: pList += self.lossnames.keys()
        if plotMetrics: pList+= self.metrics
        nx = len(x)
        for n in pList:
            a = np.array(self.lossD[n])
            startx = 0 if len(a)==nx else len(a)
            if norm : a -= a[-20:].mean()
            #if not self.scaled: a+=5 # to allow log plots
            ax.plot(x[-startx:], a, label=n, **args)
        ax.set_ylim([-5,0])
        self.vlines=[]
        if 'njetatepoch' in self.lossD.keys():
            for x in self.lossD['njetatepoch'][plotEpochsAt:]:
                self.vlines.append(ax.axvline(x, ls='--',linewidth=1, color='black') )
        ax.legend()
        ax.set(**axOpt)
        if showPlot: plt.show()
        self.scaled=True


    def truncateLosses(self):
        for n in self.lossnames+['njetprocessed']:
            l = self.lossD[n]
            del( l[:len(l)//2] )



## ***********************************************************************
## Custom input variable rescaler
## ***********************************************************************

class VariableScaler:
    """Apply linear scaling to variables (features or targets) so they fit in ~ [-1, 1]. 
    The is to act on a variable X to scale it to (X-mean)/std  where mean & std are typically the mean and std of X (could be anything, ex median & IQR)
    
    This class performs actually the operation 
      (X-mean)/(std*scaleFactor) + offset
    so that mean,std are automatically calculated and scaleFactor, offset can be manually adjusted to better fit in ~[-1,1]. 
    
    X is an array representing several variable of shape (N_events, N_variables). 
    mean, std, scaleFactor, offset are array of shape (N_variables,)
    
    To adjust scaleFactor & offset, see plotAndDebugDNN.py the method histogramInputs(), showVariablesScaleFactors() & setScaleFactors()
    """

    paramValid=False
    def __init__(self, offset=np.array([0.]), scaleFactor=np.array([1.]), fromVars=None):
        self.std= np.array([1.])
        self.mean = np.array([0.])
        self.offset = offset
        self.scaleFactor = scaleFactor
        if fromVars is not None:
            self.setParamFromVars(fromVars)

    def setSize(self, n):
        self.offset = np.zeros(n)
        self.scaleFactor = np.ones(n)


    def setParamFromVars(self,varList):
        self.scaleFactor = np.array([ v.scaleFactor for v in varList ] ,dtype=np.float32) 
        self.offset = np.array([ v.offset for v in varList ] ,dtype=np.float32) 
        self.paramValid = True
        
    def setFromMeanStdErr(self,X):
        self.scaleFactor = 1/ X.std(0)
        self.offset      = - X.mean(0)*self.scaleFactor
        self.paramValid = True

    def scale(self, X):
        if self.paramValid:
            r = X*self.scaleFactor
            r += self.offset
            return r
        else:
            return X

    def scale_inplace(self, X):
        if self.paramValid:
            # use in-place operations : attempt to minimize memory usage in case of big arrays (?) 
            X *= self.scaleFactor
            X +=self.offset
            

    def full_scale_factor(self):
        return self.std*self.scaleFactor

    def full_offset(self): # actually the offset when unscaling !
        return self.mean - self.offset*self.full_scale_factor()

    def scale_parameters(self,i=None):
        # returns (a,b) such that  x_scaled = a*x+b
        if i is None:
            return (self.scaleFactor, self.offset )
        return (self.scaleFactor[i], self.offset[i] )
    
    def transform(self, X):
        return self.scale(X)


    def unscale(self,X):
        if self.paramValid:
            # use in-place operations : attempt to minimize memory usage in case of big arrays (?) 
            r= X - self.offset
            r /= self.scaleFactor
            return r
        else:
            return X

    def unscale_inplace(self,X):
        if self.paramValid:
            X -=self.offset
            X /= self.scaleFactor


    def save(self, f, name):
        if isinstance(f,str):            
            scaleParF = h5py.File(f, 'a')
        else:
            scaleParF = f
        if name in scaleParF:
            scaleParF[name][:]=np.stack([self.offset, self.scaleFactor])
        else:
            scaleParF.create_dataset(name, data=np.stack([self.offset, self.scaleFactor]) )
        if isinstance(f,str): scaleParF.close()

    def loadFromFile(self, f, name):
        if isinstance(f,str):            
            scaleParF = h5py.File(f, 'r')
        else:
            scaleParF = f
        self.offset ,self.scaleFactor = scaleParF[name][:]
            
        if isinstance(f,str): scaleParF.close()
        self.paramValid = True
        

class NoScaler(VariableScaler):
    def scale_inplace(self, X):
        pass
    def scale(self, X):
        return  X
    def unscale(self,X):
        return X
    def unscale_inplace(self,X):
        pass



## ***********************************************************************
## Custom Layers
## ***********************************************************************     

    
class GausAnnotation(layers.Layer):
    
    def __init__(self, dim, centers, sigmas2, includeInput=False, offset=-0.5,  **kwargs):
        super(GausAnnotation, self).__init__(**kwargs)
        self.centers = centers
        self.sigmas2  = sigmas2
        self.dim = dim
        self.includeInput = includeInput
        self.offset = offset
        #print("UUUUUUUUUUU", sigmas2, self.sigmas2)

    def build(self, input_shape):
        self.c_w = tf.constant( self.centers, shape = ( len(self.centers),) , dtype='float32')
        self.w_w = tf.constant( self.sigmas2, shape = ( len(self.centers),) , dtype='float32')
        

    @tf.function
    def call(self, inputs):
        entry = tf.tile( inputs[:,self.dim:self.dim+1], [1,len(self.sigmas2)] )
        n = tf.subtract(entry, self.c_w)
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        if self.includeInput:
            return tf.concat( [annot, inputs[:,self.dim:self.dim+1] ], 1)
        return annot
    
    def get_config(self):
        config = super(GausAnnotation, self).get_config()
        config.update( dict(centers= self.centers,
                            sigmas2 = self.sigmas2,
                            dim = self.dim,
                            includeInput = self.includeInput,
                            offset = self.offset,

                            )        
        )
        return config            

    
    def test(self, entry):
        tf = np
        n = tf.subtract(entry, self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        return annot
    
class GausAnnotationSym(GausAnnotation):
    @tf.function
    def call(self, inputs):
        entry = tf.tile( tf.abs(inputs[:,self.dim:self.dim+1]), [1,len(self.sigmas2)] )
        n = tf.subtract(entry, self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        if self.includeInput:
            return tf.concat([annot, inputs[:,self.dim:self.dim+1]] , 1)
        return annot

    def test(self, entry):
        tf = np
        n = tf.subtract(tf.abs(entry), self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        return annot

    
class ExtractN(layers.Layer):
    def __init__(self, pos, **args):
        super(ExtractN,self).__init__(**args)
        self.pos = pos


    @tf.function
    def call(self, inputs):
        n0 = self.pos
        return inputs[:,n0:n0+1]

    def get_config(self):
        config = super(ExtractN, self).get_config()
        config.update( pos=self.pos )                
        return config            

class ExcludeIndices(layers.Layer):
    def __init__(self, exclude, **args):
        super(ExcludeIndices,self).__init__(**args)
        self.exclude = exclude

    def build(self, input_shape):
        self.include = [ i for i in range(input_shape[1]) if i not in self.exclude]


    @tf.function
    def call(self, inputs):
        l = [ inputs[:,i:i+1] for i in self.include ]
        return tf.concat( l , 1 ) 

    def get_config(self):
        config = super(ExcludeIndices, self).get_config()
        config.update( exclude=self.exclude )        
        
        return config            



def reset_regularizer(model, v, regType='all', layers=None):

    if regType=='all':regTypes = ['kernel_regularizer','bias_regularizer']
    elif isinstance(regType,str): regTypes=[regType]
    else: regTypes=regType  # assuming regType is a list
    if layers is None : layers = model.layers
    else: layers = [ model.get_layer(lname) for lname in layers ]

    for l in layers:
        if isinstance(l, tf.keras.Model):
            reset_regularizer(l, v, regTypes)
            continue
        for regType in regTypes:
            reg=getattr( l, regType, None)
            if reg is None:continue            
            reg.l2[()] = v
            

def show_regularizer(model, prefix='' ):
    regTypes = ['kernel_regularizer','bias_regularizer']
    for l in model.layers:
        if isinstance(l, tf.keras.Model):
            show_regularizer(l, prefix+'--')
            continue
        for regType in regTypes:            
            reg=getattr( l, regType, None)
            if reg is None:continue
            print(prefix, l.name, regType, reg.l2)


def findDenseAncestorLayers(model, initname='outputE'):
    """Returns the Dense layers inside model which are ancestors of the layer named initname
      returns a list of layers.
      Reccursively inspect all sub-models inside model
    """
    layers=[]
    targetL=None
    # inspect sub-models and look for the layers named initname
    for l in model.layers:
        if isinstance(l, tf.keras.Model):
            layers += findDenseAncestorLayers(l,initname)
        elif l.name == initname:
            targetL = l
            break

    if targetL is None:
        # return what we have found in sub-models (can be [])
        return layers

    Dense = keras.layers.Dense    
    ancestors = dict()
    def _recL(l):
        if 'dense' in l.name:
            ancestors[l.name] = l
        for n in l.inbound_nodes:
            for ltuple in n.iterate_inbound():
                # ltuple is in the form (layer, ..,..., tensor)
                _recL(ltuple[0])
    _recL(targetL)
    
    layers += [ancestors[n] for n in sorted(ancestors.keys()) ]
    return layers


def printLayerNorms(model, ancestorsOf=None):
    layerL = model.layers if ancestorsOf is None else findDenseAncestorLayers(model,ancestorsOf)
    for li,layer in enumerate(layerL):

        if 'dense' in layer.name:
            a=layer.get_weights()[0]
            b=layer.get_weights()[1]
            nL = [np.linalg.norm(a[:,i]) for i in range(a.shape[1])]
            print( '\n{} layer {} w (max,min )norms =({:.2f},{:.4f})  bias (max,min)=({:.2f},{:.4f}) '.format(li, layer.name,
                max(nL), min(nL),  max(b), min(b)) )
    


def copyWeights(src, dest, nLayer=-1, layerFilter=lambda l:False, verbose=True):
    """Copy weights between 2 keras models : src to dest  """
    
    for lcount, l in enumerate(dest.layers):
        if lcount==nLayer: break
        if layerFilter(l): continue
        if l.get_weights() == []:
            continue
        try:
            lo = src.get_layer( l.name )
        except:
            print ("WARNING  : weight layer ",l.name, l.output.shape, " not found in source !")
            continue
        if verbose:
            print( 'this layer ',l.name, l.output.shape,  '   from  ', lo.name, lo.output.shape)
        l.set_weights(lo.get_weights())
