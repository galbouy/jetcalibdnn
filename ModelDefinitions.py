"""Defintions of models to perform Jet calibration

This module defines helper objects which build keras.Model for a Trainer object.
The objects are instances of ModelDefiner and have a __call__ methods so can be used as a function.
Concrete model building classes typically inherits ModelDefiner as well as other classes, each defining
the head, the core and the tail of the NN.

Such model defining objects are communicated to the trainer through the configuration by setting them as in
  config.modelBuilder = MyDefinerClass() 

The object is then called in the Trainer.setupModel() function.

"""
import numpy as np

from .ImportKeras import *
from . import NNUtils as utils


## ***********************************************************
from collections import defaultdict
_layerNames = defaultdict( int )
def _layerName(n=None):
    global _layerNames
    if n is None :
        _layerNames= defaultdict( int )
        return
    if isinstance(n,layers.Layer):
        n = n.__class__.__name__
    i = _layerNames[n]
    _layerNames[n] = i+1
    if i==0:
        return n
    return f'{n}_{i}'


def denseBatchNormF(layer,*args, **kwargs):
    """A shortcut to add a Dense layer with dropout, batch normalization (broken ???), custom activations,... """
    batchNorm = kwargs.pop('doBatchNorm',False)
    dropout = kwargs.pop('dropout', None)
    activ = kwargs.pop('activation', 'relu')    
    activName = kwargs.pop('activ_name', None )

    kwargs['name'] = _layerName(kwargs.get('name','dense') )
    nl =  layers.Dense( *args,  **kwargs)(layer)
    if batchNorm:
        nl =  layers.BatchNormalization(name='batchnorm')(nl)
    nl = layers.Activation( utils.activationDict.get(activ, activ) , name=activName)(nl)

    if dropout:
        nl= layers.Dropout(dropout, ) (nl)
    return nl



def symetrize(l, f=-1):
    return [f*u for u in  l[::-1] ] + l[1:]


## ***********************************************************

class ModelDefiner:
    N = 100
    core_constraints = dict(
        #batchNorm=True,
        kernel_regularizer=keras.regularizers.l2(0.0001),
        bias_regularizer = keras.regularizers.l2(0.0001),
    )

    tail_constraints = dict(
        bias_regularizer = keras.regularizers.l2(0.001),
        kernel_regularizer=keras.regularizers.l2(0.001)
    )

    batchNorm = False

    last_activation='tanh'
    scale_output = True  # this is set automatically from last_activation
    

    # If not None, bypass the evaluation of number of output values from trainer.config
    forceNPredValues = None
    #  (used to build copies of NN predicting only some of the output)
    
    def __init__(self, **args):
        for (k,v) in args.items():
            setattr(self,k,v)        
        self.scale_output = ( self.last_activation in ['tanh', 'tanh0']  )
            
        
    def __call__(self,trainer, ):
        self.modelTags = []

        _layerName() # reset count
            
        inputN = len(trainer.config.features)
        inputs =layers.Input(shape=(inputN,) )

        self.inputs = inputs

        if trainer.config.lossType != "MDN":
            if self.last_activation=='tanh0':
                self.last_activation='tanh'
        
        layer = self.modelHead(trainer, inputs)
        layer = self.modelCore(trainer, layer)
        layer = self.modelTail(trainer, layer)

        net = Model(inputs=inputs, outputs=layer)

        if not self.scale_output:
            self.modelTags +=['uo']
        self.modelTags += ['N'+str(self.N)]
            
        self.setModelTag(trainer)

        return net

    def setModelTag(self,trainer):
        trainer.config.modelTag = ''.join(self.modelTags)


    def nPredictedValues(self, trainer):
        if self.forceNPredValues is not None:
            return self.forceNPredValues

        return trainer.config.nPredictedParam
        # ntype =  len(config.originalTargets) # 1 if only predicting E, 2 if predicting E and M

        # return len(config.targets) // ntype



class DenseCoreT3:
    def modelCore(self,trainer, layer):
        N = self.N
        l = layer
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.15,**l_args )
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, **l_args)
        l=denseBatchNormF( l, int(0.3*N),activation='mish' ,dropout=0.2, **l_args)
        self.modelTags += ["T3"]
        return l





class Identity:
    def modelHead(self, trainer, inputs):
        self.modelTags += ['NoAnnot']
        return inputs
    

def toCenters( l ):
    n = len(l)
    print( n-1 )
    for i in range(n-1):
        a,b=l[i:i+2]
        print ( (0.5*(a+b), (b-a)*0.5 ) , ",")  
    
etaCoverages = dict(

    full = [(0, 0.2), (0.3, 0.2), (0.6, 0.2), (0.92, 0.22), (1.15, 0.15), (1.3, 0.15), (1.4, 0.15), (1.6, 0.3), (2.0, 0.5), (2.8, 1.2), (3.6, 1.5)],
    
)

class EtaAnnotBase:
    etaAnnotClass = None
    eta_scale = 4.02 # IMPORTANT should be set to inscaler.full_scale_factor(), this is done in sample-specific setup files such as setupAntiKt4EMPFlowJets.py

    coverage = 'full'

    etavar= 'eta' # name of the variable. could be eta_det or rapidity 

    offset= -0.5

    def etaAnnotations(self, coverage, dosymetrize=False):
        """returns the centers and squared widths of eta regions according to coverage (see the etaCoverages dict for the definitions of regions)
        centers value and width are properly scaled according to self.eta_scale . 
        """
        l_eta, l_eta_w = zip( * etaCoverages[coverage ] ) 

        if dosymetrize:
            l_eta = symetrize( list(l_eta) )
            l_eta_w = symetrize(list(l_eta_w), 1)

        eta_centers = np.array( l_eta  )  / self.eta_scale
        eta_w = np.array( l_eta_w )/ self.eta_scale


        print(self.eta_scale)
        return  eta_centers, eta_w
    
 
class _EtaBlockG_Annot(EtaAnnotBase):
    def buildEtaAnnotLayer(self, trainer, inputs):
        N = self.N

        s= '' if self.symetrizeCenters else 'S'
        c = 'c' if self.coverage=='cracks2'  else ''
        self.modelTags += [ f'EtaG{s}Block{c}']
        
        eta_centers, eta_w = self.etaAnnotations(  self.coverage, self.symetrizeCenters)
        if self.etaAnnotClass.__name__.startswith('Gaus'):
            eta_w *=eta_w # the custom layers actually expects the sqared widths 
        eta_ind = trainer.config.features.index(self.etavar)        
        # build the layer by callingg etaAnnotClass, which is GausAnnotationSym or GausAnnotation, see below 
        l_eta = self.etaAnnotClass(eta_ind, eta_centers, eta_w,name='Eta_GausAnnot', includeInput=True, offset=self.offset )(inputs)

        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        neta = max(20, min(int(N/15),40))
        l_eta=denseBatchNormF( l_eta, neta ,activation='mish' , **l_args )                

        # concatenate the eta inputs after dense block, with the rest of inputs.
        # ExcludeIndices -> do not concatenate eta again, since it is part of the block
        l= layers.concatenate( [l_eta, utils.ExcludeIndices([eta_ind])(inputs) ] )
        return l

class EtaBlockGSAnnot(_EtaBlockG_Annot):
    etaAnnotClass = utils.GausAnnotationSym
    symetrizeCenters = False # No need to symetrize the eta centers, since we're using abs(eta) in this case
    modelHead = _EtaBlockG_Annot.buildEtaAnnotLayer
    

# ----------------------------------
# Classes for model tails

class AttentionMDeepTail6():
    
    def modelTail(self, trainer, l):
        
        lastregs = self.tail_constraints
        l_args = dict(doBatchNorm=self.batchNorm, **self.core_constraints)
        N = self.N

        if len(trainer.config.originalTargets)==2:
            l1=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.4, activation='mish', **l_args)
            l1=denseBatchNormF( l1, N*0.2, activation='mish', **l_args)
            l1=denseBatchNormF( l1, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputE', **lastregs)

            attl = denseBatchNormF(self.inputs, N*1.5, activation='mish', **l_args)

            l2=denseBatchNormF( l, N*0.6, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.4, activation='mish', **l_args)
            l2=denseBatchNormF( l2, N*0.2, activation='mish', **l_args)
            attl = denseBatchNormF(attl, l2.shape[1], activation='softmax', **l_args)
            l2 = layers.multiply( [l2,attl] )
            l2=denseBatchNormF( l2, self.nPredictedValues(trainer), activation=self.last_activation,activ_name='outputM', **lastregs)

            l = [l1,l2]
        else:
            raise Exception("LayerDeepTail requires 2 outputs")
        
        self.modelTags += ["AtMDT6"]

        return l


## ***********************************************************
# Class definition for full model

class HeadEtaBlockCoreT3AttMDeepT6(ModelDefiner, EtaBlockGSAnnot, DenseCoreT3, AttentionMDeepTail6):
    N = 200
