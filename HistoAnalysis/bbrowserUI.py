# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'bbrowserUI.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1116, 634)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Dialog)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame = QtWidgets.QFrame(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.optionEdit = QtWidgets.QLineEdit(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.optionEdit.sizePolicy().hasHeightForWidth())
        self.optionEdit.setSizePolicy(sizePolicy)
        self.optionEdit.setObjectName("optionEdit")
        self.verticalLayout.addWidget(self.optionEdit)
        self.spinBox_1 = QtWidgets.QSpinBox(self.frame)
        self.spinBox_1.setWrapping(True)
        self.spinBox_1.setStepType(QtWidgets.QAbstractSpinBox.DefaultStepType)
        self.spinBox_1.setObjectName("spinBox_1")
        self.verticalLayout.addWidget(self.spinBox_1)
        self.spinBox_2 = QtWidgets.QSpinBox(self.frame)
        self.spinBox_2.setObjectName("spinBox_2")
        self.verticalLayout.addWidget(self.spinBox_2)
        self.spinBox_3 = QtWidgets.QSpinBox(self.frame)
        self.spinBox_3.setObjectName("spinBox_3")
        self.verticalLayout.addWidget(self.spinBox_3)
        self.spinBox_4 = QtWidgets.QSpinBox(self.frame)
        self.spinBox_4.setObjectName("spinBox_4")
        self.verticalLayout.addWidget(self.spinBox_4)
        self.drawButton = QtWidgets.QPushButton(self.frame)
        self.drawButton.setObjectName("drawButton")
        self.verticalLayout.addWidget(self.drawButton)
        self.horizontalLayout.addWidget(self.frame)

        self.vBox = QtWidgets.QWidget(Dialog)
        self.vLayout2 = QtWidgets.QVBoxLayout(self.vBox)

        self.horizontalLayout.addWidget(self.vBox)

        
        self.retranslateUi(Dialog)
        self.drawButton.clicked.connect(self.spinBox_1.stepUp)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.drawButton.setText(_translate("Dialog", "draw"))
