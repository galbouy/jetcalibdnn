Calibration of jet E and mass with DNN
===========================================================

This package contains python code to train DNN to perform a MC-based simultaneous energy and mass calibration of jets.


It allows to use a "direct" approach calibration, focus of the QT [ATLJETMET-1377](https://its.cern.ch/jira/browse/ATLJETMET-1377) and documented in an internal note [ATL-COM-PHYS-2023-283](https://cds.cern.ch/record/2856774) and a publication [CERN-EP-2023-250](https://cds.cern.ch/record/2880114).


The package allows to derive a direct calibration, using only 1 NN in a simple approach :

  * NN learns $r_{reco} = mode(X_{reco}/X_{true})$ as a function of ($X_{reco}, \theta$)
  * The calibration is given by $X_{calib} = X_{reco} / NN(X_{reco},\theta)$ 


Dependencies
------------------

The code here uses tensorflow with the keras API. It depends on
```
tensorflow # version 2.2 or above
tensorflow_addons
numpy
h5py
uproot  # version 4.1.7 or above
numexpr
```

Setup
------------

A clean way of  using this package would be to follow this instructions :
```bash
git clone https://gitlab.cern.ch/delsart/jetcalibdnn.git JetCalibDNN
pip install -r JetCalibDNN/requirements.txt 

# create an independent directory to run the scripts  
mkdir run/
cd run/
# do this only once per run/ directory :
source ../JetCalibDNN/trainscripts/setupLinks.sh
# or source ../JetCalibDNN/trainscripts/clustertraining/setupLinks.sh

# do this on each login :
source setupPATH.sh
```

Inputs
--------

This package is set up to process many hundred millions of jets. As
the corresponding data may not be able to fit in memory, the approach
is to stream the data from the input files, loading ~10M at a time.

The data is provided to keras through a custom keras.Sequence.
The input files are expected to be flat root ntuples (1 entry per jet)
and are read in by a dedicated helper class
(`GeneratorFromRootFile.TreeChain`) using the uproot module.

In order to create the flat ntuples from regular ntuples (or even
DAOD), utilities scripts in inputs_utilities/ are available.


Code organisation
-----------------

`Variables.py` define the base `Variable` class which is used to gather information about an input variable. The information includes : name in input file, how to transform the variable (ex: take the log(E) or build a ratio from 2 other variables), how to filter events according to it, how to scale the variable before feeding to the NN, etc... The file contains the list of known `Variable` instance to be used.

`ROOTDataGenerator.py` contains code related to reading the input data and feeding it to the NN.  The `ROOTDataGenerator` class is a  custom keras.Sequence implementing the reading of the variables, performing filtering and preprocessing (according to the list of `Variable` it holds), and providing the training batches to tensorflow. In practice we use some derived classes dedicated to 1 (only JES calib) or 2 targets (JES and JMS calib) and enabling Multi-Threaded loading (eg `Generator2TargetMT`)

`ModelDefinitions.py` defines classes which builds keras.Model. The model builders are organized this way to ease the definition of many models, varying only some part of it (head, core, tail) and automatically providing identification tags for bookkeeping.  

`NNutils.py` defines customs loss functions, normalizers for inputs and targets, and custom keras.Layer such as the `GausAnnotation`.  

`ConfigUtils.py` defines the specialized dictionnary used to store all options `ConfigDict`. Also defines some default values for the configuration.

`Trainer.py` defines the `Trainer` class. This is the top-level object bringing all the above pieces together and from which we can prepare the data, run training, save or load model weights, create numerically inversed data or calibrated data.

### scripts

`trainscripts/jetCalibDNN.py` example of a main script  setting up a custom configuration, instantiating a `Trainer` object and defining function to run a training sequence. This is a rather generic script, and it expects to be invoked together with a second script dedicated to specifics of the current training (ex : input files and variables for training a specific jet collection)

`trainscripts/setupA10CSSKUFOSoftDrop_mc16.py` example of a specific script to be invoked together with `jetCalibDNN.py`. It define usefull informations needed by the Trainer class : on input files (directory, name pattern, tree name, ...), on the input features (names from Variables.py and corresponding name in input trees), define normalisation parameters of each variables.

`histoscripts/*.py` methods for drawing graphics and tests. Many
methods are defined as additions to the `Trainer` class.

Training
------
The package can be run both interactively (strongly recommended when testing/developping) or in batch systems.

First define a top-level python script. This script will  (a generic example is `jetCalibDNN.py`) :
  * define a configuration (typically by calling `conf=defaultConfig.clone( ... )`)
  * intantiate a `Trainer` object
  * define a function specifying a training sequence (that is, a sequence of call to `trainer.refit(..)` which is just a wrapper around  `keras.Model.fit( )` )


Then the starting point is the  `Trainer.train()` method. It is called as `trainer.train(config, defaultSequence, reload)` where
 * `config` is a configuration dict, the one defined in the top-level script.
 * `defaultSequence` is the function taking a `Trainer` as argument and implementing the training calls. (see `testSequence()` in  `jetCalibDNN.py` as a minimal example)
 * `reload` is False if we want to train a NN from scratch and True if we only want to reload an existing NN. In the later case, the NN is read from a file which name is built according to `config`.


One key configuration entry is `config.mode` which must be in the form 'XX:YY' where
 * XX describes the input type and is either 'true' or 'reco' (in practice we always use 'reco')
 * YY decribes if we're going to train or build predictions for the full dataset, and is either 'train' or 'predict'

Then a typicall call looks like :

`trainer.train( conf.update( mode = 'reco:train'), defaultSequence=testSequence, reload=0)`

We use the `config.update()` call to update on the fly the config defined in the top-level script.

Here are some example in interactive mode, assuming the setup described above has been executed.


### Training with direct calib ###
This approach seems to give the best performance. 

We use 'reco' as the input mode and only perform 1 training :
```python
# Launch python with : python3 -i jetCalibDNN.py --variableFile setupA10CSSKUFOsoftDrop.py 
#  (that is when python gives us the prompt it would have executed the 2 above scripts)

# start training from scratch : 

trainer.train(conf.update( nInputFiles=-1,  modelBuilder=ModelDefinitions.HeadEtaGETCore1Tail1(), mode='reco:train'  ), reload=0 , fitSequence=defaultSequence)

# Save the state of the model :
trainer.save_model()
```

  * `conf` is the default config defined in this script. In the above line it is passed to train() after being updated on some of its content. This allows to customize the training on-the-fly (i.e without updating the conf in the jetCalibDNN.py script)
  *  `modelBuilder` is set to a `keras.Model` building instance.
  *  `fitSequence` is a function defining several calls to trainer.refit() , see below for examples.

`trainer.refit()` is just a wrapper around `keras.Model.fit` which
allows to easily re-start the training with updated loss function or
alternate sampling weights or alternate input filtering.
Thus, if necessary, we can perform more training steps, calling refit again :

```python
# continue training for 2 epochs, setting the optimizer, the loss functions (here 1 for the energy, 1 for the mass), and the relative weights for each loss function
trainer.refit(batch_size=105000, optimizer=diffgrad(1e-5), nepoch=2, loss=[utils.truncmdnLoss(1.),utils.truncmdnLoss(1.)], partition=None,loss_weights=[1.,1.], metrics=[utils.lgkLoss(1e-3,1e-3)], sample_weight=None)

# then save again :
trainer.save_model()
```


Finally, it is possible to calculate the calibrated E and/or mass at each event and save them into output file  :
```python
# reload the NN (setting reload=1) :
trainer.train(conf.update( nInputFiles=-1,  modelBuilder=ModelDefinitions.HeadEtaGETCore1Tail1(), mode='reco:predict'  ), reload=1 , fitSequence=None)
# build the calibrated quantities :
trainer.createCalibDataset()
```
Alternatively, instead of calling `trainer.createCalibDataset()` and
creating new ntuple of calibrated mass and e, we can choose to
directly produce response histograms in a binned phasespace (typically
bins of E,m and eta) in order to compare with ATLAS standard calib
and draw the usual response & resolution curves. See below.

Final configuration and 
------

Input features = ['eta', 'e_var', 'm_var', 'Width', 'EMFrac', 'EM3Frac', 'Tile0Frac', 'EffNConsts', 'groomMratio', 'neutralFrac', 'sumPtTrkFrac', 'sumMassTrkFrac', 'Split12', 'Split23', 'C2', 'D2', 'Tau32', 'Tau21', 'Qw', 'mu', 'NPV', ]  (define in setupA10CSSKUFOSoftDrop*.py)

NN architecure :  `ModelDefinitions.HeadEtaBlockCoreT3AttMDeepT6(N = 700, last_activation='tanhp')`  (define in ModelDefinitions.py)

Training procedure : `defaultSequence(trainer)`  (define in `jetCalibDNN.py`)

Full training can be launched from `jetCalibDNN.py``:
```bash
python3 jetCalibDNN.py --variableFile setupA10CSSKUFOSoftDrop_mc16.py --training
```


## Plots and validation  ##


## Info on the model & predictions ##

From an interactive session:
```
exec(open('plotAndDebugDNN.py').read()) 
```

By executing this scripts we make new plotting functions availabble to the `trainer` instance.

Then :
```python
## Draw response & resolution predictions vs E or vs Eta
trainer.checkNN(otherVar=dict(fixedToMean=True, m=40) )

# The predictions will be drawn at mass fixed at m=40. One can fix any other variable by passing values to 'otherVar'
# like in otherVar=dict(fixedToMean=True, m=40, D2=0.5)
# 'fixedToMean=True' means all input variables not fixed by this call will be fixed to their mean values on all the sample.
# 'fixedToMean=Fale' means all input variables not fixed by this call will be set to their mean in the bin around the (e,m,eta) values being drawn. This works only if the relevant histos have been prepared beforehand


# dump the NN
trainer.net.summary()

# collect several plots in a pdf :
trainer.savePDFsample()

# print the maximum & minimum of the rows of the matrix of each dense
#  layer:
trainer.printLayerNorms()

# draw histograms of input features :
trainer.histogramInputs()
```

## Creating response histograms ##

The system can create a set of mass and E response histograms arranged
in a binned phase space identical to what is produced by the
`DeriveJetScale` package (but much faster !!).

From these histos, we can then draw the response and resolution
curves. 

Open an interactive session and load trained network:
```bash
python3 jetCalibDNN.py --variableFile setupA10CSSKUFOSoftDrop_mc16.py --prediction 
```

Then
```python
# first execute a helper scripts which setups the histogram building (including binned phase space and histogram containers):
exec(open('HistoAnalysis/buildBHfromTrainer.py').read()) 

# Then build histograms of energy and mass response in a specific Binned phase space defined in buildBHfromTrainer.py.
# this can be done in 1 command : 
trainer.runHistoFilling(conf, [eR_in_BINS_dnn, mR_in_BINS_dnn, ] , beginFileTask=predCalib, )
# - 1st argument is just the exact same config arguments we have passed to trainer.train( )
# - 2nd is the list of histogram containers we want to build
# - beginFileTask is a function which is called for each input file : it just performs the DNN prediction for the content of the file.

# if we're building the histos for the uncalibrated and ATLAS calibration response, do not set 'beginFileTask'
#  but we may want to add inputFriends args to the config in case the ATLAS-calibrated quantities are in a different set of files :
# (and in that case the modelBuilder argument is irrelevant, we can put anything there)
trainer.runHistoFilling(conf.update(inputFriends=prefix+'CalibJZ_*.root', ), \
              [eR_in_BINS_uncal,  eR_in_BINS_cal ] , beginFileTask= None\
	            outName="AtlasCalib_resp.h5" )

```

This will create the histograms and save then in an output .h5 file.



## Analysing response histograms ##
The package provides also utilities to analyse and plot the histograms produced at the previous step.

`BinnedArraysGraphics.py` provide plotting helpers. In particular the
`GraphicSession` class allows to read back and manipulate many
`BinnedHistos`.
There are examples in `histoscript/initRespAnalysis.py`. What it does essentially is
define a `GraphicSession` object with :
```python
from JetCalibDNN.HistoAnalysis import BinnedArraysGraphics as bag 
gs = bag.GraphicSession( )
```

and then read-in `BinnedHistos` from several .h5 files with
```python
gs.readBAFromFile('afilename.h5')
gs.readBAFromFile('aOtherfilename.h5')
...etc...
``` 

Then `gs` will hold all `BinnedHistos` from the read files and make
them available.

A commented example interactive session :
```python
# assume the session is started with python -i initRespAnalysis.py
# and the initRespAnalysis.py did properly call readBAFromFile()

# we can access the BinnedHistos :
>>> gs.eR_in_Eeta_uncal
<JetCalibDNN.HistoAnalysis.BinnedArrays.BinnedHistos object at 0x7f6bed937c10>

# by default only the definitions were loaded. The actual content
# needs to be loaded explicitly :
>>> gs.eR_in_Eeta_uncal.loadContent()

# now we can access histograms individually (the argument is a tuple : 
# the indices of the bin. Here, a 2D bin) :
>>> gs.eR_in_Eeta_uncal.histoAt( (3,1) )
<JetCalibDNN.HistoUtils.Histo1D object at 0x7f6bed82bb20>

# or draw it :
>>> gs.eR_in_Eeta_uncal.drawAt( (3,1) )
[<matplotlib.lines.Line2D object at 0x7f6bb42e1d60>]

# there are advanced functions like drawManyHistos, which draw histos
# from different BinnedHistos into 1 plot :
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] , coords=np.s_[2,3], nplots=1)
# above we pass the list of BinnedHistos, the coordinates in numpy
# slicing format (hence the np.s_ ) and the number of subpot per
# widow.

# to draw 4 sub-plots at positons (2,3), (2,4) , (3,3), (3,4) do :
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] , coords=np.s_[2:4,3:5], nplots=4)

# to save this exact graph in a pdf, do :
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] ,coords=np.s_[2:4,3:5], nplots=4, outname='outHisto.pdf')

# To draw ALL the histos with 4 subplots per page into a multi page pdf, just
# ignore the coord argument : 
>>> gs.drawManyHistos( [gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal ] ,nplots=4, outname='outHisto.pdf')

```

The package also provides a complete script to compute the response closure and resolution for any `BinnedHistos` : `responseAnalysis.py` 
It can be used in interactive mode initialised with initRespAnalysis.py and instantiates the `RespWrapper` class.
A commented example :
```python
# assume the session is started with python -i initRespAnalysis.py
# and the initRespAnalysis.py did properly call readBAFromFile()

# init a RespWrapper object with a list of bh and a function for computing the response mode (gauss or gauss2side). If no function provided mode replaced by mean
>>> resp = RespWrapper([gs.eR_in_Eeta_uncal, gs.eR_in_Eeta_cal], gauss2side)

# to compute response mode (i.e found mode of the histogram in each bins) -> add an attribute to the bh (resp and resolution)
>>> resp.getAllRespInBH()

# to save the modified bh
>>> resp.saveBH()

# to plot the response closure or resolution regardless of the binned phase space (BPS)
>>> resp.globalPlotFct( plot='resp', bins=[None,0,2])
# this will plot the response closure as a function of the first axis of the BPS for the bin 0 of axis 2 and bin 2 of axis 3
# any BPS can be plotted (2D,3D,4D,...), needs to give to bins an array of the same size as the number of dimensions and with one None

```



