
if 0:
    import keras
    import keras.backend as K
    from keras.models import Sequential, Model
    from keras.constraints import max_norm
    from tensorflow.keras.constraints import  max_norm
    import keras.layers as layers
    from keras.constraints import  max_norm
else:
    import tensorflow as tf
    from tensorflow import keras 
    from tensorflow.keras import backend as K

    from tensorflow.keras.models import Sequential, Model
    from tensorflow.keras import layers
    from tensorflow.keras.constraints import  max_norm
    from tensorflow.python.framework.ops import disable_eager_execution
    
    #disable_eager_execution()
    import tensorflow_addons as tfa
