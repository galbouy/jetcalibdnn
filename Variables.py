"""
Define utilities to represent and manipulate variables to be used as inputs or targets.

"""
import numpy as np
import numexpr as ne
import tensorflow as tf

from .ConfigUtils import ConfigDict
from . import NNUtils as utils
from .HistoUtils import Histo1D, Histo2D


class Variable:
    """ Helper class to describe a variable used as an input or a target
    
    This class holds the variable name, description and a set of functions used to filter or transform the data (ex: m --> log(m))
    It also holds a slice to the numpy array containing the data (in self.array)

    """
    
    array = None
    interpretation = None
    referenceVars = ConfigDict()
    aliases = []
    from_src=True
    update_order = None
    #filterFunc = None
    is_sampleW = False

    dependencies = [] # the list of Variable instance name, this instance requires.
    chain = None

    pre_filter = []
    post_filter = []


    # scaling factor and offset to be applied once the variable has been scaled by NNutils.VariableScaler
    # The goal is to adjust  the scaling so the final scaled variable fit well within [-1,1]
    #  Precisely if this variable has index i in the variable list :
    #        - scaleAdjust is set  to inscaler.scaleFactor[i]
    #        - offsetAdjust is set to inscaler.offset[i]
    # scaleAdjust = 3. 
    # offsetAdjust = 0.

    scaleFactor = 1
    offset = 0
    
    # configuation when plotting histo with this variable
    h_nbins = 150
    h_range= None

    is_target = False # set to True automatically by Trainer    



    isElike = False
    
    #src_indices = None # array containing indices of all sources, == np.arange(len(src_array))
    def __init__(self,name, src_name=None, array_transfo=[],array_revtransfo=[], array_filter=None, title=None, desc=None, irrev_array_transfo=[], **args ):
        self.name=name        
        if callable(src_name): src_name = src_name(name)
        else: src_name = src_name or name
        self.src_name = src_name 
        self.array_transfo = array_transfo
        self.array_revtransfo = array_revtransfo
        self.array_filter = array_filter
        
        self.irrev_array_transfo = irrev_array_transfo # some vars need a single irrev transfo at load time. we use this list to store such transfo
        self.default_filter = array_filter
        self.title = title or name 
        self.desc  = desc or name
        
        for k,v in args.items():
            setattr(self,k,v)
        self.update_after = args.pop('update_after', [] )
        
        Variable.referenceVars[name] = self

    def clone(self):
        from copy import copy
        return copy(self)
    
    def setup(self, chain, array ):
        self.chain =chain
        self.array = array
    

    def update_array(self, reverse=False):
        a = self.array
        ldict = dict(a=a, **numExprConst)
        transformations = self.array_revtransfo if reverse else self.array_transfo
        for tf in transformations:
            ne.evaluate( tf , local_dict=ldict,out=a)
        #print ("updated ",self.name, "reverse=",reverse, a[:4])

    def irrev_update_array(self, ):
        ldict = dict(a=self.array, **numExprConst)
        for tf in self.irrev_array_transfo:
            array=ne.evaluate( tf , local_dict=ldict, out=self.array)

        
    def transformed_array(self, reverse=False , array=None):
        #print ("updating ",self.name)
        array = array if array is not None else  self.array
        transformations = self.array_revtransfo if reverse else self.array_transfo
        ldict = dict(a=array, **numExprConst)
        for tf in transformations:
            #print "tf"
            ldict['a'] = array
            array=ne.evaluate( tf , local_dict=ldict)
        return array

    
    def untransformed_array(self):
        return self.transformed_array(reverse=True)
    


    def build_pre_maskers(self):
        """Build the list of masker functions corresponding to the pre_filters """
        utils.debug('build maskers ',self.name, self.pre_filter)
        return self.build_maskers(self.pre_filter)
    def build_post_maskers(self):
        """Build the list of masker functions corresponding to the post_filters """
        return self.build_maskers(self.post_filter)


    def build_single_masker(self, filterFunc ):
        """Builds a masker function from filterFunc : the returned function perfoms the operation 'masked_entries &= filterFunc(self.array)'  """
        def _f(masked_entries):
            masked_entries &= filterFunc( self.array )
            print ("filtered ", self.name , " : ",np.count_nonzero(masked_entries), self.array[:4] ) 
        return _f

    def build_maskers(self, filters):
        """return a list of masker functions for self.array from the input filter functions (can be a either a list of func or a func)  """
        if callable(filters): return [ self.build_single_masker(filters) ]
        return [ self.build_single_masker(f) for f in filters ]
        
        
    def reset_filter(self):
        self.array_filter = self.default_filter

    def calculate_update_order(self):
        if self.update_order is not None:
            return
        if self.update_after==[]:
            self.update_order =0
            return
        others = []
        for vname in self.update_after:
            v=self.chain.allVars[vname]
            v.calculate_update_order()
            others.append( v.update_order)
        self.update_order = min(others)-1



    def overwrite_scaler(self, trainer):
        pass

    def setScaleFactors(self, scaleFactor=None, offset = None):
        if scaleFactor!=None : self.scaleAdjust = scaleFactor
        if offset !=None     : self.offsetAdjust = offset

    def setScaleParameters(self, sf=None, o=None):
        if sf!=None : self.scaleFactor = sf
        if o!=None: self.offset = o
        
    @staticmethod
    def add_alias( vname, alias):
        Variable.aliases += [ (vname, alias) ]
        Variable.referenceVars[alias] = Variable.referenceVars[vname]
        


    def asDict(self):
        d = dict( name=self.name, from_src =self.from_src,
                  array_transfo = self.array_transfo,              
                  #offsetAdjust = self.offsetAdjust,
                 )

        
        return d


class PseudoVar(Variable):
    from_src=False


class VarAbsValue(Variable):
    """Alows to define a variable being the absloute value of a source variable."""
    from_src=False
    def __init__(self,name,  srcvar ,**args):
        Variable.__init__(self, name, dependencies=[srcvar], srcvar=srcvar, **args)
        
    def setup( self, chain, array ):
        self.chain = chain
        self.srcvar = chain.allVars[self.srcvar]
        self.array = array

        # make sure the system will call update_array before numerator and denominator are updated. 
        self.update_after += [self.srcvar.name  ]
        return self.name, None
        

    def update_array(self,reverse=False):
        if reverse: return
        ne.evaluate("abs(v)", local_dict=dict(v=self.srcvar.array), out = self.array)
        print( 'VarAbsValue ', self.srcvar.array[:10], self.array[:10] )



class VarRatio(Variable):
    """Alows to define a variable being the ratio of 2 source variables. """
    from_src=False
    def __init__(self,name, reverse_update=False, **args ):
        args.setdefault('dependencies', [args['numerator'], args['denom'] ])
        Variable.__init__(self, name, reverse_update=reverse_update, **args)
        
    def setup( self, chain, array ):
        self.chain = chain
        self.numerator = chain.allVars[self.numerator]
        self.denom = chain.allVars[self.denom]
        self.array = array
        # make sure the system will call update_array before numerator and denominator are updated. 
        if self.reverse_update:
            self.numerator.update_after += [ self.name ]
            self.denom.update_after += [ self.name ]
        else:
            self.update_after += [self.numerator.name, self.denom.name ]
        return self.name, None

    def reset_depenencies(self):
        self.dependencies = [self.numerator, self.denom]

    def update_array(self,reverse=False):
        if reverse:
            return
        #imax = len(src_indices)
        print ("updating ",self.name, self.numerator.array[:4] , self.denom.array[:4] )
        ldict = dict(n=self.numerator.array, d=self.denom.array, **numExprConst)

        if hasattr(self, 'mindenom'):
            ldict['minD']= self.mindenom 
            ne.evaluate("n/where(d==0, minD, d)", local_dict=ldict, out = self.array)
        else:
            ne.evaluate("n/d", local_dict=ldict, out = self.array)            
        #self.array[:] = self.numerator.array / self.denom.array



class WeightVar(Variable):
    is_sampleW = True
    normalize = True
    
    def update_array(self,reverse=False):
        Variable.update_array(self,reverse)
        if not reverse:
            if self.chain.last_loaded_file and self.normalize:
                self.normalize_array()

    def normalize_array(self):        
        self.array /=self.array[:self.chain.last_loaded_file.Nentries].sum()
        self.array *= self.array.shape[0]



# needed to workaround automatic casts from literals to float64 in numexpr
numExprConst=dict(
    c_0 = np.float32(0.),
    c_01 = np.float32(0.1),
    c_001 = np.float32(0.01),
    c_0e5 = np.float32(1e-5),
    c_1 = np.float32(1.),
    c_10 = np.float32(1.),
    c_100 = np.float32(100.),
    #c_1000 = np.float32(100.),
)


_evarArgs = dict( pre_filter= lambda e : e>80, h_range=(100,6300),
                  array_transfo=[ "log(a)"  ], array_revtransfo=["exp(a)"], isElike=True,
                  #scaleAdjust=1.9, # if no transformation.
)
_mvarArgs = dict(  h_range=(0,300), #pre_filter= lambda m : m>0,
                  array_transfo=["where(a<=0.1,c_01,a)","log(a)"] , array_revtransfo=["exp(a)"], isElike=True,
                  # offsetAdjust=-0.75, # if no transformation.
)


varList = [

    # Energy 
    Variable("e_true" ,    title="E true",  ), #**_evarArgs
    Variable("e_reco" , "e", title='E reco',  **_evarArgs,),
    Variable("e_dnn" ,  "e_dnn" ,  title='e DNN', **_evarArgs,),
    Variable("e_cal" ,  "e_calib" ,  title='e Atlas', **_evarArgs,),
    
    # Mass
    Variable("m_true" ,     title='M true', **_mvarArgs),
    Variable("m_reco" ,  "m" ,  title='M reco',**_mvarArgs),
    Variable("m_dnn" ,  "m_dnn" ,  title='M DNN', **_evarArgs,),
    Variable("m_cal" ,  "m_calib" ,  title='M Atlas', **_evarArgs,),
    
    # pT
    Variable("pt_true" ,    title="pT true",),
    Variable("pt_reco" ,    title="pT reco",  isElike=True),
    Variable("pt_cal" ,    title="pT cal",  isElike=True),
    Variable("pt_dnn" ,    title="pT dnn",  isElike=True),

    # Event level vars
    Variable("NPV" , h_range=(0,60), bins=45, ),
    Variable("mu", h_range=(0,90),  ),
    WeightVar("eventWeightXS", normalize=True),

    Variable("PID", h_range=(0,23), ),
    
    # Jet direction
    Variable("eta", "DetectorEta" ,h_range=(-5.,5.),  ),
    VarAbsValue('abseta', 'eta' , hrange=(0,5) ),
    Variable("eta_true", "True Eta" ,h_range=(-5.,5.),  ),
    Variable("y", "jet_rapidity", h_range=(-5.,5.),),
    Variable("y_true", "jet_true_rapidity", h_range=(-5.,5.),),

    # Substructure vars
    Variable("D2", array_transfo=["where(a!=a,-c_01,a)"] , h_range=(-0.001,200),  ), # "a!=a" a trick to avoid nan values (isnan not supported by numexpr2)
    Variable("C2", array_transfo=["where(a!=a,-c_01,a)"] , h_range=(-0.001,200),  ),
    Variable("Tau32",    array_transfo=["where(a<=0,-c_01,a)"], h_range=(-0.001,1.4) ),
    Variable("Tau21",    array_transfo=["where(a<=0,-c_01,a)"], h_range=(-0.001,1.4) ),
    Variable("Qw" , array_transfo=["where(a<=0,c_1,a)"], h_range=(0,800) ,  ),         
    Variable("Width", title='Width', h_range=(0.,1.0)),
    Variable("Split12", title='Split12', h_range=(0,2000)),
    Variable("Split23", title='Split23', h_range=(0,600)),
    Variable("groomMratio" , h_range=(0,1), array_transfo=["where(a!=a,c_0,a)"] ),
    
    # Detector level vars
    Variable("neutralFrac" , h_range=(0,1) , ),
    Variable("sumPtTrkFrac" , h_range=(0,1.2) , ),
    Variable("EM3Frac" , h_range=(0,0.6) , array_transfo=["where(a!=a,c_0,a)"] ),
    Variable("Tile0Frac" , h_range=(0,1), array_transfo=["where(a!=a,c_0,a)"] ),
    Variable("sumMassTrkFrac", h_range=(0,1), ),
    Variable("EMFrac", h_range=(0,1), array_transfo=["where(a!=a,c_0,a)"] ),    
    Variable("EffNConsts" , h_range=(0,10) , ),
    Variable("EffNTracks" , h_range=(0,18) , array_transfo=["where(a!=a,-c_01,a)"]),

    # Response
    VarRatio("r_raw_e",   numerator="e_reco", denom="e_true",  post_filter=lambda r : np.logical_and(r>0.1, r<3),  title='E response', h_range=(0,3) , ),
    VarRatio("r_raw_m",   numerator="m_reco", denom="m_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='M response' ,h_range=(0,4),),
    VarRatio("r_raw_pt",   numerator="pt_reco", denom="pt_true",  post_filter=lambda r : np.logical_and(r>0.1, r<3),  title='p_T response', h_range=(0,3) , ),
    
    VarRatio("r_cal_e",   numerator="e_cal", denom="e_true",  post_filter=lambda r : np.logical_and(r>0.3, r<3),  title='E response', h_range=(0,3) , ),
    VarRatio("r_cal_m",   numerator="m_cal", denom="m_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='M response' ,h_range=(0,4),),
    VarRatio("r_cal_pt",   numerator="pt_cal", denom="pt_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='pT response' ,h_range=(0,4),),
    VarRatio("r_dnn_e",   numerator="e_dnn", denom="e_true",  post_filter=lambda r : np.logical_and(r>0.1, r<3),  title='E response', h_range=(0,3) , ),
    VarRatio("r_dnn_m",   numerator="m_dnn", denom="m_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='M response' ,h_range=(0,4),),
    VarRatio("r_dnn_pt",   numerator="pt_dnn", denom="pt_true" , post_filter=lambda r : np.logical_and(r>0.1, r<5) , mindenom=np.float32(1.), title='pT response' ,h_range=(0,4),),

    VarRatio("r_e_ratio", numerator="r_dnn_e", denom="r_cal_e", h_range=(0,2.)),
    Variable("r_m_ratio", numerator="r_dnn_m", denom="r_cal_m", h_range=(0,2.)),

    PseudoVar("r_e_sig", title="E Resolution"),
    PseudoVar("r_m_sig", title="M Resolution"),    
    PseudoVar("r_e_sig2", title="E Resolution (right)"),
    PseudoVar("r_m_sig2", title="M Resolution (right)"),  

]


# The dictionnary below is used to map the generic 'e_var', 'm_var',etc.. to the actual Variable name
# according to the input mode.

_defA = lambda t,r : ConfigDict(true=t, reco=r)
Variable.aliasConfig = ConfigDict(
    e_var = _defA('e_true', 'e_reco'),
    m_var = _defA('m_true', 'm_reco'),
    r_e   = _defA('r_raw_e', 'r_raw_e'),
    r_m   = _defA('r_raw_m', 'r_raw_m'),
)


# ****************************************************
# Define known partitions of the input data.
#  each partition is defined as a set of filters on 1 or more input variables.
#  There is 2 kind of filters : those applied before the variable transformation, those after.
# a partition is thus define as 2 dictionnaries :
#
#   part_name = ( dict_preFilters, dict_postFilters) ,
# where
#  dict_preFilters = dict( var_name1 = filter_function, var_name2 = filter_function2, ...)
# 
dataPartitions = ConfigDict( )
dataPartitions[None] = ({}, {} )
